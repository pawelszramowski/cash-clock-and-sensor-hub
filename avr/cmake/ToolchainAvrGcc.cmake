set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

set(CMAKE_C_COMPILER avr-gcc)
set(CMAKE_CXX_COMPILER avr-gcc)
set(CMAKE_ASM_COMPILER avr-gcc)

# CMAKE_FIND_ROOT_PATH_MODE_INCLUDE, CMAKE_FIND_ROOT_PATH_MODE_LIBRARY,
# CMAKE_FIND_ROOT_PATH_MODE_PACKAGE, and CMAKE_FIND_ROOT_PATH_MODE_PROGRAM
# often found in example toolchain files only take effect when CMAKE_FIND_ROOT_PATH is set.
# However, CMAKE_FIND_ROOT_PATH does not refer to toolchain directory root path.
# For better understanding, read documentation of find_* commands and CMAKE_FIND_* variables,
# and experiment with --debug-find option.

set(ASM_C_CXX_FLAGS_INIT "-mmcu=${MCU} -DF_CPU=${F_CPU} -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -mrelax")

set(CMAKE_ASM_FLAGS_INIT "${ASM_C_CXX_FLAGS_INIT}")
set(CMAKE_C_FLAGS_INIT "${ASM_C_CXX_FLAGS_INIT}")
set(CMAKE_CXX_FLAGS_INIT "${ASM_C_CXX_FLAGS_INIT} -fno-exceptions -fno-rtti")
set(CMAKE_EXE_LINKER_FLAGS_INIT "-Wl,--gc-sections")

set(CPPCHECK_PLATFORM avr8)

function(write_executable_map TARGET)
    target_link_options(${TARGET}
        PRIVATE
            "-Wl,-Map=$<TARGET_FILE_DIR:${TARGET}>/$<TARGET_NAME:${TARGET}>.map"
            -Wl,--demangle
    )
    # set_target_property doesn't append so use set_property instead.
    set_property(
        TARGET ${TARGET}
        APPEND
        PROPERTY ADDITIONAL_CLEAN_FILES "$<TARGET_FILE_DIR:${TARGET}>/$<TARGET_NAME:${TARGET}>.map"
    )
endfunction()

function(write_executable_listing TARGET)
    # Build event commands do not run when the output is missing but the target does not need to be (re)built.
    add_custom_command(
        TARGET ${TARGET}
        POST_BUILD
        COMMAND "${CMAKE_OBJDUMP}" --section-headers --source --demangle "$<TARGET_FILE:${TARGET}>" > "$<SHELL_PATH:$<TARGET_FILE_DIR:${TARGET}>/$<TARGET_NAME:${TARGET}>.lss>"
        VERBATIM
    )
    # BYPRODUCTS of add_custom_command cannot use target-dependent generator expressions
    # and set_target_property doesn't append so use set_property instead.
    set_property(
        TARGET ${TARGET}
        APPEND
        PROPERTY ADDITIONAL_CLEAN_FILES "$<TARGET_FILE_DIR:${TARGET}>/$<TARGET_NAME:${TARGET}>.lss"
    )
endfunction()

function(print_executable_sizes TARGET)
    # Build event commands do not run when the output is missing but the target does not need to be (re)built.
    add_custom_command(
        TARGET ${TARGET}
        POST_BUILD
        COMMAND "${CMAKE_OBJDUMP}" --private=mem-usage "$<TARGET_FILE:${TARGET}>"
        VERBATIM
        USES_TERMINAL
    )
endfunction()
