#pragma once

#include <stdint.h>

/// Piecewise linear approximation of a non-decreasing function.
class PiecewiseLinearApproximation
{
public:
    PiecewiseLinearApproximation() = delete;
    PiecewiseLinearApproximation(
            const PiecewiseLinearApproximation&) = delete;
    PiecewiseLinearApproximation(
            PiecewiseLinearApproximation&&) = delete;
    PiecewiseLinearApproximation& operator =(
            const PiecewiseLinearApproximation&) = delete;
    PiecewiseLinearApproximation& operator =(
            PiecewiseLinearApproximation&&) = delete;
    ~PiecewiseLinearApproximation() = delete;

    struct Point
    {
        uint8_t m_X;
        uint8_t m_Y;
    };

    template<const Point& POINT0, const Point& POINT1, const Point& POINT2, const Point& ...POINTS>
    static constexpr uint8_t CalculateValue(
            const uint8_t x)
    {
        return (x <= POINT1.m_X) ?
                CalculateValue<POINT0, POINT1>(x) : CalculateValue<POINT1, POINT2, POINTS...>(x);
    }

private:
    template<const Point& POINT0, const Point& POINT1>
    static constexpr uint8_t CalculateValue(
            const uint8_t x)
    {
        static_assert(POINT0.m_X < POINT1.m_X, "Points must be sorted!");
        static_assert(POINT0.m_Y <= POINT1.m_Y,
                "Function must be nondecreasing because unsigned types are used!");

        return POINT0.m_Y
                + static_cast<uint16_t>(POINT1.m_Y - POINT0.m_Y)
                        * static_cast<uint16_t>(x - POINT0.m_X) / (POINT1.m_X - POINT0.m_X);
    }
};
