#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "common.h"

#include "GammaCorrection.hpp"
#include "Gpio/PushPullOutput.hpp"
#include "Tc/GenericClock.hpp"
#include "Tc/Tc1Clock.hpp"

/// LCD display backlight provided by PWM-controlled LED.
class Backlight
{
public:
    Backlight() = delete;
    Backlight(
            const Backlight&) = delete;
    Backlight(
            Backlight&&) = delete;
    Backlight& operator =(
            const Backlight&) = delete;
    Backlight& operator =(
            Backlight&&) = delete;
    ~Backlight() = delete;

    static constexpr uint8_t DUTY_CYCLE_ALWAYS_OFF = 0U;
    static constexpr uint8_t DUTY_CYCLE_ALWAYS_ON = UINT8_MAX;
    static constexpr uint8_t DUTY_CYCLE_MIN = DUTY_CYCLE_ALWAYS_OFF + 1U;
    static constexpr uint8_t DUTY_CYCLE_MAX = DUTY_CYCLE_ALWAYS_ON - 1U;

    static void Startup(
            const uint8_t dutyCycle = DUTY_CYCLE_ALWAYS_ON)
    {
        BacklightOutput::Startup();
        InitTc1();
        SetDutyCycle(dutyCycle);
    }

    static void SetDutyCycle(
            const uint8_t dutyCycle)
    {
        if (dutyCycle <= DUTY_CYCLE_ALWAYS_OFF)
        {
            BacklightOutput::SetState(false);
            StopTc1();
        }
        else if (dutyCycle >= DUTY_CYCLE_ALWAYS_ON)
        {
            BacklightOutput::SetState(true);
            StopTc1();
        }
        else
        {
            // BacklightOutput will be overridden by T/C1.
            StartTc1(dutyCycle);
        }
    }

    static void SetBrightness(
            const uint8_t brightness)
    {
        SetDutyCycle(GammaCorrection::DecodeValue(brightness));
    }

private:
    static void InitTc1()
    {
        // Set T/C1 Waveform Generation Mode to Fast PWM mode with TOP value in ICR1
        // in order to enable ICR1 writes.
        TCCR1B = TC1_WGM13_12;
        TCCR1A = TC1_WGM11_10;
        TCCR1C = 0U;

        // Reset T/C1.
        TCNT1 = 0U;
        ICR1 = TC1_TOP;
        OCR1A = TcUtils::Tc1Clock::MAX_CNT;
        OCR1B = TcUtils::Tc1Clock::MAX_CNT;

        // Disable interrupts and reset flags.
        TIMSK1 &= ~(BIT_VAL(ICIE1) | BIT_VAL(OCIE1B) | BIT_VAL(OCIE1A) | BIT_VAL(TOIE1));
        TIFR1 = BIT_VAL(ICF1) | BIT_VAL(OCF1B) | BIT_VAL(OCF1A) | BIT_VAL(TOV1);
    }

    static void StartTc1(
            const uint8_t dutyCycle)
    {
        // Set Output Compare Register to a fraction of TOP value proportional to the duty cycle.
        OCR1B = static_cast<uint32_t>(dutyCycle) * static_cast<uint32_t>(TC1_TOP)
                / static_cast<uint32_t>(UINT8_MAX + 1U);

        // Start T/C1 with OC1B cleared on Compare Match and set at BOTTOM value (non-inverting mode).
        TCCR1A = TC1_WGM11_10 | TC1_COM1B;
        TCCR1B = TC1_WGM13_12 | TC1_CS;
    }

    static void StopTc1()
    {
        // Stop T/C1 and disconnect OC1B.
        TCCR1B = TC1_WGM13_12;
        TCCR1A = TC1_WGM11_10;

        // Reset T/C1.
        TCNT1 = 0U;
    }

    using BacklightOutput = Gpio::PushPullOutput<'B', 2U, true>;

    static constexpr uint32_t TC1_PWM_FREQUENCY_IN_HZ = 50000UL;
    static constexpr TcUtils::Tc1Clock::Reg_t TC1_MIN_RESOLUTION_IN_STEPS = UINT8_MAX;

    static constexpr auto TC1_PRESCALER = TcUtils::Tc1Clock::GetPrescalerValue(
            TC1_PWM_FREQUENCY_IN_HZ, TC1_MIN_RESOLUTION_IN_STEPS);
    static_assert(TC1_PRESCALER != TcUtils::Prescaler::INVALID_VALUE, "Requested T/C1 parameters are not achievable!");

    static constexpr auto TC1_CS = TcUtils::Tc1Clock::GetClockSelect(TC1_PRESCALER);
    static constexpr auto TC1_TOP = TcUtils::Tc1Clock::GetTopValue(TC1_PRESCALER,
                                                                   TC1_PWM_FREQUENCY_IN_HZ);

    static constexpr uint8_t TC1_WGM13_12 = 1U << WGM13 | 1U << WGM12;
    static constexpr uint8_t TC1_WGM11_10 = 1U << WGM11 | 0U << WGM10;
    static constexpr uint8_t TC1_COM1B = 1U << COM1B1 | 0U << COM1B0;
};
