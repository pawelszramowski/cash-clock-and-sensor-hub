#pragma once

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <stdint.h>

#include "common.h"

/// Basic control of ATmegaX8.
class Mcu
{
public:
    Mcu() = delete;
    Mcu(
            const Mcu&) = delete;
    Mcu(
            Mcu&&) = delete;
    Mcu& operator =(
            const Mcu&) = delete;
    Mcu& operator =(
            Mcu&&) = delete;
    ~Mcu() = delete;

    class ResetCause
    {
    public:
        bool IsWatchodogSystemReset() const
        {
            return (m_Mcusr & BIT_VAL(WDRF)) != 0U;
        }

        bool IsBrownOutReset() const
        {
            return (m_Mcusr & BIT_VAL(BORF)) != 0U;
        }

        bool IsExternalReset() const
        {
            return (m_Mcusr & BIT_VAL(EXTRF)) != 0U;
        }

        bool IsPowerOnReset() const
        {
            return (m_Mcusr & BIT_VAL(PORF)) != 0U;
        }

    private:
        friend class Mcu;

        explicit ResetCause(
                uint8_t mcusr)
                :
                m_Mcusr(mcusr)
        {
            // Do nothing.
        }

        uint8_t m_Mcusr = 0U;
    };

    static void Startup()
    {
        AdjustClockPrescaler();
        DisableUnusedPeripherals();
    }

    static void ConfigureWatchdog(
            const uint8_t wdto)
    {
        // Enable interrupt and system reset mode.
        wdt_enable(wdto);
        WDTCSR |= BIT_VAL(WDIF) | BIT_VAL(WDIE);
        sei();
    }

    ATTR_NO_RETURN
    static void Reset()
    {
        cli();

        // Disable interrupt mode.
        WDTCSR &= BIT_VAL(WDIE);

        // Enable watchdog with the shortest possible period.
        wdt_enable(WDTO_15MS);

        // Wait in an infinite loop for a watchdog reset which not only
        // jumps to a start vector but also resets internal registers.
        while (true)
        {
            // Do nothing.
        }
    }

    static void Poll();

    static ResetCause GetResetCause()
    {
        return ResetCause(GPIOR0);
    }

private:
    ATTR_SECTION(".init3") ATTR_NAKED ATTR_USED
    static void DisableWatchdogAfterReset()
    {
        GPIOR0 = MCUSR;
        MCUSR = 0U;
        wdt_disable();
    }

    static void AdjustClockPrescaler()
    {
        // FUSE_CKDIV8 sets prescaler to 8 initially, not permanently.
        clock_prescale_set(clock_div_1);
    }

    static void DisableUnusedPeripherals()
    {
        // Disable unused peripherals.
        ACSR = BIT_VAL(ACD);
        PRR = BIT_VAL(PRTIM2) | BIT_VAL(PRSPI);

        // Disable unused digital inputs.
        DIDR1 = 0U;
        DIDR0 = BIT_VAL(ADC2D) | BIT_VAL(ADC1D) | BIT_VAL(ADC0D);
    }

    static constexpr uint16_t MIN_FREE_STACK_SIZE_IN_BYTES = 32U;
};
