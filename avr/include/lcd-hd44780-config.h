// @formatter:off

/** \defgroup lcd_hd44780 Obsługa wyświetlacza LCD ze sterownikiem HD44780
 *
 * \{ */

////////////////////////////////////////////////////////////////////////////////
/* Zabezpieczenie przed wielokrotnym dołączeniem pliku nagłówkowego */
////////////////////////////////////////////////////////////////////////////////

#ifndef LCD_HD44780_CONFIG_H_INCLUDED
#define LCD_HD44780_CONFIG_H_INCLUDED

////////////////////////////////////////////////////////////////////////////////
/* Ustawienia */
////////////////////////////////////////////////////////////////////////////////

/** \brief Wykorzystanie interfejsu 4-bitowego. */
#define LCD_USE_4B_INTERFACE            1

/** \brief Wykorzystanie linii Read/Write.
 *
 * Jeśli zmienna ma wartość równą zero to po każdej operacji
 * odczekiwany jest odpowiedni okres czasu. */
#define LCD_USE_RW                      1

/** \brief Odczytywanie wyłącznie flagi zajętości.
 *
 * Jeśli zmienna ma wartość różną od zera, to zwracany stan licznika
 * adresowego jest zawsze równy zero, a funkcja do odczytu pamięci DDRAM
 * i CGRAM nie jest dostępna. */
#define LCD_READ_ONLY_BF                1

/** \brief Wykorzystanie dwóch linii wyświetlacza LCD. */
#define LCD_USE_2_LINES                 1

////////////////////////////////////////////////////////////////////////////////
/* Podłączenie sprzętu */
////////////////////////////////////////////////////////////////////////////////

/** \brief Port linii DB7 magistrali danych. */
#define LCD_DB7_PORT                    D
/** \brief Linia DB7 magistrali danych. */
#define LCD_DB7                         7

/** \brief Port linii DB6 magistrali danych. */
#define LCD_DB6_PORT                    D
/** \brief Linia DB6 magistrali danych. */
#define LCD_DB6                         6

/** \brief Port linii DB5 magistrali danych. */
#define LCD_DB5_PORT                    D
/** \brief Linia DB5 magistrali danych. */
#define LCD_DB5                         5

/** \brief Port linii DB4 magistrali danych. */
#define LCD_DB4_PORT                    D
/** \brief Linia DB4 magistrali danych. */
#define LCD_DB4                         4

#if LCD_USE_4B_INTERFACE == 0
    /** \brief Port linii DB3 magistrali danych. */
    #define LCD_DB3_PORT                D
    /** \brief Linia DB3 magistrali danych. */
    #define LCD_DB3                     3

    /** \brief Port linii DB2 magistrali danych. */
    #define LCD_DB2_PORT                D
    /** \brief Linia DB2 magistrali danych. */
    #define LCD_DB2                     2

    /** \brief Port linii DB1 magistrali danych. */
    #define LCD_DB1_PORT                D
    /** \brief Linia DB1 magistrali danych. */
    #define LCD_DB1                     1

    /** \brief Port linii DB0 magistrali danych. */
    #define LCD_DB0_PORT                D
    /** \brief Linia DB0 magistrali danych. */
    #define LCD_DB0                     0
#endif

/** \brief Port linii Enable. */
#define LCD_E_PORT                      B
/** \brief Linia Enable. */
#define LCD_E                           1

#if LCD_USE_RW != 0
    /** \brief Port linii Read/Write. */
    #define LCD_RW_PORT                 B
    /** \brief Linia Read/Write.
     *
     * Jeśli stała programowa LCD_RW nie jest zdefiniowana, to zamiast sprawdzać
     * flagę zajętości odczekiwane są odpowiednie okresy czasu. */
    #define LCD_RW                      3
#endif

/** \brief Port linii Registers Select. */
#define LCD_RS_PORT                     B
/** \brief Linia Registers Select. */
#define LCD_RS                          0

#endif

/** \} */
