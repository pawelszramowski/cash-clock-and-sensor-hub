#pragma once

#include <stdint.h>

namespace GpioUtils {

/// General purpose I/O port.
struct Port
{
    static constexpr char INVALID_DESIGNATOR = '\0';

    char m_Designator;
    uint8_t m_MinPinNum;
    uint8_t m_MaxPinNum;
    volatile uint8_t* m_pPort;
    volatile uint8_t* m_pDdr;
    volatile uint8_t* m_pPin;
};

}
