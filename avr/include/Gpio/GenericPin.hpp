#pragma once

#include <stdint.h>

#include "common.h"

#include "Gpio/Port.hpp"

// NOTE: Don't use identifiers like PORT0, PORT1 etc. They are already defined in avr-libc.

namespace GpioUtils {

/// Generic general purpose I/O pin.
template<char PORT_DESIGNATOR, uint8_t PIN_NUM, const Port& ...SUPPORTED_PORTS>
class GenericPin
{
public:
    GenericPin() = delete;
    GenericPin(
            const GenericPin&) = delete;
    GenericPin(
            GenericPin&&) = delete;
    GenericPin& operator =(
            const GenericPin&) = delete;
    GenericPin& operator =(
            GenericPin&&) = delete;
    ~GenericPin() = delete;

    static void SetPort(
            const bool value)
    {
        if (value)
        {
            *PORT.m_pPort |= PIN_MASK;
        }
        else
        {
            *PORT.m_pPort &= ~PIN_MASK;
        }
    }

    static bool GetPort()
    {
        return (*PORT.m_pPort & PIN_MASK) != 0U;
    }

    static void SetDdr(
            const bool value)
    {
        if (value)
        {
            *PORT.m_pDdr |= PIN_MASK;
        }
        else
        {
            *PORT.m_pDdr &= ~PIN_MASK;
        }
    }

    static bool GetDdr()
    {
        return (*PORT.m_pDdr & PIN_MASK) != 0U;
    }

    static void SetPin(
            const bool value)
    {
        if (value)
        {
            *PORT.m_pPin |= PIN_MASK;
        }
        else
        {
            *PORT.m_pPin &= ~PIN_MASK;
        }
    }

    static bool GetPin()
    {
        return (*PORT.m_pPin & PIN_MASK) != 0U;
    }

private:
    static constexpr Port INVALID_PORT = { Port::INVALID_DESIGNATOR, 0U,
    UINT8_MAX, nullptr, nullptr, nullptr };

    template<const Port& GPIO_PORT>
    static constexpr const Port& ChoosePort()
    {
        return (GPIO_PORT.m_Designator == PORT_DESIGNATOR) ? GPIO_PORT : INVALID_PORT;
    }

    template<const Port& GPIO_PORT0, const Port& GPIO_PORT1, const Port& ...GPIO_PORTS>
    static constexpr const Port& ChoosePort()
    {
        return (ChoosePort<GPIO_PORT0>().m_Designator != Port::INVALID_DESIGNATOR) ?
                ChoosePort<GPIO_PORT0>() : ChoosePort<GPIO_PORT1, GPIO_PORTS...>();
    }

    static constexpr const Port& PORT = ChoosePort<SUPPORTED_PORTS...>();

    static_assert(PORT_DESIGNATOR == PORT.m_Designator, "Invalid port designator!");
    static_assert(PIN_NUM >= PORT.m_MinPinNum, "Port bit number too small!");
    static_assert(PIN_NUM <= PORT.m_MaxPinNum, "Port bit number too large!");

    static constexpr uint8_t PIN_MASK = BIT_VAL(PIN_NUM);
};

}
