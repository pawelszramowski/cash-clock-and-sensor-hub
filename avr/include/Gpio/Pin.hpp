#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "Gpio/GenericPin.hpp"
#include "Gpio/Port.hpp"

namespace GpioUtils {

/// General purpose I/O ports available in ATmegaX8.
struct Ports
{
    static constexpr Port B { 'B', 0U, 7U, &PORTB, &DDRB, &PINB };
    static constexpr Port C { 'C', 0U, 6U, &PORTC, &DDRC, &PINC };
    static constexpr Port D { 'D', 0U, 7U, &PORTD, &DDRD, &PIND };
};

}

namespace Gpio {

/// General purpose I/O pin of ATmegaX8.
template<char PORT_DESIGNATOR, uint8_t PIN_NUM>
using Pin = GpioUtils::GenericPin<PORT_DESIGNATOR, PIN_NUM, GpioUtils::Ports::B, GpioUtils::Ports::C, GpioUtils::Ports::D>;

}
