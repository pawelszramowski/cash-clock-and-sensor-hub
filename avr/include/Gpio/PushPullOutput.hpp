#pragma once

#include <stdint.h>

#include "Gpio/Pin.hpp"

namespace Gpio {

template<char PORT_DESIGNATOR, uint8_t PIN_NUM, bool ACTIVE_STATE>
class PushPullOutput
{
public:
    PushPullOutput() = delete;
    PushPullOutput(
            const PushPullOutput&) = delete;
    PushPullOutput(
            PushPullOutput&&) = delete;
    PushPullOutput& operator =(
            const PushPullOutput&) = delete;
    PushPullOutput& operator =(
            PushPullOutput&&) = delete;
    ~PushPullOutput() = delete;

    static void Startup(
            const bool active = false)
    {
        SetState(active);
        ThePin::SetDdr(true);
    }

    static void SetState(
            const bool active)
    {
        (ACTIVE_STATE == true) ? ThePin::SetPort(active) : ThePin::SetPort(!active);
    }

    static void ToggleState()
    {
        ThePin::SetPin(true);
    }

    static bool GetExpectedState()
    {
        return (ACTIVE_STATE == true) ? ThePin::GetPort() : !ThePin::GetPort();
    }

    static bool GetActualState()
    {
        return (ACTIVE_STATE == true) ? ThePin::GetPin() : !ThePin::GetPin();
    }

private:
    using ThePin = Pin<PORT_DESIGNATOR, PIN_NUM>;
};

}
