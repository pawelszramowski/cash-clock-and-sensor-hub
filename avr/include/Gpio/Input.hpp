#pragma once

#include <stdint.h>

#include "Gpio/Pin.hpp"

namespace Gpio {

template<char PORT_DESIGNATOR, uint8_t PIN_NUM, bool ACTIVE_STATE>
class Input
{
public:
    Input() = delete;
    Input(
            const Input&) = delete;
    Input(
            Input&&) = delete;
    Input& operator =(
            const Input&) = delete;
    Input& operator =(
            Input&&) = delete;
    ~Input() = delete;

    static void Startup(
            const bool enablePullUp = false)
    {
        ThePin::SetDdr(false);
        SetPullUp(enablePullUp);
    }

    static bool GetState()
    {
        return (ACTIVE_STATE == true) ? ThePin::GetPin() : !ThePin::GetPin();
    }

    static void SetPullUp(
            const bool enable)
    {
        ThePin::SetPort(enable);
    }

private:
    using ThePin = Pin<PORT_DESIGNATOR, PIN_NUM>;
};

}
