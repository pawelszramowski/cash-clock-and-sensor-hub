#pragma once

#include <stdint.h>

#include "bme280.h"
#include "common.h"

#include "I2c/DeviceRegs.hpp"
#include "I2c/Master.hpp"
#include "Tc/PeriodicTimer.hpp"

/// Bosch BME280 digital humidity, pressure and temperature sensor.
class Bme280
{
public:
    Bme280() = delete;
    Bme280(
            const Bme280&) = delete;
    Bme280(
            Bme280&&) = delete;
    Bme280& operator =(
            const Bme280&) = delete;
    Bme280& operator =(
            Bme280&&) = delete;
    ~Bme280() = delete;

    struct AtmosphericPressure
    {
        using Value_t = uint32_t;

        Value_t m_ValueInPa;

        static constexpr Value_t MINIMUM_VALUE_IN_PA = 30000UL;
        static constexpr Value_t MAXIMUM_VALUE_IN_PA = 110000UL;
        static constexpr Value_t RESOLUTION_IN_PA = 1UL;
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 1000U;

        bool IsValid() const
        {
            return (m_ValueInPa >= MINIMUM_VALUE_IN_PA) && (m_ValueInPa <= MAXIMUM_VALUE_IN_PA);
        }

        void Reset()
        {
            m_ValueInPa = UINT32_MAX;
        }
    };

    struct Temperature
    {
        using Value_t = int16_t;

        Value_t m_ValueIn0p01DegC;

        static constexpr Value_t MINIMUM_VALUE_IN_0P01_DEG_C = -4000;
        static constexpr Value_t MAXIMUM_VALUE_IN_0P01_DEG_C = 8500;
        static constexpr Value_t RESOLUTION_IN_0P01_DEG_C = 1;
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 1000U;

        bool IsValid() const
        {
            return (m_ValueIn0p01DegC >= MINIMUM_VALUE_IN_0P01_DEG_C)
                    && (m_ValueIn0p01DegC <= MAXIMUM_VALUE_IN_0P01_DEG_C);
        }

        void Reset()
        {
            m_ValueIn0p01DegC = INT16_MAX;
        }
    };

    struct RelativeHumidity
    {
        using Value_t = uint16_t;

        Value_t m_ValueIn0p01Pct;

        static constexpr Value_t MINIMUM_VALUE_IN_0P01_PCT = 0U;
        static constexpr Value_t MAXIMUM_VALUE_IN_0P01_PCT = 10000U;
        static constexpr Value_t RESOLUTION_IN_0P01_PCT = 1U;
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 1000U;

        bool IsValid() const
        {
            static_assert(MINIMUM_VALUE_IN_0P01_PCT == 0U,
                    "Use non-zero minimum value in validity check!");
            return m_ValueIn0p01Pct <= MAXIMUM_VALUE_IN_0P01_PCT;
        }

        void Reset()
        {
            m_ValueIn0p01Pct = UINT16_MAX;
        }
    };

    static constexpr uint32_t MAX_SCL_FREQUENCY_IN_HZ = 2000000UL;

    static void Startup()
    {
        ASSERT_ALWAYS(VerifyId() == I2c::Master::NO_ERROR);
        ASSERT_ALWAYS(Initialize() == I2c::Master::NO_ERROR);
        ASSERT_ALWAYS(ReadCalibrationData() == I2c::Master::NO_ERROR);

        m_PollTimer.Start(MIN_POLL_PERIOD_IN_MS);

        m_AtmosphericPressure.Reset();
        m_Temperature.Reset();
        m_RelativeHumidity.Reset();
    }

    static void Poll()
    {
        if (m_PollTimer.IsDue())
        {
            I2c::DeviceRegs<Registers::REG_PRESS_MSB, Registers::REG_HUM_LSB> regData;
            const auto error = regData.Read(I2C_ADDRESS);
            ASSERT_ALWAYS(error == I2c::Master::NO_ERROR);

            bme280_uncomp_data uncompensatedData;
            bme280_parse_sensor_data(&regData[Registers::REG_PRESS_MSB], &uncompensatedData);

            bme280_data compensatedData;
            ASSERT_ALWAYS(
                    bme280_compensate_data(BME280_ALL, &uncompensatedData, &compensatedData, &m_CalibrationData) == BME280_OK);

#if defined(BME280_32BIT_ENABLE)
            m_AtmosphericPressure.m_ValueInPa = compensatedData.pressure;
            m_Temperature.m_ValueIn0p01DegC = compensatedData.temperature;
            m_RelativeHumidity.m_ValueIn0p01Pct = 100UL * compensatedData.humidity / 1024UL;
#else
            static_assert(false, "Invalid format of compensated data!");
#endif
        }
    }

    static const AtmosphericPressure& GetAtmosphericPressure()
    {
        return m_AtmosphericPressure;
    }

    static const Temperature& GetTemperature()
    {
        return m_Temperature;
    }

    static const RelativeHumidity& GetRelativeHumidity()
    {
        return m_RelativeHumidity;
    }

private:
    static I2c::Master::Error VerifyId()
    {
        I2c::DeviceRegs<Registers::REG_ID> regId;
        auto error = regId.Read(I2C_ADDRESS);
        if ((error == I2c::Master::NO_ERROR)
                && (regId.As<Registers::id_t>()->chip_id != Registers::id_t::BME280_ID))
        {
            error = I2c::Master::ERR_DATA;
        }

        return error;
    }

    static I2c::Master::Error Initialize()
    {
        auto error = I2c::Master::NO_ERROR;

        I2c::DeviceRegs<Registers::REG_CTRL_HUM> regCtrlHum;
        regCtrlHum.As<Registers::Control::ctrl_hum_t>()->osrs_h = Registers::Control::OSRS_OVERSAMPLING_X1;
        error = regCtrlHum.Write(I2C_ADDRESS);
        if (error != I2c::Master::NO_ERROR)
        {
            return error;
        }

        I2c::DeviceRegs<Registers::REG_CONFIG> regConfig;
        regConfig.As<Registers::Control::config_t>()->t_sb = Registers::Control::config_t::T_SB_1000_MS;
        error = regConfig.Write(I2C_ADDRESS);
        if (error != I2c::Master::NO_ERROR)
        {
            return error;
        }

        I2c::DeviceRegs<Registers::REG_CTRL_MEAS> regCtrlMeas;
        regCtrlMeas.As<Registers::Control::ctrl_meas_t>()->osrs_t = Registers::Control::OSRS_OVERSAMPLING_X1;
        regCtrlMeas.As<Registers::Control::ctrl_meas_t>()->osrs_p = Registers::Control::OSRS_OVERSAMPLING_X1;
        regCtrlMeas.As<Registers::Control::ctrl_meas_t>()->mode = Registers::Control::ctrl_meas_t::MODE_NORMAL;
        error = regCtrlMeas.Write(I2C_ADDRESS);
        if (error != I2c::Master::NO_ERROR)
        {
            return error;
        }

        return error;
    }

    static I2c::Master::Error ReadCalibrationData()
    {
        auto error = I2c::Master::NO_ERROR;

        I2c::DeviceRegs<Registers::REG_CALIB00, Registers::REG_CALIB25> regCalib00Calib25;
        error = regCalib00Calib25.Read(I2C_ADDRESS);
        if (error != I2c::Master::NO_ERROR)
        {
            return error;
        }

        bme280_parse_temp_press_calib_data(&regCalib00Calib25[Registers::REG_CALIB00],
                                           &m_CalibrationData);

        I2c::DeviceRegs<Registers::REG_CALIB26, Registers::REG_CALIB41> regCalib26Calib41;
        error = regCalib26Calib41.Read(I2C_ADDRESS);
        if (error != I2c::Master::NO_ERROR)
        {
            return error;
        }

        bme280_parse_humidity_calib_data(&regCalib26Calib41[Registers::REG_CALIB26],
                                         &m_CalibrationData);

        return error;
    }

    struct Registers
    {
        enum : uint8_t
        {
            REG_HUM_LSB = 0xFEU,
            REG_HUM_MSB = 0xFDU,
            REG_TEMP_XLSB = 0xFCU,
            REG_TEMP_LSB = 0xFBU,
            REG_TEMP_MSB = 0xFAU,
            REG_PRESS_XLSB = 0xF9U,
            REG_PRESS_LSB = 0xF8U,
            REG_PRESS_MSB = 0xF7U,
            REG_CONFIG = 0xF5U,
            REG_CTRL_MEAS = 0xF4U,
            REG_STATUS = 0xF3U,
            REG_CTRL_HUM = 0xF2U,
            REG_CALIB41 = 0xF0U,
            REG_CALIB26 = 0xE1U,
            REG_RESET = 0xE0U,
            REG_ID = 0xD0U,
            REG_CALIB25 = 0xA1U,
            REG_CALIB00 = 0x88U,
        };

        using calib00_calib25_t = uint8_t[REG_CALIB25 - REG_CALIB00 + 1U];

        struct id_t
        {
            uint8_t chip_id :8;

            static constexpr uint8_t BME280_ID = 0x60U;
        };

        struct reset_t
        {
            uint8_t reset :8;

            static constexpr uint8_t RESET_KEY = 0xB6U;
        };

        using calib26_calib41_t = uint8_t[REG_CALIB41 - REG_CALIB26 + 1U];

        struct Control
        {
            struct ctrl_hum_t
            {
                uint8_t osrs_h :3;
            } ctrl_hum;
            struct status_t
            {
                uint8_t im_update :1;
                uint8_t :2;
                uint8_t measuring :1;
            } status;
            struct ctrl_meas_t
            {
                uint8_t mode :2;
                uint8_t osrs_p :3;
                uint8_t osrs_t :3;

                enum : uint8_t
                {
                    MODE_SLEEP = 0b00,
                    MODE_FORCED = 0b01,
                    MODE_NORMAL = 0b11,
                };
            } ctrl_meas;
            struct config_t
            {
                uint8_t spi3w_en :1;
                uint8_t :1;
                uint8_t filter :3;
                uint8_t t_sb :3;

                enum : uint8_t
                {
                    FILTER_OFF = 0b000,
                    FILTER_COEFFICIENT_2 = 0b001,
                    FILTER_COEFFICIENT_4 = 0b010,
                    FILTER_COEFFICIENT_8 = 0b011,
                    FILTER_COEFFICIENT_16 = 0b100,
                };

                enum : uint8_t
                {
                    T_SB_0P5_MS = 0b000,
                    T_SB_62P5_MS = 0b001,
                    T_SB_125_MS = 0b010,
                    T_SB_250_MS = 0b011,
                    T_SB_500_MS = 0b100,
                    T_SB_1000_MS = 0b101,
                    T_SB_10_MS = 0b110,
                    T_SB_20_MS = 0b111,
                };
            } config;

            enum : uint8_t
            {
                OSRS_SKIPPED = 0b00,
                OSRS_OVERSAMPLING_X1 = 0b001,
                OSRS_OVERSAMPLING_X2 = 0b010,
                OSRS_OVERSAMPLING_X4 = 0b011,
                OSRS_OVERSAMPLING_X8 = 0b100,
                OSRS_OVERSAMPLING_X16 = 0b101,
            };
        } ATTR_PACKED;

        struct Data
        {
            struct press_msb_t
            {
                uint8_t press_msb :8;
            } press_msb;
            struct press_lsb_t
            {
                uint8_t press_lsb :8;
            } press_lsb;
            struct press_xlsb_t
            {
                uint8_t :4;
                uint8_t press_xlsb :4;
            } press_xlsb;
            struct temp_msb_t
            {
                uint8_t temp_msb :8;
            } temp_msb;
            struct temp_lsb_t
            {
                uint8_t temp_lsb :8;
            } temp_lsb;
            struct temp_xlsb_t
            {
                uint8_t :4;
                uint8_t temp_xlsb :4;
            } temp_xlsb;
            struct hum_msb_t
            {
                uint8_t hum_msb :8;
            } hum_msb;
            struct hum_lsb_t
            {
                uint8_t hum_lsb :8;
            } hum_lsb;

            uint32_t GetPress() const
            {
                return (press_msb.press_msb << 12U) | (press_lsb.press_lsb << 4U)
                        | press_xlsb.press_xlsb;
            }

            uint32_t GetTemp() const
            {
                return (temp_msb.temp_msb << 12U) | (temp_lsb.temp_lsb << 4U) | temp_xlsb.temp_xlsb;
            }

            uint32_t GetHum() const
            {
                return (hum_msb.hum_msb << 8U) | hum_lsb.hum_lsb;
            }
        } ATTR_PACKED;
    };

    static constexpr uint8_t I2C_ADDRESS = 0x76U << 1U;

    static constexpr uint16_t MIN_POLL_PERIOD_IN_MS = 1000U;

    static PeriodicTimer m_PollTimer;

    static bme280_calib_data m_CalibrationData;

    static AtmosphericPressure m_AtmosphericPressure;
    static Temperature m_Temperature;
    static RelativeHumidity m_RelativeHumidity;

    static_assert(AtmosphericPressure::SAMPLING_PERIOD_IN_MS >= MIN_POLL_PERIOD_IN_MS, "Invalid sampling period!");
    static_assert(Temperature::SAMPLING_PERIOD_IN_MS >= MIN_POLL_PERIOD_IN_MS, "Invalid sampling period!");
    static_assert(RelativeHumidity::SAMPLING_PERIOD_IN_MS >= MIN_POLL_PERIOD_IN_MS, "Invalid sampling period!");
};

