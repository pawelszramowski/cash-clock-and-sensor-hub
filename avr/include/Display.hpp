#pragma once

#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <util/delay.h>

#include "Bcd/Date.hpp"
#include "Bcd/Time.hpp"
#include "Bcd/Value.hpp"
#include "common.h"

#include "lcd-hd44780.h"
#include "Sensors.hpp"
#include "Tc/OneshotTimer.hpp"
#include "Tc/PeriodicTimer.hpp"

/// 2 lines by 16 characters LCD display.
///
/// Layout when showing time:
/// \verbatim
/// HH:MM:SS  TTTTTC
/// PPPPPPhPa HHHHH%
/// \endverbatim
///
/// Layout when showing date:
/// \verbatim
/// YY.MM.DD  TTTTTC
/// PPPPPPhPa HHHHH%
/// \endverbatim
///
/// Layout when showing particulate matter:
/// \verbatim
/// ppppug/m3 TTTTTC
/// PPPPPPhPa HHHHH%
/// \endverbatim
///
/// Possible layout when showing time with two icons:
/// \verbatim
/// HH:MM:SS PPPPhPa
/// II TTTTT*C HHHH%
/// \endverbatim
///
/// Possible layout when showing date with two icons:
/// \verbatim
/// YY.MM.DD PPPPhPa
/// II TTTTT*C HHHH%
/// \endverbatim
class Display
{
public:
    Display() = delete;
    Display(
            const Display&) = delete;
    Display(
            Display&&) = delete;
    Display& operator =(
            const Display&) = delete;
    Display& operator =(
            Display&&) = delete;
    ~Display() = delete;

    static constexpr uint8_t SCROLL_INDEFINITELY = 0U;

    static void EarlyStartup()
    {
        LCD_Init();
        LCD_EnableDisplay();
    }

    static void ForceFailedAssertInfo(
            const char* pFileNameP,
            const uint16_t lineNum,
            const char* pAdditionalInfoP,
            const uint8_t scrollsNum)
    {
        char lineBuffer[LCD_CHARS_PER_LINE];

        LCD_ClearDisplay();

        AssertLocationToString(lineBuffer, pFileNameP, lineNum);
        LCD_WriteString(lineBuffer);

        LCD_Goto(1, 0);
        LCD_WriteStringP(pAdditionalInfoP);

        ScrollDisplay(scrollsNum);
    }

    static void ForcePm2p5SensorPowerOnInfo(
            const bool pm2p5SensorEnabledAtPowerOn)
    {
        LCD_ClearDisplay();

        LCD_WriteStringP(PSTR("PM2.5 sensor at"));
        LCD_Goto(1, 0);
        LCD_WriteStringP(PSTR("power on: "));
        LCD_WriteStringP(pm2p5SensorEnabledAtPowerOn ? PSTR("on") : PSTR("off"));

        OneshotTimer displayTimer;
        displayTimer.Start(PM2P5_SENSOR_POWER_ON_INFO_DISPLAY_TIME_IN_MS);
        while (!displayTimer.IsExpired())
        {
            wdt_reset();
        }
    }

    static void LateStartup()
    {
        static constexpr uint8_t ONE_CHAR = 1U;
        LCD_WriteCgramP(CGRAM_PATTERN_DEGREES_CELSIUS, ONE_CHAR, CGRAM_CHAR_DEGREES_CELSIUS);
        LCD_WriteCgramP(CGRAM_PATTERN_MICRO, ONE_CHAR, CGRAM_CHAR_MICRO);

        m_UpdateTimer.Start(MIN_UPDATE_PERIOD_IN_MS, true);
        m_IsTimeDateBlankingActive = false;
    }

    static void Update(
            const Bcd::Time& rTime,
            const Bcd::Date& rDate,
            const bool isSetRequired,
            const Sensors::AtmosphericPressure& rAtmosphericPressure,
            const Sensors::Temperature& rTemperature,
            const Sensors::RelativeHumidity& rRelativeHumidity,
            const Sensors::Pm2p5& rPm2p5)
    {
        if (m_UpdateTimer.IsDue())
        {
            char lineBuffer[LCD_CHARS_PER_LINE];

            LCD_ClearDisplay();

            if (rPm2p5.IsValid())
            {
                ASSERT_ALWAYS(DecToString(lineBuffer, rPm2p5.m_ValueInUgPerM3) <= 4U);
                LCD_WriteString(lineBuffer);
                LCD_WriteChar(CGRAM_CHAR_MICRO);
                LCD_WriteStringP(PSTR("g/m3"));
            }
            else if (rTime.IsValid() && rDate.IsValid()
                    && !(isSetRequired && m_IsTimeDateBlankingActive))
            {
                if (Bcd::IsInRange(rTime.m_Second, 0x31, 0x45))
                {
                    DateToString(lineBuffer, rDate);
                }
                else
                {
                    TimeToString(lineBuffer, rTime);
                }
                LCD_WriteString(lineBuffer);
            }

            if (rTemperature.IsValid())
            {
                LCD_Goto(0, 10);
                ASSERT_ALWAYS(DecToString(lineBuffer, 5U, rTemperature.m_ValueIn0p01DegC, 2U) > 0U);
                LCD_WriteString(lineBuffer);
                LCD_WriteChar(CGRAM_CHAR_DEGREES_CELSIUS);
            }

            if (rAtmosphericPressure.IsValid())
            {
                LCD_Goto(1, 0);
                ASSERT_ALWAYS(
                        DecToString(lineBuffer, 6U, rAtmosphericPressure.m_ValueInPa, 2U) > 0U);
                LCD_WriteString(lineBuffer);
                LCD_WriteStringP(PSTR("hPa"));
            }

            if (rRelativeHumidity.IsValid())
            {
                LCD_Goto(1, 10);
                ASSERT_ALWAYS(
                        DecToString(lineBuffer, 5U, rRelativeHumidity.m_ValueIn0p01Pct, 2U) > 0U);
                LCD_WriteString(lineBuffer);
                LCD_WriteChar('%');
            }

            m_IsTimeDateBlankingActive = !m_IsTimeDateBlankingActive;
        }
    }

private:
    static void AssertLocationToString(
            char (&rBuffer)[LCD_CHARS_PER_LINE],
            const char* pFileNameP,
            const uint16_t lineNum)
    {
        static constexpr small_size_t MAX_LINE_NUM_LEN = 5U; // for UINT16_MAX
        static constexpr small_size_t MAX_FILE_NAME_LEN = sizeof(rBuffer) - sizeof(NAME_LINE_SEPARATOR)
                - MAX_LINE_NUM_LEN - sizeof '\0';

        char* pBuffer = &rBuffer[0];

        if (pFileNameP != nullptr)
        {
            const small_size_t fileNameLen = strlen_P(pFileNameP);
            if (fileNameLen > MAX_FILE_NAME_LEN)
            {
                static constexpr small_size_t ELLIPSIS_LEN = 3U;
                static constexpr small_size_t SHORTENED_FILE_NAME_LEN = MAX_FILE_NAME_LEN - ELLIPSIS_LEN;

                for (small_size_t dotsLeft = ELLIPSIS_LEN; dotsLeft > 0U; --dotsLeft)
                {
                    *pBuffer++ = '.';
                }

                (void)strcpy_P(pBuffer, pFileNameP + (fileNameLen - SHORTENED_FILE_NAME_LEN));
                pBuffer += SHORTENED_FILE_NAME_LEN;
            }
            else
            {
                (void)strcpy_P(pBuffer, pFileNameP);
                pBuffer += fileNameLen;
            }

            // FIXME Backslash is replaced with slash because standard (Japanese) HD44780 ROM
            // (code 00) supplies a Yen symbol where the backslash character is normally found
            // (and left and right arrow symbols in place of tilde and the rubout character).
            for (char* pChar = &rBuffer[0]; pChar < pBuffer; ++pChar)
            {
                if (*pChar == '\\')
                {
                    *pChar = '/';
                }
            }
        }

        *pBuffer++ = NAME_LINE_SEPARATOR;
        pBuffer += DecToString(pBuffer, lineNum);
        *pBuffer = '\0';
    }

    /// @pre Watchdog is disabled or enabled with longer timeout than @ref CHAR_SCROLL_PERIOD_IN_MS.
    static void ScrollDisplay(
            const uint8_t scrollsNum)
    {
        for (uint8_t scrollsLeft = scrollsNum;
                (scrollsLeft > 0U) || (scrollsNum == SCROLL_INDEFINITELY); --scrollsLeft)
        {
            for (uint8_t charsLeft = LCD_CHARS_PER_LINE; charsLeft > 0; --charsLeft)
            {
                _delay_ms(CHAR_SCROLL_PERIOD_IN_MS);
                LCD_WriteInstruction(
                        LCD_CMD_CURSOR_OR_DISPLAY_SHIFT | LCD_MOD_SHIFT_DISPLAY | LCD_MOD_SHIFT_LEFT);
            }
        }
    }

    static void TimeToString(
            char* pBuffer,
            const Bcd::Time& rTime)
    {
        pBuffer += BcdToString(pBuffer, rTime.m_Hour);
        *pBuffer++ = TIME_SEPARATOR;
        pBuffer += BcdToString(pBuffer, rTime.m_Minute);
        *pBuffer++ = TIME_SEPARATOR;
        (void)BcdToString(pBuffer, rTime.m_Second);
    }

    static void DateToString(
            char* pBuffer,
            const Bcd::Date& rDate)
    {
        pBuffer += BcdToString(pBuffer, rDate.m_Year);
        *pBuffer++ = DATE_SEPARATOR;
        pBuffer += BcdToString(pBuffer, rDate.m_Month);
        *pBuffer++ = DATE_SEPARATOR;
        (void)BcdToString(pBuffer, rDate.m_Day);
    }

    static uint8_t HexToString(
            char* pBuffer,
            const uint8_t value)
    {
        const uint8_t highNibble = (value >> 4U) & 0x0FU;
        *pBuffer++ = highNibble >= 0xAU ? highNibble - 0xAU + 'A' : highNibble + '0';
        const uint8_t lowNibble = value & 0x0FU;
        *pBuffer++ = lowNibble >= 0xAU ? lowNibble - 0xAU + 'A' : lowNibble + '0';
        *pBuffer = '\0';
        return 2U;
    }

    static uint8_t BcdToString(
            char* pBuffer,
            const Bcd::Bcd_t value)
    {
        *pBuffer++ = '0' + ((value >> 4U) & 0x0FU);
        *pBuffer++ = '0' + (value & 0x0FU);
        *pBuffer = '\0';
        return 2U;
    }

    static uint8_t DecToString(
            char* pBuffer,
            const int32_t value,
            const uint8_t fractionalDigitsNum = 0U)
    {
        char* pCurrentChar = pBuffer;
        uint8_t stringLen = 0U;

        const bool isNegative = value < 0;
        ldiv_t divisionResults { value, 0 };

        do
        {
            divisionResults = ldiv(divisionResults.quot, DECIMAL_RADIX);
            const char digit = static_cast<char>(labs(divisionResults.rem));
            *pCurrentChar++ = '0' + digit;
            ++stringLen;

            if (stringLen == fractionalDigitsNum)
            {
                *pCurrentChar++ = DECIMAL_POINT;
                ++stringLen;
            }
        } while ((divisionResults.quot != 0)
                || ((fractionalDigitsNum > 0U)
                        && (stringLen <= (fractionalDigitsNum + sizeof(DECIMAL_POINT)))));

        if (isNegative)
        {
            *pCurrentChar++ = '-';
            ++stringLen;
        }

        *pCurrentChar-- = '\0';

        while (pBuffer < pCurrentChar)
        {
            const char temp = *pCurrentChar;
            *pCurrentChar-- = *pBuffer;
            *pBuffer++ = temp;
        }

        return stringLen;
    }

    static uint8_t DecToString(
            char* const pBuffer,
            const uint8_t maxLen,
            int32_t value,
            uint8_t fractionalDigitsNum)
    {
        uint8_t stringLen;

        while (true)
        {
            stringLen = DecToString(pBuffer, value, fractionalDigitsNum);
            if (stringLen <= maxLen)
            {
                break;
            }
            else if (fractionalDigitsNum > 0U)
            {
                if (value >= 0)
                {
                    value += DECIMAL_RADIX / 2;
                }
                else
                {
                    value -= DECIMAL_RADIX / 2;
                }
                value /= DECIMAL_RADIX;
                --fractionalDigitsNum;
            }
            else
            {
                *pBuffer = '\0';
                stringLen = 0U;
                break;
            }
        }

        return stringLen;
    }

    static constexpr char NAME_LINE_SEPARATOR = ':';
    static constexpr char TIME_SEPARATOR = ':';
    static constexpr char DATE_SEPARATOR = '.';
    static constexpr char DECIMAL_POINT = ',';

    static constexpr long int DECIMAL_RADIX = 10;

    static constexpr double CHAR_SCROLL_PERIOD_IN_MS = 1000;
    static constexpr double PM2P5_SENSOR_POWER_ON_INFO_DISPLAY_TIME_IN_MS = 3000;
    static constexpr uint16_t MIN_UPDATE_PERIOD_IN_MS = 500U;

    enum : char
    {
        CGRAM_CHAR_DEGREES_CELSIUS = '\x08',
        CGRAM_CHAR_MICRO
    };

    static constexpr uint8_t CGRAM_PATTERN_DEGREES_CELSIUS[LCD_BYTES_PER_CHAR] PROGMEM =
            {
                    0x18,
                    0x18,
                    0x06,
                    0x09,
                    0x08,
                    0x08,
                    0x09,
                    0x06
            };

    static constexpr uint8_t CGRAM_PATTERN_MICRO[LCD_BYTES_PER_CHAR] PROGMEM =
            {
                    0x11,
                    0x11,
                    0x11,
                    0x13,
                    0x1D,
                    0x10,
                    0x10,
                    0x10
            };

    static PeriodicTimer m_UpdateTimer;
    static bool m_IsTimeDateBlankingActive;
};
