// @formatter:off

/** \defgroup lcd_hd44780 Obsługa wyświetlacza LCD ze sterownikiem HD44780
 *
 * \{ */

////////////////////////////////////////////////////////////////////////////////
/* Zabezpieczenie przed wielokrotnym dołączeniem pliku nagłówkowego */
////////////////////////////////////////////////////////////////////////////////

#ifndef LCD_HD44780_H_INCLUDED
#define LCD_HD44780_H_INCLUDED

////////////////////////////////////////////////////////////////////////////////
/* Dołączenie bibliotek */
////////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <stdint.h>

#include "common.h"
#include "lcd-hd44780-config.h"

////////////////////////////////////////////////////////////////////////////////
/* Definicje stałych programowych */
////////////////////////////////////////////////////////////////////////////////

/** \brief Liczba bajtów przypadająca na każdy znak w pamięci CGRAM. */
#define LCD_BYTES_PER_CHAR              8

/** \brief Liczba znaków mieszczących się w pamięci CGRAM. */
#define LCD_CHARS_PER_CGRAM             8

/** \brief Liczba znaków mieszczących się w jednej linii w pamięci DDRAM. */
#define LCD_CHARS_PER_LINE              40

/** \brief Flaga zajętości. */
#define LCD_BUSY_FLAG                   7

/** \brief Adres pierwszego znaku pierwszej linii w pamięci DDRAM. */
#define LCD_ADDR_LINE_1                 0x00

/** \brief Adres pierwszego znaku drugiej linii w pamięci DDRAM. */
#define LCD_ADDR_LINE_2                 0x40

/** \brief Komenda Clear Display. */
#define LCD_CMD_CLEAR_DISPLAY           BIT_VAL(0)

/** \brief Komenda Return Home. */
#define LCD_CMD_RETURN_HOME             BIT_VAL(1)

/** \brief Komenda Entry Mode Set. */
#define LCD_CMD_ENTRY_MODE_SET          BIT_VAL(2)

/** \brief Modyfikator komendy Entry Mode Set ustawiający inkrementację
 * licznika adresowego. */
#define LCD_MOD_ADDRESS_INC             BIT_VAL(1)

/** \brief Modyfikator komendy Entry Mode Set ustawiający dekrementację
 * licznika adresowego. */
#define LCD_MOD_ADDRESS_DEC             0x00

/** \brief Modyfikator komendy Entry Mode Set włączający przesuwanie. */
#define LCD_MOD_SHIFT_ENABLE            BIT_VAL(0)

/** \brief Modyfikator komendy Entry Mode Set wyłączający przesuwanie. */
#define LCD_MOD_SHIFT_DISABLE           0x00

/** \brief Komenda Display Control. */
#define LCD_CMD_DISPLAY_CONTROL         BIT_VAL(3)

/** \brief Modyfikator komendy Display Control włączający wyświetlacz. */
#define LCD_MOD_DISPLAY_ON              BIT_VAL(2)

/** \brief Modyfikator komendy Display Control wyłączający wyświetlacz. */
#define LCD_MOD_DISPLAY_OFF             0x00

/** \brief Modyfikator komendy Display Control włączający kursor. */
#define LCD_MOD_CURSOR_ON               BIT_VAL(1)

/** \brief Modyfikator komendy Display Control wyłączający kursor. */
#define LCD_MOD_CURSOR_OFF              0x00

/** \brief Modyfikator komendy Display Control włączający miganie kursora. */
#define LCD_MOD_CURSOR_BLINK_ON         BIT_VAL(0)

/** \brief Modyfikator komendy Display Control wyłączający miganie kursora. */
#define LCD_MOD_CURSOR_BLINK_OFF        0x00

/** \brief Komenda Cursor or Display Shift. */
#define LCD_CMD_CURSOR_OR_DISPLAY_SHIFT BIT_VAL(4)

/** \brief Modyfikator komendy Cursor or Display Shift przesuwający obraz. */
#define LCD_MOD_SHIFT_DISPLAY           BIT_VAL(3)

/** \brief Modyfikator komendy Cursor or Display Shift przesuwający kursor. */
#define LCD_MOD_SHIFT_CURSOR            0x00

/** \brief Modyfikator komendy Cursor or Display Shift ustawiający przesuwanie
 * w prawo. */
#define LCD_MOD_SHIFT_RIGHT             BIT_VAL(2)

/** \brief Modyfikator komendy Cursor or Display Shift ustawiający przesuwanie
 * w lewo. */
#define LCD_MOD_SHIFT_LEFT              0x00

/** \brief Komenda Set CGRAM Address. */
#define LCD_CMD_SET_CGRAM_ADDRESS       BIT_VAL(6)

/** \brief Komenda Set DDRAM Address. */
#define LCD_CMD_SET_DDRAM_ADDRESS       BIT_VAL(7)


////////////////////////////////////////////////////////////////////////////////
/* Deklaracje funkcji */
////////////////////////////////////////////////////////////////////////////////

#if defined(__cplusplus)
extern "C" {
#endif

/** \brief Inicjalizacja wyświetlacza LCD. */
void LCD_Init(
        void);

/** \brief Zapisanie łańcucha znaków do pamięci wyświetlacza LCD. */
void LCD_WriteString(
        const char *pString);

/** \brief Zapisanie łańcucha znaków z pamięci programu do pamięci
 * wyświetlacza LCD. */
void LCD_WriteStringP(
        const char *pStringP);

/** \brief Zapisanie podanej liczby wzorów znaków do pamięci CGRAM. */
void LCD_WriteCgramP(
        const uint8_t *pDataP,
        const uint8_t charCount,
        const char address);

/** \brief Zapisanie podanej liczby wzorów znaków do pamięci CGRAM. */
void LCD_ClearCgram(
        void);

/** \brief Zapisanie jednego znaku do pamięci DDRAM lub CGRAM
 * wyświetlacza LCD. */
void LCD_WriteData(
        const uint8_t data);

static inline
void LCD_WriteChar(
        const char data)
{
    LCD_WriteData((uint8_t)data);
}

/** \brief Wysłanie instrukcji do wyświetlacza LCD. */
void LCD_WriteInstruction(
        const uint8_t instruction);

#if (LCD_USE_RW != 0) && (LCD_READ_ONLY_BF == 0)
    /** \brief Odczytanie jednego znaku z pamięci DDRAM lub CGRAM
     * wyświetlacza LCD. */
    uint8_t LCD_ReadData(
            void);
#endif

#if LCD_USE_RW != 0
    /** \brief Odczytanie flagi zajętości i licznika adresowego z
     * wyświetlacza LCD. */
    uint8_t LCD_ReadInstruction(
            void);
#endif

static inline
void LCD_EnableDisplay(
        void)
{
    LCD_WriteInstruction(LCD_CMD_DISPLAY_CONTROL | LCD_MOD_DISPLAY_ON
            | LCD_MOD_CURSOR_OFF | LCD_MOD_CURSOR_BLINK_OFF);
}

/** \brief Ustawienie kursora pod zadanym adresem pamięci DDRAM. */
static inline
void LCD_Goto(
        const uint8_t row,
        const uint8_t column)
{
    LCD_WriteInstruction(LCD_CMD_SET_DDRAM_ADDRESS | ((row > 0U) ? LCD_ADDR_LINE_2
            : LCD_ADDR_LINE_1) | column);
}

/** \brief Wyczyszczenie pamięci DDRAM wyświetlacza. */
static inline
void LCD_ClearDisplay(
        void)
{
    LCD_WriteInstruction(LCD_CMD_CLEAR_DISPLAY);
}

#if defined(__cplusplus)
}
#endif

#endif

/** \} */
