#pragma once

#include <avr/io.h>
#include <stdint.h>

namespace AdcUtils {

/// Wrapper for ATmegaX8 ADC clock prescaler calculations.
class Clock
{
public:
    Clock() = delete;
    Clock(
            const Clock&) = delete;
    Clock(
            Clock&&) = delete;
    Clock& operator =(
            const Clock&) = delete;
    Clock& operator =(
            Clock&&) = delete;
    ~Clock() = delete;

private:
    struct Prescaler
    {
        static constexpr uint16_t INVALID_VALUE = 0U;
        static constexpr uint8_t INVALID_PS = 0U;

        uint16_t m_Value;
        uint8_t m_Ps;
    };

    static constexpr uint32_t MIN_FREQUENCY_IN_HZ = 50000UL;
    static constexpr uint32_t MAX_FREQUENCY_IN_HZ = 200000UL;

    static constexpr bool IsPrescalerValueValid(
            const uint16_t prescalerValue)
    {
        return ((F_CPU / prescalerValue) >= MIN_FREQUENCY_IN_HZ)
                && ((F_CPU / prescalerValue) <= MAX_FREQUENCY_IN_HZ);
    }

    template<const Prescaler& PRESCALER>
    static constexpr uint8_t ChoosePrescalerSelect()
    {
        return IsPrescalerValueValid(PRESCALER.m_Value) ? PRESCALER.m_Ps : Prescaler::INVALID_PS;
    }

    template<const Prescaler& PRESCALER0, const Prescaler& PRESCALER1,
            const Prescaler& ...PRESCALERS>
    static constexpr uint8_t ChoosePrescalerSelect()
    {
        return (ChoosePrescalerSelect<PRESCALER0>() != Prescaler::INVALID_PS) ?
                ChoosePrescalerSelect<PRESCALER0>() :
                ChoosePrescalerSelect<PRESCALER1, PRESCALERS...>();
    }

    static constexpr Prescaler P1 { 2U, 0U << ADPS2 | 0U << ADPS1 | 1U << ADPS0 };
    static constexpr Prescaler P2 { 4U, 0U << ADPS2 | 1U << ADPS1 | 0U << ADPS0 };
    static constexpr Prescaler P3 { 8U, 0U << ADPS2 | 1U << ADPS1 | 1U << ADPS0 };
    static constexpr Prescaler P4 { 16U, 1U << ADPS2 | 0U << ADPS1 | 0U << ADPS0 };
    static constexpr Prescaler P5 { 32U, 1U << ADPS2 | 0U << ADPS1 | 1U << ADPS0 };
    static constexpr Prescaler P6 { 64U, 1U << ADPS2 | 1U << ADPS1 | 0U << ADPS0 };
    static constexpr Prescaler P7 { 128U, 1U << ADPS2 | 1U << ADPS1 | 1U << ADPS0 };

public:
    static constexpr uint8_t GetPrescalerSelect(
            const bool preferAccuracyOverSpeed = true)
    {
        return preferAccuracyOverSpeed ?
                ChoosePrescalerSelect<P7, P6, P5, P4, P3, P2, P1>() :
                ChoosePrescalerSelect<P1, P2, P3, P4, P5, P6, P7>();
    }
};

}
