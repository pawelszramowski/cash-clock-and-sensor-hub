#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "common.h"

#include "Adc/Clock.hpp"

/// Analog-to-Digital Converter of ATmegaX8.
class Adc
{
public:
    Adc() = delete;
    Adc(
            const Adc&) = delete;
    Adc(
            Adc&&) = delete;
    Adc& operator =(
            const Adc&) = delete;
    Adc& operator =(
            Adc&&) = delete;
    ~Adc() = delete;

    static constexpr uint8_t RESOLUTION_IN_BITS = 10U;
    using Sample_t = uint16_t;

    static constexpr Sample_t FULL_SCALE = BITS_MASK(RESOLUTION_IN_BITS);

    enum ReferenceVoltage : uint8_t
    {
        VREF_AREF = 0U << REFS1 | 0U << REFS0,
        VREF_AVCC = 0U << REFS1 | 1U << REFS0,
        VREF_RESERVED2 = 1U << REFS1 | 0U << REFS0,
        VREF_INTERNAL_1V1 = 1U << REFS1 | 1U << REFS0,
    };

    enum Channel : uint8_t
    {
        CHANNEL_ADC0 = 0U << MUX3 | 0U << MUX2 | 0U << MUX1 | 0U << MUX0,
        CHANNEL_ADC1 = 0U << MUX3 | 0U << MUX2 | 0U << MUX1 | 1U << MUX0,
        CHANNEL_ADC2 = 0U << MUX3 | 0U << MUX2 | 1U << MUX1 | 0U << MUX0,
        CHANNEL_ADC3 = 0U << MUX3 | 0U << MUX2 | 1U << MUX1 | 1U << MUX0,
        CHANNEL_ADC4 = 0U << MUX3 | 1U << MUX2 | 0U << MUX1 | 0U << MUX0,
        CHANNEL_ADC5 = 0U << MUX3 | 1U << MUX2 | 0U << MUX1 | 1U << MUX0,
        CHANNEL_ADC6 = 0U << MUX3 | 1U << MUX2 | 1U << MUX1 | 0U << MUX0,
        CHANNEL_ADC7 = 0U << MUX3 | 1U << MUX2 | 1U << MUX1 | 1U << MUX0,
        CHANNEL_TEMP = 1U << MUX3 | 0U << MUX2 | 0U << MUX1 | 0U << MUX0,
        CHANNEL_RESERVED9 = 1U << MUX3 | 0U << MUX2 | 0U << MUX1 | 1U << MUX0,
        CHANNEL_RESERVED10 = 1U << MUX3 | 0U << MUX2 | 1U << MUX1 | 0U << MUX0,
        CHANNEL_RESERVED11 = 1U << MUX3 | 0U << MUX2 | 1U << MUX1 | 1U << MUX0,
        CHANNEL_RESERVED12 = 1U << MUX3 | 1U << MUX2 | 0U << MUX1 | 0U << MUX0,
        CHANNEL_RESERVED13 = 1U << MUX3 | 1U << MUX2 | 0U << MUX1 | 1U << MUX0,
        CHANNEL_VBG = 1U << MUX3 | 1U << MUX2 | 1U << MUX1 | 0U << MUX0,
        CHANNEL_GND = 1U << MUX3 | 1U << MUX2 | 1U << MUX1 | 1U << MUX0,
    };

    static void Startup(
            const ReferenceVoltage vref)
    {
        ADCSRA = 0U;
        ADCSRB = 0U;
        ADMUX = (vref & VREF_MASK) | 0U << ADLAR;

        // Discard the first, inaccurate conversion after switching reference voltage source.
        (void)ConvertChannel(CHANNEL_GND);

        ASSERT_ALWAYS(ConvertChannel(CHANNEL_GND) == 0U);
    }

    static void TriggerConversion(
            const Channel channel)
    {
        ADMUX = (ADMUX & ~CHANNEL_MASK) | (channel & CHANNEL_MASK);
        ADCSRA = 1U << ADEN | 1U << ADSC | 0U << ADATE | 1U << ADIF | 0U << ADIE
                | AdcUtils::Clock::GetPrescalerSelect();
    }

    static bool IsConversionInProgress()
    {
        return (ADCSRA & BIT_VAL(ADSC)) != 0U;
    }

    static bool IsConversionCompleted()
    {
        return (ADCSRA & BIT_VAL(ADIF)) != 0U;
    }

    static Sample_t GetConversionResult()
    {
        return ADCW;
    }

    static Channel GetSelectedChannel()
    {
        return static_cast<Channel>(ADMUX & CHANNEL_MASK);
    }

    static Sample_t ConvertChannel(
            const Channel channel)
    {
        TriggerConversion(channel);

        while (!IsConversionCompleted())
        {
            // Do nothing.
        }

        return GetConversionResult();
    }

private:
    static constexpr uint8_t VREF_MASK = BIT_VAL(REFS1) | BIT_VAL(REFS0);
    static constexpr uint8_t CHANNEL_MASK = BIT_VAL(MUX3)
            | BIT_VAL(MUX2) | BIT_VAL(MUX1) | BIT_VAL(MUX0);
};
