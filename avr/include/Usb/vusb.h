#pragma once

// @formatter:off

#if defined(__cplusplus)
extern "C" {
#endif

#if defined(__GNUC__) && defined(__AVR__)
    #include "usbdrv.h"
#else
    #error "Provide V-USB stubs!"
#endif

#if defined(__cplusplus)
}
#endif
