#pragma once

#include <util/delay.h>

#include "Gpio/Pin.hpp"

namespace Usb {

/// Detection of +5V USB Vbus voltage provided by the host.
class VbusSense
{
public:
    VbusSense() = delete;
    VbusSense(
            const VbusSense&) = delete;
    VbusSense(
            VbusSense&&) = delete;
    VbusSense& operator =(
            const VbusSense&) = delete;
    VbusSense& operator =(
            VbusSense&&) = delete;
    ~VbusSense() = delete;

    static void Startup()
    {
        DigitalSensePin::SetPort(false);
        DigitalSensePin::SetDdr(false);
    }

    /// @warning Returns true when ISP programmer is connected since VBUS_SNS is shared with MISO.
    static bool IsHostDetected()
    {
        // Discharge parasitic capacitance which could distort the measurement.
        DigitalSensePin::SetDdr(true);
        DigitalSensePin::SetDdr(false);

        // Wait about five filter time constants until pin capacitance (~10pF)
        // is recharged through the series resistor R9 (22k).
        _delay_us(2);

        // Read the pin state.
        return DigitalSensePin::GetPin();
    }

private:
    using DigitalSensePin = Gpio::Pin<'B', 5U>;
};

}
