#pragma once

namespace Usb {
namespace Hid {

/// Interface of a recipient of an output report targeted at Usb::Hid::Consumer.
template<typename TData>
class Sink
{
public:
    Sink() = default;
    Sink(
            const Sink&) = delete;
    Sink(
            Sink&&) = delete;
    Sink& operator =(
            const Sink&) = delete;
    Sink& operator =(
            Sink&&) = delete;
    virtual ~Sink() = default;

    virtual bool OnUpdate(
            const TData& rData) = 0;
};

}
}
