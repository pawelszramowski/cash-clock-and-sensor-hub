#pragma once

#include "Usb/vusb.h"

namespace Usb {
namespace Hid {

/// Interface used by Usb::Hid::ControlEp and Usb::Hid::IntrInEp to transfer HID reports.
class Report
{
public:
    Report() = default;
    Report(
            const Report&) = delete;
    Report(
            Report&&) = delete;
    Report& operator =(
            const Report&) = delete;
    Report& operator =(
            Report&&) = delete;
    virtual ~Report() = default;

    virtual void* GetRawData(
            usbMsgLen_t& rSize) = 0;

    virtual bool OnUpdate() = 0;
};

}
}
