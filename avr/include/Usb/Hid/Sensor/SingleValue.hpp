#pragma once

#include <stdint.h>

#include "Usb/Hid/Sensor/GenericSensor.hpp"
#include "Usb/Hid/Sensor/Usage.hpp"

namespace Usb {
namespace Hid {
namespace Sensor {

/// @warning It must be a POD so it must have a trivial default constructor (not user-provided).
template<typename TValue>
struct SingleValueCustomProperties final
{
    uint16_t m_ChangeSensitivity;
    TValue m_Maximum;
    TValue m_Minimum;
};

template<typename TValue>
using SingleValueBase = GenericSensor<TValue, SingleValueCustomProperties<TValue>>;

/// A sensor reporting a single analog value.
///
/// @note Non-virtual and non-static members must be accessed through this pointer
/// or introduced by using declaration due to a template base class. See:
/// - https://isocpp.org/wiki/faq/templates#nondependent-name-lookup-types
/// - https://isocpp.org/wiki/faq/templates#nondependent-name-lookup-members
/// - https://isocpp.org/wiki/faq/templates#nondependent-name-lookup-silent-bug
template<typename TValue>
class SingleValue: public SingleValueBase<TValue>
{
public:
    SingleValue() = delete;
    explicit SingleValue(
            const uint8_t reportId,
            const uint32_t minReportIntervalInMs,
            const uint32_t defaultReportIntervalInMs,
            const TValue& rInitialValue,
            const SingleValueCustomProperties<TValue>& rCustomProperties,
            const SensorState sensorState = SensorState::UNKNOWN,
            const ConnectionType connectionType = ConnectionType::PC_ATTACHED)
            : SingleValueBase<TValue>(reportId,
                                      minReportIntervalInMs,
                                      defaultReportIntervalInMs,
                                      rInitialValue,
                                      rCustomProperties,
                                      sensorState,
                                      connectionType),
              m_MinChangeSensitivity(rCustomProperties.m_ChangeSensitivity)
    {
        // Do nothing.
    }
    SingleValue(
            const SingleValue&) = delete;
    SingleValue(
            SingleValue&&) = delete;
    SingleValue& operator =(
            const SingleValue&) = delete;
    SingleValue& operator =(
            SingleValue&&) = delete;
    virtual ~SingleValue() = default;

    virtual bool OnUpdate(
            Report& rReport) override
    {
        (void)SingleValueBase<TValue>::OnUpdate(rReport);

        if (this->m_FeatureIn.m_Report.m_CustomProperties.m_ChangeSensitivity
                != this->m_FeatureOut.m_Report.m_CustomProperties.m_ChangeSensitivity)
        {
            this->m_FeatureIn.m_Report.m_CustomProperties.m_ChangeSensitivity =
                    (this->m_FeatureOut.m_Report.m_CustomProperties.m_ChangeSensitivity
                            >= m_MinChangeSensitivity) ?
                            this->m_FeatureOut.m_Report.m_CustomProperties.m_ChangeSensitivity :
                            m_MinChangeSensitivity;
            this->SetInsignificantEventType(EventType::PROPERTY_CHANGED);
        }

        return true;
    }

    uint16_t GetChangeSensitivity() const
    {
        return this->m_FeatureIn.m_Report.m_CustomProperties.m_ChangeSensitivity;
    }

    const TValue& GetValue() const
    {
        return this->m_Input.m_Report.m_SensorData;
    }

    void SetValue(
            const TValue& rValue)
    {
        if (this->m_Input.m_Report.m_SensorState == SensorState::READY)
        {
            if (GetValue() != rValue)
            {
                const uint16_t absDifference =
                        (GetValue() < rValue) ? (rValue - GetValue()) : (GetValue() - rValue);

                this->m_Input.m_Report.m_SensorData = rValue;
                this->SetInsignificantEventType(
                        (absDifference >= GetChangeSensitivity()) ?
                                                                    EventType::CHANGE_SENSITIVITY :
                                                                    EventType::DATA_UPDATED);
            }
        }
    }

private:
    const uint16_t m_MinChangeSensitivity;
};

}
}
}
