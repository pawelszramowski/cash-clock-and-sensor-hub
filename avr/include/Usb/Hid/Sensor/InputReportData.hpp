#pragma once

#include <stdint.h>

#include "common.h"

#include "Usb/Hid/Sensor/Usage.hpp"

namespace Usb {
namespace Hid {
namespace Sensor {

/// HID sensor input report data transmitted to host.
///
/// @warning It must be a POD so it must have a trivial default constructor (not user-provided).
template<typename TData>
struct InputReportData final
{
    using Data_t = TData;

    uint8_t m_Id;
    SensorState m_SensorState;
    EventType m_EventType;
    TData m_SensorData;

    bool IsIdValid() const
    {
        return (m_Id > 0U);
    }

    bool IsSensorStateValid() const
    {
        return (m_SensorState >= SensorState::UNKNOWN) && (m_SensorState <= SensorState::ERROR);
    }

    bool IsEventTypeValid() const
    {
        return (m_EventType >= EventType::UNKNOWN) && (m_EventType <= EventType::CHANGE_SENSITIVITY);
    }

    bool IsValid() const
    {
        return IsIdValid() && IsSensorStateValid() && IsEventTypeValid();
    }
}ATTR_PACKED;

}
}
}
