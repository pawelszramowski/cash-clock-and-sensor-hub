#pragma once

#include <stdint.h>

#include "common.h"

#include "Usb/Hid/Sensor/Usage.hpp"

namespace Usb {
namespace Hid {
namespace Sensor {

/// HID sensor feature report data exchanged with host.
///
/// @warning It must be a POD since it's exchanged with external devices
/// so it must have a trivial default constructor (not user-provided)!
template<typename TCustomProperties>
struct FeatureReportData final
{
    using CustomProperties_t = TCustomProperties;

    uint8_t m_Id;
    ConnectionType m_ConnectionType; ///< Read-only for host.
    ReportingState m_ReportingState; ///< Read/write for host.
    PowerState m_PowerState; ///< Read/write for host.
    SensorState m_SensorState; ///< Read-only for host.
    uint32_t m_ReportIntervalInMs; ///< Read-write for host.
    TCustomProperties m_CustomProperties; ///< Read-write for host.

    bool IsIdValid() const
    {
        return (m_Id > 0U);
    }

    bool IsConnectionTypeValid() const
    {
        return (m_ConnectionType >= ConnectionType::PC_INTEGRATED)
                && (m_ConnectionType <= ConnectionType::PC_EXTERNAL);
    }

    bool IsReportingStateValid() const
    {
        return (m_ReportingState == ReportingState::NO_EVENTS)
                || (m_ReportingState == ReportingState::ALL_EVENTS);
    }

    bool IsPowerStateValid() const
    {
        return (m_PowerState == PowerState::D0_FULL_POWER)
                || (m_PowerState == PowerState::D1_LOW_POWER)
                || (m_PowerState == PowerState::D4_POWER_OFF);
    }

    bool IsSensorStateValid() const
    {
        return (m_SensorState >= SensorState::UNKNOWN) && (m_SensorState <= SensorState::ERROR);
    }

    bool IsValid() const
    {
        return IsIdValid() && IsConnectionTypeValid() && IsReportingStateValid()
                && IsPowerStateValid() && IsSensorStateValid();
    }
}ATTR_PACKED;

}
}
}
