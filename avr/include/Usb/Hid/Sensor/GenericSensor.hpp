#pragma once

#include <avr/pgmspace.h>
#include <stdint.h>

#include "common.h"

#include "Tc/SystemTimer.hpp"
#include "Usb/Hid/Constants.hpp"
#include "Usb/Hid/Function.hpp"
#include "Usb/Hid/InReport.hpp"
#include "Usb/Hid/IntrInEp.hpp"
#include "Usb/Hid/OutReport.hpp"
#include "Usb/vusb.h"

#include "Usb/Hid/Sensor/FeatureReportData.hpp"
#include "Usb/Hid/Sensor/InputReportData.hpp"
#include "Usb/Hid/Sensor/Usage.hpp"

namespace Usb {
namespace Hid {
namespace Sensor {

/// Implementation of a logical HID sensor device as per HID Usage Tables Sensor Page.
///
/// @param TData
/// @param TCustomProperties Custom properties in a feature report. If the sensor
/// doesn't need custom configuration then add minimal report interval etc.
template<typename TData, typename TCustomProperties>
class GenericSensor: public Function
{
public:
    using Data_t = TData;
    using CustomProperties_t = TCustomProperties;

    GenericSensor() = delete;
    explicit GenericSensor(
            const uint8_t reportId,
            const uint32_t minReportIntervalInMs,
            const uint32_t defaultReportIntervalInMs,
            const TData& rInitialData,
            const TCustomProperties& rInitialCustomPropertiesP,
            const SensorState sensorState = SensorState::UNKNOWN,
            const ConnectionType connectionType = ConnectionType::PC_ATTACHED)
            :
            m_IntrInTimestampInMs(SystemTimer::GetTimestampInMs()),
            m_MinReportIntervalInMs(minReportIntervalInMs),
            m_DefaultReportIntervalInMs(defaultReportIntervalInMs),
            m_Input(),
            m_FeatureIn(),
            m_FeatureOut(*this)
    {
        ASSERT_ALWAYS(minReportIntervalInMs <= defaultReportIntervalInMs);
        ASSERT_ALWAYS(defaultReportIntervalInMs <= SystemTimer::MAX_USABLE_TIME_DELTA_IN_MS);

        m_Input.m_Report.m_Id = reportId;
        m_Input.m_Report.m_SensorState = sensorState;
        m_Input.m_Report.m_EventType = EventType::UNKNOWN;
        m_Input.m_Report.m_SensorData = rInitialData;
        ASSERT_ALWAYS(m_Input.m_Report.IsValid());

        m_FeatureIn.m_Report.m_Id = reportId;
        m_FeatureIn.m_Report.m_ConnectionType = connectionType;
        m_FeatureIn.m_Report.m_ReportingState = ReportingState::ALL_EVENTS;
        m_FeatureIn.m_Report.m_PowerState = PowerState::D0_FULL_POWER;
        m_FeatureIn.m_Report.m_SensorState = SensorState::UNKNOWN;
        m_FeatureIn.m_Report.m_ReportIntervalInMs = defaultReportIntervalInMs;
        (void)memcpy_P(&m_FeatureIn.m_Report.m_CustomProperties, &rInitialCustomPropertiesP,
                       sizeof(TCustomProperties));
        ASSERT_ALWAYS(m_FeatureIn.m_Report.IsValid());
    }
    GenericSensor(
            const GenericSensor&) = delete;
    GenericSensor(
            GenericSensor&&) = delete;
    GenericSensor& operator =(
            const GenericSensor&) = delete;
    GenericSensor& operator =(
            GenericSensor&&) = delete;
    virtual ~GenericSensor() = default;

    virtual Report* GetReport(
            const uint8_t classRequest,
            const uint8_t reportType,
            const uint8_t reportId) final override
    {
        Report* pReport = nullptr;

        if (classRequest == GET_REPORT)
        {
            if ((reportType == INPUT_REPORT) && (reportId == m_Input.m_Report.m_Id))
            {
                m_Input.m_Report.m_EventType = EventType::POLL_RESPONSE;
                pReport = &m_Input;
            }
            else if ((reportType == FEATURE_REPORT) && (reportId == m_FeatureIn.m_Report.m_Id))
            {
                pReport = &m_FeatureIn;
            }
        }
        else if (classRequest == SET_REPORT)
        {
            if ((reportType == FEATURE_REPORT) && (reportId == m_FeatureIn.m_Report.m_Id))
            {
                pReport = &m_FeatureOut;
            }
        }

        return pReport;
    }

    virtual bool OnIntrInEpReady(
            IntrInEp& rIntrInEp) final override
    {
        bool reportScheduled;

        if (m_Input.m_Report.m_EventType == EventType::POLL_RESPONSE)
        {
            m_Input.m_Report.m_EventType = EventType::UNKNOWN;
        }

        const uint16_t currentTimestampInMs = SystemTimer::GetTimestampInMs();
        if ((m_Input.m_Report.m_EventType == EventType::STATE_CHANGED)
                || (m_Input.m_Report.m_EventType == EventType::CHANGE_SENSITIVITY)
                || ((m_FeatureIn.m_Report.m_ReportingState == ReportingState::ALL_EVENTS)
                        && ((currentTimestampInMs - m_IntrInTimestampInMs)
                                >= m_FeatureIn.m_Report.m_ReportIntervalInMs)))
        {
            m_IntrInTimestampInMs = currentTimestampInMs;

            reportScheduled = rIntrInEp.ScheduleReport(m_Input);
            ASSERT_ALWAYS(reportScheduled);
            m_Input.m_Report.m_EventType = EventType::UNKNOWN;
        }
        else
        {
            reportScheduled = false;
        }

        return reportScheduled;
    }

    virtual bool OnUpdate(
            Report& rReport) override
    {
        ASSERT_ALWAYS(&rReport == &m_FeatureOut);

        if ((m_FeatureIn.m_Report.m_ReportingState != m_FeatureOut.m_Report.m_ReportingState)
                && m_FeatureOut.m_Report.IsReportingStateValid())
        {
            SetReportingState(m_FeatureOut.m_Report.m_ReportingState);
        }
        if ((m_FeatureIn.m_Report.m_PowerState != m_FeatureOut.m_Report.m_PowerState)
                && m_FeatureOut.m_Report.IsPowerStateValid())
        {
            SetPowerState(m_FeatureOut.m_Report.m_PowerState);
        }
        if (m_FeatureIn.m_Report.m_ReportIntervalInMs != m_FeatureOut.m_Report.m_ReportIntervalInMs)
        {
            SetReportIntervalInMs(m_FeatureOut.m_Report.m_ReportIntervalInMs);
        }

        return true;
    }

    static constexpr usbMsgLen_t GetMaxControlEpInDataSize()
    {
        return sizeof(m_FeatureIn.m_Report);
    }

    static constexpr usbMsgLen_t GetMaxIntrInEpDataSize()
    {
        return sizeof(m_Input.m_Report);
    }

    ReportingState GetReportingState() const
    {
        return m_FeatureIn.m_Report.m_ReportingState;
    }

    PowerState GetPowerState() const
    {
        return m_FeatureIn.m_Report.m_PowerState;
    }

    SensorState GetSensorState() const
    {
        return m_FeatureIn.m_Report.m_SensorState;
    }

    uint32_t GetReportIntervalInMs() const
    {
        return m_FeatureIn.m_Report.m_ReportIntervalInMs;
    }

    void SetSensorState(
            const SensorState state)
    {
        if (m_FeatureIn.m_Report.m_SensorState != state)
        {
            m_FeatureIn.m_Report.m_SensorState = state;
            m_Input.m_Report.m_SensorState = state;
            m_Input.m_Report.m_EventType = EventType::STATE_CHANGED;
        }
    }

protected:
    void SetReportingState(
            const ReportingState reportingState)
    {
        m_FeatureIn.m_Report.m_ReportingState = reportingState;
        SetInsignificantEventType(EventType::PROPERTY_CHANGED);
    }

    void SetPowerState(
            const PowerState powerState)
    {
        m_FeatureIn.m_Report.m_PowerState = powerState;
        SetInsignificantEventType(EventType::PROPERTY_CHANGED);
    }

    virtual void SetReportIntervalInMs(
            const uint32_t reportIntervalInMs)
    {
        if (reportIntervalInMs == 0UL)
        {
            m_FeatureIn.m_Report.m_ReportIntervalInMs = m_DefaultReportIntervalInMs;
        }
        else if (reportIntervalInMs < m_MinReportIntervalInMs)
        {
            m_FeatureIn.m_Report.m_ReportIntervalInMs = m_MinReportIntervalInMs;
        }
        else if (reportIntervalInMs > SystemTimer::MAX_USABLE_TIME_DELTA_IN_MS)
        {
            m_FeatureIn.m_Report.m_ReportIntervalInMs = SystemTimer::MAX_USABLE_TIME_DELTA_IN_MS;
        }
        else
        {
            m_FeatureIn.m_Report.m_ReportIntervalInMs = reportIntervalInMs;
        }
        SetInsignificantEventType(EventType::PROPERTY_CHANGED);
    }

    void SetInsignificantEventType(
            const EventType eventType)
    {
        if (m_Input.m_Report.m_EventType != EventType::STATE_CHANGED)
        {
            m_Input.m_Report.m_EventType = eventType;
        }
    }

    uint16_t m_IntrInTimestampInMs;
    const uint32_t m_MinReportIntervalInMs;
    const uint32_t m_DefaultReportIntervalInMs;

    InReport<InputReportData<TData>> m_Input;
    InReport<FeatureReportData<TCustomProperties>> m_FeatureIn;
    OutReport<FeatureReportData<TCustomProperties>> m_FeatureOut;
};

}
}
}
