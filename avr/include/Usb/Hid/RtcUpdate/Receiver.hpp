#pragma once

#include "Usb/Hid/Consumer.hpp"

#include "Usb/Hid/RtcUpdate/OutputReportData.hpp"

namespace Usb {
namespace Hid {
namespace RtcUpdate {

/// Consumer of an output report with time and date for a real-time clock.
using Receiver = Consumer<OutputReportData>;

}
}
}
