#pragma once

#include "Rtc.hpp"
#include "Usb/Hid/Consumer.hpp"
#include "Usb/Hid/Sink.hpp"

#include "Usb/Hid/RtcUpdate/OutputReportData.hpp"
#include "Usb/Hid/RtcUpdate/Receiver.hpp"

namespace Usb {
namespace Hid {
namespace RtcUpdate {

/// Implementation of Usb::Hid::Sink which updates time and date in a real-time clock
/// according to data in an output report.
class Performer: public Sink<OutputReportData>
{
public:
    Performer() = delete;
    explicit Performer(
            Receiver& rReceiver)
    {
        rReceiver.SetSink(this);
    }
    Performer(
            const Performer&) = delete;
    Performer(
            Performer&&) = delete;
    Performer& operator =(
            const Performer&) = delete;
    Performer& operator =(
            Performer&&) = delete;
    virtual ~Performer() = default;

    virtual bool OnUpdate(
            const OutputReportData& rData) override
    {
        return Rtc::SetTimeDate(rData.m_Time, rData.m_Date);
    }
};

}
}
}
