#pragma once

#include <stdint.h>
#include <string.h>

#include "common.h"
#include "common.hpp"
#include "Usb/Hid/Constants.hpp"
#include "Usb/Hid/Device.hpp"
#include "Usb/Hid/Function.hpp"
#include "Usb/Hid/InReport.hpp"
#include "Usb/Hid/IntrInEp.hpp"
#include "Usb/Hid/OutReport.hpp"
#include "Usb/vusb.h"

namespace Usb {
namespace Hid {
namespace Test {

template<typename TFeatureReportData, typename TInputReportData>
class Loopback: public Device
{
public:
    Loopback()
            : m_Counter(0U),
              m_FeatureOut(*this),
              m_FeatureIn(),
              m_Input()
    {
        // Do nothing.
    }
    Loopback(
            const Loopback&) = delete;
    Loopback(
            Loopback&&) = delete;
    Loopback& operator =(
            const Loopback&) = delete;
    Loopback& operator =(
            Loopback&&) = delete;
    virtual ~Loopback() = default;

    virtual Report* GetReport(
            const uint8_t classRequest,
            const uint8_t reportType,
            const uint8_t) override
    {
        if (classRequest == GET_REPORT)
        {
            if (reportType == INPUT_REPORT)
            {
                return &m_Input;
            }
            else if (reportType == FEATURE_REPORT)
            {
                return &m_FeatureIn;
            }
        }
        else if (classRequest == SET_REPORT)
        {
            if (reportType == FEATURE_REPORT)
            {
                return &m_FeatureOut;
            }
        }
        return nullptr;
    }

    virtual bool OnIntrInEpReady(
            IntrInEp& rIntrInEp) override
    {
        ASSERT_ALWAYS(rIntrInEp.ScheduleReport(m_Input));
        (void)memset(&m_Input.m_Report, m_Counter++, sizeof(TInputReportData));
        return true;
    }

    virtual void OnUpdate(
            const Report& rReport) override
    {
        ASSERT_ALWAYS(&rReport == &m_FeatureOut);
        (void)memcpy(&m_FeatureIn.m_Report, &m_FeatureOut.m_Report, sizeof(TFeatureReportData));
    }

    virtual uint8_t* GetControlEpInBuffer(
            usbMsgLen_t& rMaxDataSize) override
    {
        rMaxDataSize = sizeof(m_ControlEpInBuffer);
        return &m_ControlEpInBuffer[0];
    }

    virtual uint8_t* GetIntrInEpBuffer(
            usbMsgLen_t& rMaxDataSize) override
    {
        rMaxDataSize = sizeof(m_IntrInEpBuffer);
        return &m_IntrInEpBuffer[0];
    }

private:
    uint8_t m_Counter;

    OutReport<TFeatureReportData> m_FeatureOut;
    InReport<TFeatureReportData> m_FeatureIn;
    InReport<TInputReportData> m_Input;

    uint8_t m_ControlEpInBuffer[nonstd::maxv(sizeof(TFeatureReportData), sizeof(TInputReportData))];
    uint8_t m_IntrInEpBuffer[sizeof(TInputReportData)];
};

}
}
}
