#pragma once

#include <stdint.h>
#include <string.h>

#include "common.h"
#include "Usb/vusb.h"

#include "Usb/Hid/Constants.hpp"
#include "Usb/Hid/Device.hpp"
#include "Usb/Hid/Report.hpp"

namespace Usb {
namespace Hid {

/// HID implementation of Control endpoint (EP0) which handles IN and OUT transactions.
class ControlEp
{
    friend usbMsgLen_t ::usbFunctionSetup(
            uchar data[8]);
    friend uchar ::usbFunctionWrite(
            uchar *data,
            uchar len);
    friend uchar ::usbFunctionRead(
            uchar *data,
            uchar len);

public:
    ControlEp() = delete;
    ControlEp(
            const ControlEp&) = delete;
    ControlEp(
            ControlEp&&) = delete;
    ControlEp& operator =(
            const ControlEp&) = delete;
    ControlEp& operator =(
            ControlEp&&) = delete;
    ~ControlEp() = delete;

    static void Startup(
            Device& rDevice)
    {
        ASSERT_ALWAYS(m_pDevice == nullptr);

        m_pReport = nullptr;
        m_IsStalled = false;

        m_pCurrentByte = nullptr;
        m_BytesLeft = 0U;

        m_pInBuffer = rDevice.GetControlEpInBuffer(m_InBufferSize);

        m_pDevice = &rDevice;

        usbInit();
        usbDeviceConnect();
    }

    static void Poll()
    {
        ASSERT_ALWAYS(m_pDevice != nullptr);

        usbPoll();
    }

    static void Shutdown()
    {
        ASSERT_ALWAYS(m_pDevice != nullptr);

        usbDeviceDisconnect();

        m_pDevice = nullptr;

        m_pReport = nullptr;
        m_IsStalled = false;

        m_pInBuffer = nullptr;
        m_InBufferSize = 0U;

        m_pCurrentByte = nullptr;
        m_BytesLeft = 0U;

    }

private:
    static usbMsgLen_t HandleSetupTransaction(
            const usbRequest_t * const pRequest)
    {
        m_pReport = nullptr;
        m_IsStalled = false;

        usbMsgLen_t status = IGNORE_REQUEST;

        if (((pRequest->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS)
                && (m_pDevice != nullptr))
        {
            const auto reportType = pRequest->wValue.bytes[1];
            const auto reportId = pRequest->wValue.bytes[0];
            const auto reportLength = pRequest->wLength.word;

            if (pRequest->bRequest == GET_REPORT)
            {
                Report* const pReport = m_pDevice->GetReport(GET_REPORT, reportType, reportId);

                if (pReport != nullptr)
                {
                    ASSERT_ALWAYS(m_pInBuffer != nullptr);

                    usbMsgLen_t dataSize = 0U;
                    void* const pData = pReport->GetRawData(dataSize);

                    ASSERT_ALWAYS(pData != nullptr);
                    ASSERT_ALWAYS(dataSize <= m_InBufferSize);

                    if (dataSize == reportLength)
                    {
                        m_pCurrentByte = static_cast<uint8_t*>(memcpy(m_pInBuffer, pData, dataSize));
                        m_BytesLeft = dataSize;

                        m_pReport = pReport;
                    }
                    else
                    {
                        m_IsStalled = true;
                    }

                    status = CALL_FN_RW;
                }
            }
            else if (pRequest->bRequest == SET_REPORT)
            {
                Report* const pReport = m_pDevice->GetReport(SET_REPORT, reportType, reportId);

                if (pReport != nullptr)
                {
                    usbMsgLen_t bufferSize = 0U;
                    void* const pBuffer = pReport->GetRawData(bufferSize);

                    ASSERT_ALWAYS(pBuffer != nullptr);

                    if (bufferSize == reportLength)
                    {
                        m_pCurrentByte = static_cast<uint8_t*>(pBuffer);
                        m_BytesLeft = bufferSize;

                        m_pReport = pReport;
                    }
                    else
                    {
                        m_IsStalled = true;
                    }

                    status = CALL_FN_RW;
                }
            }
        }

        return status;
    }

    static uint8_t HandleOutTransaction(
            const uint8_t* pData,
            const uint8_t receivedDataSize)
    {
        uint8_t status;

        if (m_IsStalled)
        {
            status = PROCESSING_ERROR;
        }
        else if (m_pReport != nullptr)
        {
            const uint8_t storedDataSize =
                    (receivedDataSize > m_BytesLeft) ? m_BytesLeft : receivedDataSize;

            (void)memcpy(m_pCurrentByte, pData, storedDataSize);
            m_pCurrentByte += storedDataSize;
            m_BytesLeft -= storedDataSize;

            if (m_BytesLeft == 0U)
            {
                const bool isDataValid = m_pReport->OnUpdate();
                m_pReport = nullptr;
                m_IsStalled = !isDataValid;

                status = isDataValid ? ALL_DATA_RECEIVED : PROCESSING_ERROR;

            }
            else
            {
                status = MORE_DATA_EXPECTED;
            }
        }
        else
        {
            status = ALL_DATA_RECEIVED;
        }

        return status;
    }

    static uint8_t HandleInTransaction(
            uint8_t* pBuffer,
            const uint8_t requestedDataSize)
    {
        uint8_t loadedDataSize;

        if (m_IsStalled)
        {
            loadedDataSize = PROCESSING_ERROR;
        }
        else if (m_pReport != nullptr)
        {
            loadedDataSize = (requestedDataSize > m_BytesLeft) ? m_BytesLeft : requestedDataSize;

            (void)memcpy(pBuffer, m_pCurrentByte, loadedDataSize);
            m_pCurrentByte += loadedDataSize;
            m_BytesLeft -= loadedDataSize;

            if (m_BytesLeft == 0U)
            {
                m_pReport = nullptr;
            }
        }
        else
        {
            loadedDataSize = 0U;
        }

        return loadedDataSize;
    }

    static constexpr uint8_t MORE_DATA_EXPECTED = 0U;
    static constexpr uint8_t ALL_DATA_RECEIVED = 1U;
    static constexpr uint8_t PROCESSING_ERROR = 0xFFU;

    static constexpr usbMsgLen_t IGNORE_REQUEST = 0U;
    static constexpr usbMsgLen_t CALL_FN_RW = USB_NO_MSG;

    static Device* m_pDevice;
    static uint8_t* m_pInBuffer;
    static usbMsgLen_t m_InBufferSize;

    static Report* m_pReport;
    static bool m_IsStalled;

    static uint8_t* m_pCurrentByte;
    static usbMsgLen_t m_BytesLeft;
};

}
}
