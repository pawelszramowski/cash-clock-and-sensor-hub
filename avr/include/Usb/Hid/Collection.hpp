#pragma once

// @formatter:off

#include <stdint.h>

#include "common.h"
#include "common.hpp"
#include "Usb/vusb.h"

#include "common.hpp"
#include "Usb/Hid/CollectionConfig.hpp"
#include "Usb/Hid/Device.hpp"

namespace Usb {
namespace Hid {

/// Implementation of Usb::Hid::Device which aggregates multiple Usb::Hid::Function's.
class Collection: public Device
{
public:
    Collection() :
            #define CALL_CTOR(type, name, ...) name(__VA_ARGS__),
            #define SEPARATOR
            FOR_EACH_FUNCTION_IN_COLLECTION(CALL_CTOR, SEPARATOR)
            #undef SEPARATOR
            #undef CALL_CTOR
            m_PreviousIntrInEpUserReportId{0U}
    {
        uint8_t previousId = 0U;

        #define VERIFY_ID_IS_INCREASING(type, name, ...)                                        \
        do {                                                                                    \
            static constexpr uint8_t currentId = VA_GET_1ST_ARG(__VA_ARGS__);                   \
            ASSERT_ALWAYS(previousId < currentId);                                              \
            previousId = currentId;                                                             \
        } while (false);
        #define SEPARATOR
        FOR_EACH_FUNCTION_IN_COLLECTION(VERIFY_ID_IS_INCREASING, SEPARATOR)
        #undef SEPARATOR
        #undef VERIFY_ID_IS_INCREASING
    }
    Collection(
            const Collection&) = delete;
    Collection(
            Collection&&) = delete;
    Collection& operator =(
            const Collection&) = delete;
    Collection& operator =(
            Collection&&) = delete;
    ~Collection() = default;

    virtual Report* GetReport(
            const uint8_t classRequest,
            const uint8_t reportType,
            const uint8_t reportId) override
    {
        #define ATTEMPT_TO_GET_REPORT(type, name, ...)                                          \
        do {                                                                                    \
            Report* const pReport = name.GetReport(classRequest, reportType, reportId);         \
            if (pReport != nullptr)                                                             \
            {                                                                                   \
                return pReport;                                                                 \
            }                                                                                   \
        } while (false);
        #define SEPARATOR
        FOR_EACH_FUNCTION_IN_COLLECTION(ATTEMPT_TO_GET_REPORT, SEPARATOR)
        #undef SEPARATOR
        #undef ATTEMPT_TO_GET_REPORT

        return nullptr;
    }

    virtual bool OnIntrInEpReady(
            IntrInEp& rIntrInEp) override
    {
        #define NOTIFY_INTR_EP_READY(type, name, ...)                                           \
        do {                                                                                    \
            static constexpr uint8_t currentId = VA_GET_1ST_ARG(__VA_ARGS__);                   \
            if (m_PreviousIntrInEpUserReportId < currentId)                                     \
            {                                                                                   \
                m_PreviousIntrInEpUserReportId = currentId;                                     \
                return name.OnIntrInEpReady(rIntrInEp);                                         \
            }                                                                                   \
        } while (false);
        #define SEPARATOR
        FOR_EACH_FUNCTION_IN_COLLECTION(NOTIFY_INTR_EP_READY, SEPARATOR)
        #undef SEPARATOR
        #undef NOTIFY_INTR_EP_READY

        m_PreviousIntrInEpUserReportId = 0U;
        return false;
    }

    virtual bool OnUpdate(
            Report&) override
    {
        // Collection doesn't directly own any reports.
        ASSERT_ALWAYS(false);
        return false;
    }

    virtual uint8_t* GetControlEpInBuffer(
            usbMsgLen_t& rMaxDataSize) override
    {
        rMaxDataSize = MAX_CONTROL_EP_IN_DATA_SIZE;
        return &m_ControlEpInBuffer[0];
    }

    virtual uint8_t* GetIntrInEpBuffer(
            usbMsgLen_t& rMaxDataSize) override
    {
        rMaxDataSize = MAX_INTR_IN_EP_DATA_SIZE;
        return &m_IntrInEpBuffer[0];
    }

    #define DECLARE_DATA_MEMBER(type, name, ...) type name;
    #define SEPARATOR
    FOR_EACH_FUNCTION_IN_COLLECTION(DECLARE_DATA_MEMBER, SEPARATOR)
    #undef SEPARATOR
    #undef DECLARE_DATA_MEMBER

private:
    uint8_t m_PreviousIntrInEpUserReportId;

    #define GET_MAX_CONTROL_EP_IN_DATA_SIZE(type, name, ...) type::GetMaxControlEpInDataSize()
    #define SEPARATOR ,
    static constexpr usbMsgLen_t MAX_CONTROL_EP_IN_DATA_SIZE =
            nonstd::maxv(FOR_EACH_FUNCTION_IN_COLLECTION(GET_MAX_CONTROL_EP_IN_DATA_SIZE, SEPARATOR));
    #undef SEPARATOR
    #undef GET_MAX_CONTROL_EP_IN_DATA_SIZE

    #define GET_MAX_INTR_IN_EP_DATA_SIZE(type, name, ...) type::GetMaxIntrInEpDataSize()
    #define SEPARATOR ,
    static constexpr usbMsgLen_t MAX_INTR_IN_EP_DATA_SIZE =
            nonstd::maxv(FOR_EACH_FUNCTION_IN_COLLECTION(GET_MAX_INTR_IN_EP_DATA_SIZE, SEPARATOR));
    #undef SEPARATOR
    #undef GET_MAX_INTR_IN_EP_DATA_SIZE

    uint8_t m_ControlEpInBuffer[MAX_CONTROL_EP_IN_DATA_SIZE];
    uint8_t m_IntrInEpBuffer[MAX_INTR_IN_EP_DATA_SIZE];
};

}
}
