#pragma once

#include <stdbool.h>

#include "Usb/Hid/Function.hpp"
#include "Usb/Hid/Report.hpp"
#include "Usb/vusb.h"

namespace Usb {
namespace Hid {

/// Implementation of Usb::Hid::Report provided by Usb::Hid::Function to Usb::Hid::ControlEp
/// for OUT transactions (host to device).
template<typename TReportData>
class OutReport: public Report
{
public:
    explicit OutReport(
            Function& rOwner)
            : m_Report(),
              m_pOwner(&rOwner)
    {
        // Do nothing.
    }
    OutReport(
            const OutReport&) = delete;
    OutReport(
            OutReport&&) = delete;
    OutReport& operator =(
            const OutReport&) = delete;
    OutReport& operator =(
            OutReport&&) = delete;
    virtual ~OutReport() = default;

    virtual void* GetRawData(
            usbMsgLen_t& rSize) override
    {
        rSize = sizeof(m_Report);
        return &m_Report;
    }

    virtual bool OnUpdate() override
    {
        return m_pOwner->OnUpdate(*this);
    }

    TReportData m_Report;

private:
    // TODO: Investigate why using a reference instead of a pointer generates a warning.
    // The warning is "ignoring packed attribute because of unpacked non-POD field".
    Function* const m_pOwner;
};

}
}
