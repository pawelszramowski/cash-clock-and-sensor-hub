#pragma once

#include <stdint.h>
#include <string.h>

#include "common.h"
#include "Usb/vusb.h"

#include "Usb/Hid/Device.hpp"
#include "Usb/Hid/Report.hpp"

namespace Usb {
namespace Hid {

/// HID implementation of mandatory Interrupt In endpoint.
class IntrInEp
{
public:
    IntrInEp() = delete;
    explicit IntrInEp(
            Device& rDevice)
            :
            m_pDevice(&rDevice),
            m_pReport(nullptr),
            m_pBuffer(rDevice.GetIntrInEpBuffer(m_BufferSize)),
            m_pCurrentByte(nullptr),
            m_BytesLeft(0U),
            m_SendZeroLengthPacket(false)
    {
        ASSERT_ALWAYS(m_pBuffer != nullptr);
        ASSERT_ALWAYS(m_BufferSize > 0U);
    }
    IntrInEp(
            const IntrInEp&) = delete;
    IntrInEp(
            IntrInEp&&) = delete;
    IntrInEp& operator =(
            const IntrInEp&) = delete;
    IntrInEp& operator =(
            IntrInEp&&) = delete;
    ~IntrInEp() = default;

    bool ScheduleReport(
            Report& rReport)
    {
        bool reportScheduled;

        if (m_pReport == nullptr)
        {
            usbMsgLen_t dataSize = 0U;
            void* const pData = rReport.GetRawData(dataSize);

            ASSERT_ALWAYS(pData != nullptr);
            ASSERT_ALWAYS(dataSize <= m_BufferSize);

            m_pCurrentByte = static_cast<uint8_t*>(memcpy(m_pBuffer, pData, dataSize));
            m_BytesLeft = dataSize;
            m_SendZeroLengthPacket = (dataSize < m_BufferSize)
                    && (dataSize % MAX_PACKET_SIZE == 0U);

            m_pReport = &rReport;

            reportScheduled = true;
        }
        else
        {
            reportScheduled = false;
        }

        return reportScheduled;
    }

    void Poll()
    {
        if (usbInterruptIsReady())
        {
            if (m_pReport == nullptr)
            {
                (void)m_pDevice->OnIntrInEpReady(*this);
            }

            // Check the condition again, since OnIntrInEpReady() call
            // might have triggered ScheduleReport() call.
            if (m_pReport != nullptr)
            {
                const uint8_t loadedDataSize =
                        (m_BytesLeft > MAX_PACKET_SIZE) ? MAX_PACKET_SIZE : m_BytesLeft;
                usbSetInterrupt(m_pCurrentByte, loadedDataSize);
                m_pCurrentByte += loadedDataSize;
                m_BytesLeft -= loadedDataSize;

                if (m_BytesLeft == 0U)
                {
                    if (m_SendZeroLengthPacket)
                    {
                        m_SendZeroLengthPacket = false;
                    }
                    else
                    {
                        m_pReport = nullptr;
                    }
                }
            }
        }
    }

private:
    static constexpr uint8_t MAX_PACKET_SIZE = 8U;

    Device* m_pDevice;
    Report* m_pReport;

    uint8_t* const m_pBuffer;
    usbMsgLen_t m_BufferSize;

    uint8_t* m_pCurrentByte;
    usbMsgLen_t m_BytesLeft;
    bool m_SendZeroLengthPacket;
};

}
}
