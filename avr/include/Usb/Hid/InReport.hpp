#pragma once

#include "common.h"

#include "Usb/Hid/Report.hpp"
#include "Usb/vusb.h"

namespace Usb {
namespace Hid {

/// Implementation of Usb::Hid::Report provided by Usb::Hid::Function to Usb::Hid::ControlEp
/// and Usb::Hid::IntrInEp for IN transactions (device to host).
template<typename TReportData>
class InReport: public Report
{
public:
    InReport()
            : m_Report()
    {
        // Do nothing.
    }
    InReport(
            const InReport&) = delete;
    InReport(
            InReport&&) = delete;
    InReport& operator =(
            const InReport&) = delete;
    InReport& operator =(
            InReport&&) = delete;
    virtual ~InReport() = default;

    virtual void* GetRawData(
            usbMsgLen_t& rSize) override
    {
        rSize = sizeof(m_Report);
        return &m_Report;
    }

    virtual bool OnUpdate() override
    {
        ASSERT_ALWAYS(false);
        return false;
    }

    TReportData m_Report;
};

}
}
