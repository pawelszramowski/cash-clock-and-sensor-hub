#pragma once

#include <stdint.h>

#include "Usb/vusb.h"

#include "Usb/Hid/Function.hpp"

namespace Usb {
namespace Hid {

/// Interface between endpoints and Usb::Hid::Function's which also provides buffers
/// used to store IN transaction data during transmission to USB host.
///
/// Buffering is used to guarantee integrity and prevent blocking during
/// transmission to host.
class Device: public Function
{
public:
    Device() = default;
    Device(
            const Device&) = delete;
    Device(
            Device&&) = delete;
    Device& operator =(
            const Device&) = delete;
    Device& operator =(
            Device&&) = delete;
    virtual ~Device() = default;

    virtual uint8_t* GetControlEpInBuffer(
            usbMsgLen_t& rMaxDataSize) = 0;

    virtual uint8_t* GetIntrInEpBuffer(
            usbMsgLen_t& rMaxDataSize) = 0;
};

}
}
