#pragma once

#include <stdint.h>

#include "Usb/vusb.h"

namespace Usb {
namespace Hid {

class IntrInEp;
class Report;

/// Interface of a single functionality exposed by HID device.
class Function
{
public:
    Function() = default;
    Function(
            const Function&) = delete;
    Function(
            Function&&) = delete;
    Function& operator =(
            const Function&) = delete;
    Function& operator =(
            Function&&) = delete;
    virtual ~Function() = default;

    virtual Report* GetReport(
            const uint8_t classRequest,
            const uint8_t reportType,
            const uint8_t reportId) = 0;

    virtual bool OnIntrInEpReady(
            IntrInEp& rIntrInEp) = 0;

    virtual bool OnUpdate(
            Report& rReport) = 0;

    static constexpr usbMsgLen_t GetMaxControlEpInDataSize() = delete;
    static constexpr usbMsgLen_t GetMaxIntrInEpDataSize() = delete;

protected:
    static constexpr usbMsgLen_t NO_IN_SUPPORT = 0U;
};

}
}
