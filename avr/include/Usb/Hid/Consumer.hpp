#pragma once

#include <stdint.h>

#include "common.h"
#include "Usb/vusb.h"

#include "Usb/Hid/Constants.hpp"
#include "Usb/Hid/Function.hpp"
#include "Usb/Hid/OutReport.hpp"
#include "Usb/Hid/Sink.hpp"

namespace Usb {
namespace Hid {

/// Implementation of Usb::Hid::Function which consumes an output report produced by USB host
/// and passes it to a Usb::Hid::Sink for processing.
template<typename TReportData>
class Consumer: public Function
{
public:
    Consumer() = delete;
    explicit Consumer(
            const uint8_t reportId)
            :
            m_ReportId(reportId),
            m_pSink(nullptr),
            m_Output(*this)
    {
        // Do nothing.
    }
    Consumer(
            const Consumer&) = delete;
    Consumer(
            Consumer&&) = delete;
    Consumer& operator =(
            const Consumer&) = delete;
    Consumer& operator =(
            Consumer&&) = delete;
    virtual ~Consumer() = default;

    virtual Report* GetReport(
            const uint8_t classRequest,
            const uint8_t reportType,
            const uint8_t reportId) override
    {
        Report* pReport;

        if ((classRequest == SET_REPORT) && (reportType == OUTPUT_REPORT)
                && (reportId == m_ReportId))
        {
            pReport = &m_Output;
        }
        else
        {
            pReport = nullptr;
        }

        return pReport;
    }

    virtual bool OnIntrInEpReady(
            IntrInEp&) override
    {
        return false;
    }

    virtual bool OnUpdate(
            Report& rReport) override
    {
        ASSERT_ALWAYS(&rReport == &m_Output);
        ASSERT_ALWAYS(m_pSink != nullptr);

        return m_pSink->OnUpdate(m_Output.m_Report);
    }

    static constexpr usbMsgLen_t GetMaxControlEpInDataSize()
    {
        return NO_IN_SUPPORT;
    }

    static constexpr usbMsgLen_t GetMaxIntrInEpDataSize()
    {
        return NO_IN_SUPPORT;
    }

    void SetSink(
            Sink<TReportData>* const pSink)
    {
        m_pSink = pSink;
    }

private:
    const uint8_t m_ReportId;
    Sink<TReportData>* m_pSink;
    OutReport<TReportData> m_Output;
};

}
}
