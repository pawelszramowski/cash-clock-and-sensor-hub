#pragma once

#include <avr/pgmspace.h>

#include "Sensors.hpp"
#include "Usb/Hid/reportIds.h"
#include "Usb/Hid/RtcUpdate/Receiver.hpp"
#include "Usb/Hid/Sensor/SingleValue.hpp"

namespace Usb {
namespace Hid {
namespace Sensor {

using Environmental = SingleValue<uint16_t>;

namespace AtmosphericPressure {

static constexpr Sensors::AtmosphericPressure::Value_t VALUE_UNIT_IN_PA = 10U;

static constexpr Environmental::Data_t ConvertToSensorValue(
        const Sensors::AtmosphericPressure::Value_t valueInPa)
{
    return (valueInPa + VALUE_UNIT_IN_PA / 2U) / VALUE_UNIT_IN_PA;
}

static constexpr Environmental::CustomProperties_t CUSTOM_PROPERTIES PROGMEM = {
        ConvertToSensorValue(Sensors::AtmosphericPressure::RESOLUTION_IN_PA),
        ConvertToSensorValue(Sensors::AtmosphericPressure::MAXIMUM_VALUE_IN_PA),
        ConvertToSensorValue(Sensors::AtmosphericPressure::MINIMUM_VALUE_IN_PA) };

}

namespace Temperature {

static constexpr Environmental::Data_t ConvertToSensorValue(
        const Sensors::Temperature::Value_t valueIn0p01DegC)
{
    return valueIn0p01DegC;
}

static constexpr Environmental::CustomProperties_t CUSTOM_PROPERTIES PROGMEM = {
        ConvertToSensorValue(Sensors::Temperature::RESOLUTION_IN_0P01_DEG_C),
        ConvertToSensorValue(Sensors::Temperature::MAXIMUM_VALUE_IN_0P01_DEG_C),
        ConvertToSensorValue(Sensors::Temperature::MINIMUM_VALUE_IN_0P01_DEG_C) };

}

namespace RelativeHumidity {

static constexpr Environmental::Data_t ConvertToSensorValue(
        const Sensors::Temperature::Value_t valueIn0p01Pct)
{
    return valueIn0p01Pct;
}

static constexpr Environmental::CustomProperties_t CUSTOM_PROPERTIES PROGMEM = {
        ConvertToSensorValue(Sensors::RelativeHumidity::RESOLUTION_IN_0P01_PCT),
        ConvertToSensorValue(Sensors::RelativeHumidity::MAXIMUM_VALUE_IN_0P01_PCT),
        ConvertToSensorValue(Sensors::RelativeHumidity::MINIMUM_VALUE_IN_0P01_PCT) };

}

namespace Pm2p5 {
static constexpr Environmental::Data_t ConvertToSensorValue(
        const Sensors::Temperature::Value_t valueInUgPerM3)
{
    return valueInUgPerM3;
}

static constexpr Environmental::CustomProperties_t CUSTOM_PROPERTIES PROGMEM = {
        ConvertToSensorValue(Sensors::Pm2p5::RESOLUTION_IN_UG_PER_M3),
        ConvertToSensorValue(Sensors::Pm2p5::MAXIMUM_VALUE_IN_UG_PER_M3),
        ConvertToSensorValue(Sensors::Pm2p5::MINIMUM_VALUE_IN_UG_PER_M3) };

}

}
}
}

/// Configuration for a collection of HID functions.
///
/// Arguments of ACTION macro:
/// \li type derived from Usb::Hid::Function,
/// \li collection's public member name,
/// \li one or more constructor's arguments; first must be report ID.
///
/// Report IDs must be greater than zero and increasing.
#define FOR_EACH_FUNCTION_IN_COLLECTION(ACTION, SEPARATOR) \
        ACTION(Usb::Hid::RtcUpdate::Receiver, \
              m_RtcUpdateReceiver, \
              USB_HID_REPORT_ID_RTC_UPDATE ) \
        SEPARATOR \
        ACTION(Usb::Hid::Sensor::Environmental, \
              m_AtmosphericPressure, \
              USB_HID_REPORT_ID_ATMOSPHERIC_PRESSURE_SENSOR, \
              Sensors::AtmosphericPressure::SAMPLING_PERIOD_IN_MS, \
              Sensors::AtmosphericPressure::SAMPLING_PERIOD_IN_MS, \
              Usb::Hid::Sensor::Temperature::ConvertToSensorValue(0U), \
              Usb::Hid::Sensor::AtmosphericPressure::CUSTOM_PROPERTIES, \
              Usb::Hid::Sensor::SensorState::INITIALIZING ) \
        SEPARATOR \
        ACTION(Usb::Hid::Sensor::Environmental, \
              m_Temperature, \
              USB_HID_REPORT_ID_TEMPERATURE_SENSOR, \
              Sensors::Temperature::SAMPLING_PERIOD_IN_MS, \
              Sensors::Temperature::SAMPLING_PERIOD_IN_MS, \
              Usb::Hid::Sensor::Temperature::ConvertToSensorValue(0), \
              Usb::Hid::Sensor::Temperature::CUSTOM_PROPERTIES, \
              Usb::Hid::Sensor::SensorState::INITIALIZING ) \
        SEPARATOR \
        ACTION(Usb::Hid::Sensor::Environmental, \
              m_RelativeHumidity, \
              USB_HID_REPORT_ID_RELATIVE_HUMIDITY_SENSOR, \
              Sensors::RelativeHumidity::SAMPLING_PERIOD_IN_MS, \
              Sensors::RelativeHumidity::SAMPLING_PERIOD_IN_MS, \
              Usb::Hid::Sensor::RelativeHumidity::ConvertToSensorValue(0U), \
              Usb::Hid::Sensor::RelativeHumidity::CUSTOM_PROPERTIES, \
              Usb::Hid::Sensor::SensorState::INITIALIZING ) \
        SEPARATOR \
        ACTION(Usb::Hid::Sensor::Environmental, \
            m_Pm2p5, \
            USB_HID_REPORT_ID_PM2P5_SENSOR, \
            Sensors::Pm2p5::SAMPLING_PERIOD_IN_MS, \
            Sensors::Pm2p5::SAMPLING_PERIOD_IN_MS, \
            Usb::Hid::Sensor::Pm2p5::ConvertToSensorValue(0U), \
            Usb::Hid::Sensor::Pm2p5::CUSTOM_PROPERTIES, \
            Usb::Hid::Sensor::SensorState::UNKNOWN ) \
        // No separator at the end!
