#pragma once

#include <stdint.h>

#include "Bcd/Date.hpp"
#include "Bcd/Time.hpp"
#include "common.h"

#include "I2c/DeviceRegs.hpp"
#include "I2c/Master.hpp"

/// PCF8563 I2C real-time clock/calendar.
class Pcf8563
{
public:
    Pcf8563() = delete;
    Pcf8563(
            const Pcf8563&) = delete;
    Pcf8563(
            Pcf8563&&) = delete;
    Pcf8563& operator =(
            const Pcf8563&) = delete;
    Pcf8563& operator =(
            Pcf8563&&) = delete;
    ~Pcf8563() = delete;

    static constexpr uint32_t MAX_SCL_FREQUENCY_IN_HZ = 400000UL;

    static void Startup(
            const uint16_t maxStartupTimeInMs)
    {
        ASSERT_ALWAYS(I2c::Master::IsDeviceAvailable(I2C_ADDRESS, maxStartupTimeInMs));
        ASSERT_ALWAYS(Initialize() == I2c::Master::NO_ERROR);
    }

    static I2c::Master::Error GetTimeDate(
            Bcd::Time& rTime,
            Bcd::Date& rDate,
            bool& rIsSetRequired)
    {
        I2c::DeviceRegs<REG_VL_SECONDS, REG_YEARS> timeDateRegs;

        const auto error = timeDateRegs.Read(I2C_ADDRESS);

        if (I2c::Master::NO_ERROR == error)
        {
            rTime.m_Second = timeDateRegs[REG_VL_SECONDS] & MASK_VL_SECONDS;
            rTime.m_Minute = timeDateRegs[REG_MINUTES] & MASK_MINUTES;
            rTime.m_Hour = timeDateRegs[REG_HOURS] & MASK_HOURS;
            rDate.m_Day = timeDateRegs[REG_DAYS] & MASK_DAYS;
            rDate.m_Weekday = timeDateRegs[REG_WEEKDAYS] & MASK_WEEKDAYS;
            rDate.m_Month = timeDateRegs[REG_CENTURY_MONTHS] & MASK_CENTURY_MONTHS;
            rDate.m_Year = timeDateRegs[REG_YEARS] & MASK_YEARS;

            rIsSetRequired = ((timeDateRegs[REG_VL_SECONDS] & BIT_VAL(BIT_VL)) != 0U)
                    || ((timeDateRegs[REG_CENTURY_MONTHS] & BIT_VAL(BIT_C)) != 0U);
        }

        return error;
    }

    static I2c::Master::Error SetTimeDate(
            const Bcd::Time& rTime,
            const Bcd::Date& rDate,
            const bool isSetRequired)
    {
        I2c::DeviceRegs<REG_VL_SECONDS, REG_YEARS> timeDateRegs;

        timeDateRegs[REG_VL_SECONDS] = rTime.m_Second & MASK_VL_SECONDS;
        timeDateRegs[REG_MINUTES] = rTime.m_Minute & MASK_MINUTES;
        timeDateRegs[REG_HOURS] = rTime.m_Hour & MASK_HOURS;
        timeDateRegs[REG_DAYS] = rDate.m_Day & MASK_DAYS;
        timeDateRegs[REG_WEEKDAYS] = rDate.m_Weekday & MASK_WEEKDAYS;
        timeDateRegs[REG_CENTURY_MONTHS] = rDate.m_Month & MASK_CENTURY_MONTHS;
        timeDateRegs[REG_YEARS] = rDate.m_Year & MASK_YEARS;

        if (isSetRequired)
        {
            timeDateRegs[REG_CENTURY_MONTHS] |= BIT_VAL(BIT_C);
        }

        return timeDateRegs.Write(I2C_ADDRESS);
    }

    static I2c::Master::Error GetValidatedTimeDate(
            Bcd::Time& rTime,
            Bcd::Date& rDate,
            bool& rIsSetRequired)
    {
        auto error = GetTimeDate(rTime, rDate, rIsSetRequired);

        if (error == I2c::Master::NO_ERROR)
        {
            bool areValuesInvalid = false;

            if (!rTime.IsValid())
            {
                rTime.SetToDayStart();
                areValuesInvalid = true;
            }
            if (!rDate.IsValid())
            {
                rDate.SetToCenturyStart();
                areValuesInvalid = true;
            }

            if (areValuesInvalid)
            {
                rIsSetRequired = true;
                error = SetTimeDate(rTime, rDate, rIsSetRequired);
            }
        }

        return error;
    }

    static I2c::Master::Error SetValidatedTimeDate(
            const Bcd::Time& rTime,
            const Bcd::Date& rDate)
    {
        return (rTime.IsValid() && rDate.IsValid()) ?
                                                      SetTimeDate(rTime, rDate, false) :
                                                      I2c::Master::ERR_DATA;
    }

private:
    static I2c::Master::Error Initialize()
    {
        I2c::DeviceRegs<REG_CONTROL_STATUS_1, REG_CONTROL_STATUS_2> controlStatusRegs;

        controlStatusRegs[REG_CONTROL_STATUS_1] = 0U << BIT_TEST1 | 0U << BIT_STOP
                | 0U << BIT_TESTC;
        controlStatusRegs[REG_CONTROL_STATUS_2] = 0U << BIT_TI_TP | 0U << BIT_AF | 0U << BIT_TF
                | 0U << BIT_AIE | 0U << BIT_TIE;

        auto error = controlStatusRegs.Write(I2C_ADDRESS);

        if (I2c::Master::NO_ERROR == error)
        {
            I2c::DeviceRegs<REG_MINUTE_ALARM, REG_TIMER> alarmTimerRegs;

            alarmTimerRegs[REG_MINUTE_ALARM] = 1U << BIT_AE | Bcd::Time::MINUTE_MIN;
            alarmTimerRegs[REG_HOUR_ALARM] = 1U << BIT_AE | Bcd::Time::HOUR_MIN;
            alarmTimerRegs[REG_DAY_ALARM] = 1U << BIT_AE | Bcd::Date::DAY_FIRST;
            alarmTimerRegs[REG_WEEKDAY_ALARM] = 1U << BIT_AE | Bcd::Date::WEEKDAY_FIRST;
            alarmTimerRegs[REG_CLKOUT_CONTROL] = 0U << BIT_FE | 0U << BIT_FD1 | 0U << BIT_FD0;
            alarmTimerRegs[REG_TIMER_CONTROL] = 0U << BIT_TE | 1U << BIT_TD1 | 1U << BIT_TD0;
            alarmTimerRegs[REG_TIMER] = 0x00U;

            error = alarmTimerRegs.Write(I2C_ADDRESS);
        }

        return error;
    }

    static constexpr uint8_t I2C_ADDRESS = 0xA2U;

    enum Register : uint8_t
    {
        REG_CONTROL_STATUS_1,
        REG_CONTROL_STATUS_2,
        REG_VL_SECONDS,
        REG_MINUTES,
        REG_HOURS,
        REG_DAYS,
        REG_WEEKDAYS,
        REG_CENTURY_MONTHS,
        REG_YEARS,
        REG_MINUTE_ALARM,
        REG_HOUR_ALARM,
        REG_DAY_ALARM,
        REG_WEEKDAY_ALARM,
        REG_CLKOUT_CONTROL,
        REG_TIMER_CONTROL,
        REG_TIMER,
        REG_COUNT
    };

    enum ValueMask : uint8_t
    {
        MASK_CONTROL_STATUS_1 = BITS_MASK(0U),
        MASK_CONTROL_STATUS_2 = BITS_MASK(0U),
        MASK_VL_SECONDS = BITS_MASK(3U + 4U),
        MASK_MINUTES = BITS_MASK(3U + 4U),
        MASK_HOURS = BITS_MASK(2U + 4U),
        MASK_DAYS = BITS_MASK(2U + 4U),
        MASK_WEEKDAYS = BITS_MASK(3U),
        MASK_CENTURY_MONTHS = BITS_MASK(1U + 4U),
        MASK_YEARS = BITS_MASK(4U + 4U),
        MASK_MINUTE_ALARM = MASK_MINUTES,
        MASK_HOUR_ALARM = MASK_HOURS,
        MASK_DAY_ALARM = MASK_DAYS,
        MASK_WEEKDAY_ALARM = MASK_WEEKDAYS,
        MASK_CLKOUT_CONTROL = BITS_MASK(0U),
        MASK_TIMER_CONTROL = BITS_MASK(0U),
        MASK_TIMER = BITS_MASK(8U)
    };

    enum ControlStatus1Bit
    {
        BIT_TESTC = 3U,
        BIT_STOP = 5U,
        BIT_TEST1 = 7U
    };

    enum ControlStatus2Bit
    {
        BIT_TIE = 0U,
        BIT_AIE = 1U,
        BIT_TF = 2U,
        BIT_AF = 3U,
        BIT_TI_TP = 4U
    };

    enum SecondsBit
    {
        BIT_VL = 7U
    };

    enum CenturyMonthsBit
    {
        BIT_C = 7U
    };

    enum AlarmBit
    {
        BIT_AE = 7U
    };

    enum ClkoutControlBit
    {
        BIT_FD0 = 0U,
        BIT_FD1 = 1U,
        BIT_FE = 7U
    };

    enum TimerControlBit
    {
        BIT_TD0 = 0U,
        BIT_TD1 = 1U,
        BIT_TE = 7U
    };
};
