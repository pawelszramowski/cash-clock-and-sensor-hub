#pragma once

#include <stdint.h>

#include "Tc/SystemTimer.hpp"

class OneshotTimer
{
public:
    void Start(
            const uint16_t timeoutInMs)
    {
        m_StartTimestampInMs = SystemTimer::GetTimestampInMs();
        m_TimeoutInMs = timeoutInMs;
        m_IsExpired = false;
    }

    bool IsStarted() const
    {
        return m_TimeoutInMs > 0U;
    }

    bool IsExpired() const
    {
        if (IsStarted() && !m_IsExpired
                && ((SystemTimer::GetTimestampInMs() - m_StartTimestampInMs) >= m_TimeoutInMs))
        {
            m_IsExpired = true;
        }

        return m_IsExpired;
    }

    void Stop()
    {
        m_TimeoutInMs = 0U;
        m_IsExpired = false;
    }

private:
    uint16_t m_StartTimestampInMs = 0U;
    uint16_t m_TimeoutInMs = 0U;
    mutable bool m_IsExpired = false;
};

