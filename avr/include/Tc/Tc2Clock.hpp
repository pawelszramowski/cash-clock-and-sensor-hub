#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "Tc/GenericClock.hpp"
#include "Tc/Prescaler.hpp"

namespace TcUtils {

/// Timer/Counter2 clock prescaler settings available in ATmegaX8.
struct Tc2Prescalers
{
    static constexpr Prescaler P1 { 1U, 0U << CS22 | 0U << CS21 | 1U << CS20 };
    static constexpr Prescaler P2 { 8U, 0U << CS22 | 1U << CS21 | 0U << CS20 };
    static constexpr Prescaler P3 { 32U, 0U << CS22 | 1U << CS21 | 1U << CS20 };
    static constexpr Prescaler P4 { 64U, 1U << CS22 | 0U << CS21 | 0U << CS20 };
    static constexpr Prescaler P5 { 128U, 1U << CS22 | 0U << CS21 | 1U << CS20 };
    static constexpr Prescaler P6 { 256U, 1U << CS22 | 0U << CS21 | 1U << CS20 };
    static constexpr Prescaler P7 { 1024U, 1U << CS22 | 0U << CS21 | 1U << CS20 };
};

/// Wrapper for Timer/Counter2 clock prescaler calculations.
using Tc2Clock = GenericClock<uint8_t, F_CPU, Tc2Prescalers::P1, Tc2Prescalers::P2, Tc2Prescalers::P3,
Tc2Prescalers::P4, Tc2Prescalers::P5, Tc2Prescalers::P6, Tc2Prescalers::P7>;
}
