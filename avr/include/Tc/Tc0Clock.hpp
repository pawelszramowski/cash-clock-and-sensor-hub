#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "Tc/GenericClock.hpp"
#include "Tc/Prescaler.hpp"

namespace TcUtils {

/// Timer/Counter0 clock prescaler settings available in ATmegaX8.
struct Tc0Prescalers
{
    static constexpr Prescaler P1 { 1U, 0U << CS02 | 0U << CS01 | 1U << CS00 };
    static constexpr Prescaler P2 { 8U, 0U << CS02 | 1U << CS01 | 0U << CS00 };
    static constexpr Prescaler P3 { 64U, 0U << CS02 | 1U << CS01 | 1U << CS00 };
    static constexpr Prescaler P4 { 256U, 1U << CS02 | 0U << CS01 | 0U << CS00 };
    static constexpr Prescaler P5 { 1024U, 1U << CS02 | 0U << CS01 | 1U << CS00 };
};

/// Wrapper for Timer/Counter0 clock prescaler calculations.
using Tc0Clock = GenericClock<uint8_t, F_CPU, Tc0Prescalers::P1, Tc0Prescalers::P2,
Tc0Prescalers::P3, Tc0Prescalers::P4, Tc0Prescalers::P5>;

}
