#pragma once

#include <stdint.h>

#include "Tc/SystemTimer.hpp"

class StopwatchTimer
{
public:
    void Start()
    {
        m_StartTimestampInMs = SystemTimer::GetTimestampInMs();
        m_IsStarted = true;
    }

    bool IsStarted() const
    {
        return m_IsStarted;
    }

    bool HasPassed(
            const uint16_t timeInMs) const
    {
        return IsStarted() && ((SystemTimer::GetTimestampInMs() - m_StartTimestampInMs) >= timeInMs);
    }

    void Stop()
    {
        m_IsStarted = false;
    }

private:
    uint16_t m_StartTimestampInMs = 0U;
    bool m_IsStarted = false;
};

