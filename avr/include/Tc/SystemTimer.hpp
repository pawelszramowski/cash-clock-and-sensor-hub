#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "common.h"

#include "Tc/GenericClock.hpp"
#include "Tc/Tc0Clock.hpp"

/// Timer/Counter used to keep low-precision system time with sub-second resolution.
class SystemTimer
{
public:
    SystemTimer() = delete;
    SystemTimer(
            const SystemTimer&) = delete;
    SystemTimer(
            SystemTimer&&) = delete;
    SystemTimer& operator =(
            const SystemTimer&) = delete;
    SystemTimer& operator =(
            SystemTimer&&) = delete;
    ~SystemTimer() = delete;

    static constexpr uint16_t MAX_USABLE_TIME_DELTA_IN_MS = UINT16_MAX / 2U;
    static constexpr uint16_t RESOLUTION_IN_MS = 5U;

    static void Startup()
    {
        m_TimestampInMs = 0U;

        // Reset T/C0.
        TCCR0B = 0U;
        TCCR0A = 0U;
        TCNT0 = 0U;
        OCR0A = TC0_TOP;
        OCR0B = TcUtils::Tc0Clock::MAX_CNT;

        // Disable interrupts and reset flags.
        TIMSK0 &= ~(BIT_VAL(OCIE0B) | BIT_VAL(OCIE0A) | BIT_VAL(TOIE0));
        TIFR0 = BIT_VAL(OCF0B) | BIT_VAL(OCF0A) | BIT_VAL(TOV0);

        // Enable T/C0 in CTC mode.
        TCCR0A = 1U << WGM01 | 0U << WGM00;
        TCCR0B = 0U << WGM02 | TC0_CS;
    }

    static void Poll()
    {
        if ((TIFR0 & BIT_VAL(OCF0A)) != 0U)
        {
            TIFR0 = BIT_VAL(OCF0A);
            m_TimestampInMs += TC0_OC_PERIOD_IN_MS;
        }
    }

    static uint16_t GetTimestampInMs()
    {
        Poll();
        return m_TimestampInMs;
    }

private:
    static constexpr uint32_t TC0_OC_PERIOD_IN_MS = RESOLUTION_IN_MS;

    static constexpr uint32_t TC0_OC_FREQUENCY_IN_HZ = 1000UL / TC0_OC_PERIOD_IN_MS;
    static constexpr auto TC0_PRESCALER = TcUtils::Tc0Clock::GetPrescalerValue(
            TC0_OC_FREQUENCY_IN_HZ);
    static_assert(TC0_PRESCALER != TcUtils::Prescaler::INVALID_VALUE, "Requested T/C0 parameters are not achievable!");

    static constexpr auto TC0_CS = TcUtils::Tc0Clock::GetClockSelect(TC0_PRESCALER);
    static constexpr auto TC0_TOP = TcUtils::Tc0Clock::GetTopValue(TC0_PRESCALER,
                                                                   TC0_OC_FREQUENCY_IN_HZ);

    static uint16_t m_TimestampInMs;
};
