#pragma once

#include <stdint.h>

#include "Tc/SystemTimer.hpp"

class PeriodicTimer
{
public:
    void Start(
            const uint16_t periodInMs,
            const bool makeDueAlready = false)
    {
        m_LastDueTimestampInMs = SystemTimer::GetTimestampInMs();
        if (makeDueAlready)
        {
            m_LastDueTimestampInMs -= periodInMs;
        }
        m_PeriodInMs = periodInMs;
    }

    bool IsStarted() const
    {
        return m_PeriodInMs > 0U;
    }

    bool IsDue() const
    {
        bool isDue = false;

        if (IsStarted())
        {
            const uint16_t currentTimestampInMs = SystemTimer::GetTimestampInMs();

            if ((currentTimestampInMs - m_LastDueTimestampInMs) >= m_PeriodInMs)
            {
                m_LastDueTimestampInMs = currentTimestampInMs;
                isDue = true;
            }
        }

        return isDue;
    }

    void Stop()
    {
        m_PeriodInMs = 0U;
    }

private:
    mutable uint16_t m_LastDueTimestampInMs = 0U;
    uint16_t m_PeriodInMs = 0U;
};

