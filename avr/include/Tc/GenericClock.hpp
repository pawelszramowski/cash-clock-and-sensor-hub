#pragma once

#include <stdint.h>

#include "Tc/Prescaler.hpp"

namespace TcUtils {

/// Wrapper for generic Timer/Counter clock prescaler calculations.
template<typename TRegister, uint32_t CLOCK_FREQUENCY_IN_HZ,
        const Prescaler& ...SUPPORTED_PRESCALERS>
class GenericClock
{
public:
    GenericClock() = delete;
    GenericClock(
            const GenericClock&) = delete;
    GenericClock(
            GenericClock&&) = delete;
    GenericClock& operator =(
            const GenericClock&) = delete;
    GenericClock& operator =(
            GenericClock&&) = delete;
    ~GenericClock() = delete;

    using Reg_t = TRegister;
    static constexpr Reg_t MAX_CNT = static_cast<Reg_t>(-1);

    static constexpr uint16_t GetPrescalerValue(
            const uint32_t compareMatchFrequencyInHz,
            const Reg_t minResolutionInSteps = 1)
    {
        return ChoosePrescalerValue<SUPPORTED_PRESCALERS...>(compareMatchFrequencyInHz,
                                                             minResolutionInSteps);
    }

    static constexpr uint8_t GetClockSelect(
            const uint16_t prescalerValue)
    {
        return ChooseClockSelect<SUPPORTED_PRESCALERS...>(prescalerValue);
    }

    static constexpr uint32_t GetTopValue(
            const uint16_t prescalerValue,
            const uint32_t compareMatchFrequencyInHz)
    {
        return ((prescalerValue != Prescaler::INVALID_VALUE) && (compareMatchFrequencyInHz != 0U)) ?
                ((CLOCK_FREQUENCY_IN_HZ / (prescalerValue * compareMatchFrequencyInHz)) - 1UL) : 0U;
    }

private:
    static constexpr bool IsTopValueValid(
            const uint32_t topValue,
            const Reg_t minResolutionInSteps = 1)
    {
        return (topValue >= minResolutionInSteps) && (topValue <= MAX_CNT);
    }

    template<const Prescaler& PRESCALER>
    static constexpr uint16_t ChoosePrescalerValue(
            const uint32_t compareMatchFrequencyInHz,
            const Reg_t minResolutionInSteps = 1)
    {
        return ((compareMatchFrequencyInHz <= (CLOCK_FREQUENCY_IN_HZ / 2UL))
                && IsTopValueValid(GetTopValue(PRESCALER.m_Value, compareMatchFrequencyInHz),
                                   minResolutionInSteps)) ?
                                                            PRESCALER.m_Value :
                                                            Prescaler::INVALID_VALUE;
    }

    template<const Prescaler& PRESCALER0, const Prescaler& PRESCALER1,
            const Prescaler& ...PRESCALERS>
    static constexpr uint16_t ChoosePrescalerValue(
            const uint32_t compareMatchFrequencyInHz,
            const Reg_t minResolutionInSteps = 1)
    {
        return (ChoosePrescalerValue<PRESCALER0>(compareMatchFrequencyInHz, minResolutionInSteps)
                != Prescaler::INVALID_VALUE) ?
                ChoosePrescalerValue<PRESCALER0>(compareMatchFrequencyInHz, minResolutionInSteps) :
                ChoosePrescalerValue<PRESCALER1, PRESCALERS...>(compareMatchFrequencyInHz,
                                                                minResolutionInSteps);
    }

    template<const Prescaler& PRESCALER>
    static constexpr uint8_t ChooseClockSelect(
            const uint16_t prescalerValue)
    {
        return (PRESCALER.m_Value == prescalerValue) ? PRESCALER.m_Cs : Prescaler::INVALID_CS;
    }

    template<const Prescaler& PRESCALER0, const Prescaler& PRESCALER1,
            const Prescaler& ...PRESCALERS>
    static constexpr uint8_t ChooseClockSelect(
            const uint16_t prescalerValue)
    {
        return (ChooseClockSelect<PRESCALER0>(prescalerValue) != Prescaler::INVALID_CS) ?
                ChooseClockSelect<PRESCALER0>(prescalerValue) :
                ChooseClockSelect<PRESCALER1, PRESCALERS...>(prescalerValue);
    }
};

}
