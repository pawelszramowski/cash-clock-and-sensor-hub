#pragma once

#include <stdint.h>

namespace TcUtils {

/// Timer/Counter clock prescaler setting.
struct Prescaler
{
    static constexpr uint16_t INVALID_VALUE = 0U;
    static constexpr uint8_t INVALID_CS = 0U;

    uint16_t m_Value;
    uint8_t m_Cs;
};

}
