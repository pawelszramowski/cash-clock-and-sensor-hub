#pragma once

#include <avr/io.h>
#include <stdint.h>

#include "Tc/GenericClock.hpp"
#include "Tc/Prescaler.hpp"

namespace TcUtils {

/// Timer/Counter1 clock prescaler settings available in ATmegaX8.
struct Tc1Prescalers
{
    static constexpr Prescaler P1 { 1U, 0U << CS12 | 0U << CS11 | 1U << CS10 };
    static constexpr Prescaler P2 { 8U, 0U << CS12 | 1U << CS11 | 0U << CS10 };
    static constexpr Prescaler P3 { 64U, 0U << CS12 | 1U << CS11 | 1U << CS10 };
    static constexpr Prescaler P4 { 256U, 1U << CS12 | 0U << CS11 | 0U << CS10 };
    static constexpr Prescaler P5 { 1024U, 1U << CS12 | 0U << CS11 | 1U << CS10 };
};

/// Wrapper for Timer/Counter1 clock prescaler calculations.
using Tc1Clock = GenericClock<uint16_t, F_CPU, Tc1Prescalers::P1, Tc1Prescalers::P2,
Tc1Prescalers::P3,Tc1Prescalers::P4, Tc1Prescalers::P5>;

}
