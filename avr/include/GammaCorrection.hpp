#pragma once

#include <stdint.h>

#include "PiecewiseLinearApproximation.hpp"

/// Conversion between relative luminance and relative output control value
/// to compensate for non-linear sensitivity of human vision.
///
/// For motivation refer to:
/// - http://en.wikipedia.org/wiki/Gamma_correction
/// - http://en.wikipedia.org/wiki/Stevens'_power_law
/// - http://en.wikipedia.org/wiki/Weber-Fechner_law
class GammaCorrection
{
public:
    GammaCorrection() = delete;
    GammaCorrection(
            const GammaCorrection&) = delete;
    GammaCorrection(
            GammaCorrection&&) = delete;
    GammaCorrection& operator =(
            const GammaCorrection&) = delete;
    GammaCorrection& operator =(
            GammaCorrection&&) = delete;
    ~GammaCorrection() = delete;

    /// Coarse piecewise linear approximation of power-law function is used:
    /// relative output control value = 1.0 * relative luminance ^ 2.2
    static uint8_t DecodeValue(
            const uint8_t brightness)
    {
        static constexpr PiecewiseLinearApproximation::Point P0 { 0U, 1U };
        static constexpr PiecewiseLinearApproximation::Point P1 { 38U, 4U };
        static constexpr PiecewiseLinearApproximation::Point P2 { 77U, 18U };
        static constexpr PiecewiseLinearApproximation::Point P3 { 115U, 44U };
        static constexpr PiecewiseLinearApproximation::Point P4 { 179U, 116U };
        static constexpr PiecewiseLinearApproximation::Point P5 { UINT8_MAX, UINT8_MAX };

        return PiecewiseLinearApproximation::CalculateValue<P0, P1, P2, P3, P4, P5>(brightness);
    }
};
