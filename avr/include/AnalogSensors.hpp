#pragma once

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#include "common.h"

#include "Adc/Adc.hpp"
#include "Backlight.hpp"
#include "SimpleMovingAverage.hpp"

/// Sensors with analog outputs measured by ADC.
class AnalogSensors
{
#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
private:
    static constexpr uint32_t BAROMETER_AMP_GAIN_IN_VOLT_PER_VOLT = 10U;
#endif

public:
    AnalogSensors() = delete;
    AnalogSensors(
            const AnalogSensors&) = delete;
    AnalogSensors(
            AnalogSensors&&) = delete;
    AnalogSensors& operator =(
            const AnalogSensors&) = delete;
    AnalogSensors& operator =(
            AnalogSensors&&) = delete;
    ~AnalogSensors() = delete;

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
    struct AtmosphericPressure
    {
        using Value_t = uint32_t;

        Value_t m_ValueInPa;

        static constexpr Value_t MINIMUM_VALUE_IN_PA = 15000U;
        static constexpr Value_t MAXIMUM_VALUE_IN_PA = 115000U;
        static constexpr Value_t RESOLUTION_IN_PA = 15625UL
                / (144UL * BAROMETER_AMP_GAIN_IN_VOLT_PER_VOLT);
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 100U;

        bool IsValid() const
        {
            return (m_ValueInPa >= MINIMUM_VALUE_IN_PA) && (m_ValueInPa <= MAXIMUM_VALUE_IN_PA);
        }

        void Reset()
        {
            m_ValueInPa = UINT32_MAX;
        }
    };
#endif

    struct BacklightDutyCycle
    {
        using Value_t = uint8_t;

        Value_t m_Value;

        bool IsValid() const
        {
            return true;
        }

        void Reset()
        {
            m_Value = UINT8_MAX;
        }
    };

    static void Startup()
    {
        static constexpr Adc::ReferenceVoltage VREF = Adc::VREF_AREF;
        static_assert(VREF == Adc::VREF_AREF,
                "Sensor supply voltage is connected to AREF pin. "
                "Other reference voltage options must not be used "
                "as they will be shorted to the external voltage!");
        Adc::Startup(VREF);

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
        m_AdcSamples.m_BarometerOut.AddSample(Adc::ConvertChannel(ADC_BAROMETER_OUT));
        m_AdcSamples.m_BarometerAmp.AddSample(Adc::ConvertChannel(ADC_BAROMETER_AMP));
        m_AdcSamples.m_BarometerRef.AddSample(Adc::ConvertChannel(ADC_BAROMETER_REF));
        m_IsAtmosphericPressureOutdated = true;
        m_AtmosphericPressure.Reset();
#endif

        m_AdcSamples.m_PhotocellDiv.AddSample(Adc::ConvertChannel(ADC_PHOTOCELL_DIV));
        m_IsBacklightDutyCycleOutdated = true;
        m_BacklightDutyCycle.Reset();

        Adc::TriggerConversion(ADC_PHOTOCELL_DIV);
    }

    static void Poll()
    {
        if (!Adc::IsConversionInProgress())
        {
            ASSERT_ALWAYS(Adc::IsConversionCompleted());
            const auto conversionResult = Adc::GetConversionResult();

            Adc::Channel nextChannel;

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
            switch (Adc::GetSelectedChannel())
            {
            case ADC_BAROMETER_OUT:
                m_AdcSamples.m_BarometerOut.AddSample(conversionResult);
                nextChannel = ADC_BAROMETER_AMP;
                break;
            case ADC_BAROMETER_AMP:
                m_AdcSamples.m_BarometerAmp.AddSample(conversionResult);
                nextChannel = ADC_BAROMETER_REF;
                break;
            case ADC_BAROMETER_REF:
                m_AdcSamples.m_BarometerRef.AddSample(conversionResult);
                m_IsAtmosphericPressureOutdated = true;
                nextChannel = ADC_PHOTOCELL_DIV;
                break;
            case ADC_PHOTOCELL_DIV:
                m_AdcSamples.m_PhotocellDiv.AddSample(conversionResult);
                m_IsBacklightDutyCycleOutdated = true;
                nextChannel = ADC_BAROMETER_OUT;
                break;
            default:
                ASSERT_ALWAYS(false);
                break;
            }
#else
            switch (Adc::GetSelectedChannel())
            {
            case ADC_PHOTOCELL_DIV:
                m_AdcSamples.m_PhotocellDiv.AddSample(conversionResult);
                m_IsBacklightDutyCycleOutdated = true;
                nextChannel = ADC_PHOTOCELL_DIV;
                break;
            default:
                ASSERT_ALWAYS(false);
                break;
            }
#endif

            Adc::TriggerConversion(nextChannel);
        }
    }

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
    static const AtmosphericPressure& GetAtmosphericPressure()
    {
        if (m_IsAtmosphericPressureOutdated)
        {
            const auto barometerOutAdc = m_AdcSamples.m_BarometerOut.GetAverageValue();
            const auto barometerRefAdc = m_AdcSamples.m_BarometerRef.GetAverageValue();

            if (IsBarometerAmpOperational(barometerRefAdc, barometerOutAdc))
            {
                const auto barometerAmpAdc = m_AdcSamples.m_BarometerAmp.GetAverageValue();
                m_AtmosphericPressure.m_ValueInPa = CalculateAtmosphericPressureInPa(
                        barometerRefAdc, barometerAmpAdc);
                ASSERT_ALWAYS(m_AtmosphericPressure.IsValid());
            }
            else
            {
                m_AtmosphericPressure.m_ValueInPa = CalculateAtmosphericPressureInPa(
                        barometerOutAdc);
                ASSERT_ALWAYS(m_AtmosphericPressure.IsValid());
            }

            m_IsAtmosphericPressureOutdated = false;
        }

        return m_AtmosphericPressure;
    }
#endif

    static const BacklightDutyCycle& GetBacklightDutyCycle()
    {
        if (m_IsBacklightDutyCycleOutdated)
        {
            m_BacklightDutyCycle.m_Value = CalculateBacklightDutyCycle(
                    m_AdcSamples.m_PhotocellDiv.GetAverageValue());
            ASSERT_ALWAYS(m_BacklightDutyCycle.IsValid());

            m_IsBacklightDutyCycleOutdated = false;
        }

        return m_BacklightDutyCycle;
    }

private:
#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
    static bool IsBarometerAmpOperational(
            const Adc::Sample_t barometerRefAdc,
            const Adc::Sample_t barometerOutAdc)
    {
        return (barometerOutAdc > barometerRefAdc)
                && (((barometerOutAdc - barometerRefAdc) * BAROMETER_AMP_GAIN_IN_VOLT_PER_VOLT)
                        < Adc::FULL_SCALE);
    }

    static AtmosphericPressure::Value_t CalculateAtmosphericPressureInPa(
            const Adc::Sample_t barometerRefAdc,
            const Adc::Sample_t barometerAmpAdc)
    {
        static_assert(Adc::RESOLUTION_IN_BITS == 10U, "Adjust transfer function formula!");

        return (15625UL * barometerAmpAdc
                + 15625UL * BAROMETER_AMP_GAIN_IN_VOLT_PER_VOLT * barometerRefAdc
                + 1520000UL * BAROMETER_AMP_GAIN_IN_VOLT_PER_VOLT)
                / (144UL * BAROMETER_AMP_GAIN_IN_VOLT_PER_VOLT);
    }

    static AtmosphericPressure::Value_t CalculateAtmosphericPressureInPa(
            const Adc::Sample_t barometerOutAdc)
    {
        static_assert(Adc::RESOLUTION_IN_BITS == 10U, "Adjust transfer function formula!");

        return ((15625UL * barometerOutAdc + 1520000UL) / 144UL);
    }
#endif

    /// Use MSB of photocell voltage divider ADC measurement almost directly
    /// as backlight duty cycle as the two have quite similar characteristics.
    static BacklightDutyCycle::Value_t CalculateBacklightDutyCycle(
            Adc::Sample_t photocellDivAdc)
    {
        static constexpr uint8_t DUTY_CYCLE_RESOLUTION_IN_BITS = CHAR_BIT
                * sizeof(BacklightDutyCycle::Value_t);

        if (Adc::RESOLUTION_IN_BITS > DUTY_CYCLE_RESOLUTION_IN_BITS)
        {
            // Round and discard extra LSB.
            photocellDivAdc += BIT_VAL(
                    Adc::RESOLUTION_IN_BITS - DUTY_CYCLE_RESOLUTION_IN_BITS - 1U);
            photocellDivAdc >>= (Adc::RESOLUTION_IN_BITS - DUTY_CYCLE_RESOLUTION_IN_BITS);
        }

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
        // FIXME Backlight is limited to a very low brightness level
        // to prevent it from raising sensor temperature by over 0.5*C!
        if (photocellDivAdc > 3U)
        {
            photocellDivAdc = 3U;
        }
#endif

        return (photocellDivAdc == Backlight::DUTY_CYCLE_ALWAYS_OFF) ?
                                                                       Backlight::DUTY_CYCLE_MIN :
                                                                       photocellDivAdc;
    }

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
    static constexpr Adc::Channel ADC_BAROMETER_OUT = Adc::CHANNEL_ADC0;
    static constexpr Adc::Channel ADC_BAROMETER_AMP = Adc::CHANNEL_ADC1;
    static constexpr Adc::Channel ADC_BAROMETER_REF = Adc::CHANNEL_ADC7;
#endif
    static constexpr Adc::Channel ADC_PHOTOCELL_DIV = Adc::CHANNEL_ADC6;

    static struct AdcSamples
    {
        static constexpr small_size_t AVERAGED_SAMPLES = 16U;

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
        SimpleMovingAverage<Adc::Sample_t, AVERAGED_SAMPLES> m_BarometerOut;
        SimpleMovingAverage<Adc::Sample_t, AVERAGED_SAMPLES> m_BarometerAmp;
        SimpleMovingAverage<Adc::Sample_t, AVERAGED_SAMPLES> m_BarometerRef;
#endif
        SimpleMovingAverage<Adc::Sample_t, AVERAGED_SAMPLES> m_PhotocellDiv;
    } m_AdcSamples;

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
    static bool m_IsAtmosphericPressureOutdated;
    static AtmosphericPressure m_AtmosphericPressure;
#endif

    static bool m_IsBacklightDutyCycleOutdated;
    static BacklightDutyCycle m_BacklightDutyCycle;
};
