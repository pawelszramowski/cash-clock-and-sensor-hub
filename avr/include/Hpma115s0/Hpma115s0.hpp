#pragma once

#include "Hpma115s0/Fsm.hpp"

class Hpma115s0
{
public:
    Hpma115s0() = delete;
    Hpma115s0(
            const Hpma115s0&) = delete;
    Hpma115s0(
            Hpma115s0&&) = delete;
    Hpma115s0& operator =(
            const Hpma115s0&) = delete;
    Hpma115s0& operator =(
            Hpma115s0&&) = delete;
    ~Hpma115s0() = delete;

    using Pm2p5 = Hpma115s0Fsm::Machine::Pm2p5;

    static void Startup()
    {
        Hpma115s0Fsm::Machine::Startup();
    }

    static void Poll()
    {
        Hpma115s0Fsm::Machine::Poll();
    }

    static void Enable(
            const bool enabled)
    {
        Hpma115s0Fsm::Machine::Enable(enabled);
    }

    static bool IsEnabled()
    {
        return Hpma115s0Fsm::Machine::IsEnabled();
    }

    static const Pm2p5& GetPm2p5()
    {
        return Hpma115s0Fsm::Machine::GetPm2p5();
    }
};
