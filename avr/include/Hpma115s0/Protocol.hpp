#pragma once

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#include "common.h"
#include "common.hpp"

namespace Hpma115s0Protocol {

static constexpr uint8_t HEAD_REQUEST = 0x68U;
static constexpr uint8_t HEAD_RESPONSE = 0x40U;

static constexpr uint8_t CMD_START_PARTICLE_MEASUREMENT = 0x01U;
static constexpr uint8_t CMD_STOP_PARTICLE_MEASUREMENT = 0x02U;
static constexpr uint8_t CMD_READ_PARTICLE_MEASUREMENT_RESULTS = 0x04U;
static constexpr uint8_t CMD_WRITE_CUSTOMER_ADJUSTMENT_COEFFICIENT = 0x08U;
static constexpr uint8_t CMD_READ_CUSTOMER_ADJUSTMENT_COEFFICIENT = 0x10U;
static constexpr uint8_t CMD_DISABLE_AUTO_SEND = 0x20U;
static constexpr uint8_t CMD_ENABLE_AUTO_SEND = 0x40U;

template<typename T>
struct Frame
{
    uint8_t m_Head;
    uint8_t m_Len;
    uint8_t m_Cmd;
    T m_Data;
    uint8_t m_Cs;

    Frame() = default;
    Frame(
            const uint8_t head,
            const uint8_t cmd,
            const T& rData)
            :
            m_Head(head),
            m_Len(CalculateLen()),
            m_Cmd(cmd),
            m_Data(rData),
            m_Cs(CalculateCs())
    {
        // Do nothing.
    }

    constexpr uint8_t CalculateLen() const
    {
        return sizeof(m_Cmd) + sizeof(T);
    }

    uint8_t CalculateCs() const
    {
        auto pByte = reinterpret_cast<const uint8_t*>(&m_Data);

        uint16_t sum = 0U;
        for (small_size_t i = sizeof(m_Data); i > 0U; --i)
        {
            sum += *pByte++;
        }

        return (65535U
                - (static_cast<uint16_t>(m_Head) + static_cast<uint16_t>(m_Len)
                        + static_cast<uint16_t>(m_Cmd) + sum - 1U));
    }

    bool IsValidRequest() const
    {
        return (m_Head == HEAD_REQUEST) && (m_Len == CalculateLen()) && m_Data.IsValid()
                && (m_Cs == CalculateCs());
    }

    bool IsValidResponseTo(
            const uint8_t cmd) const
    {
        return (m_Head == HEAD_RESPONSE) && (m_Len == CalculateLen()) && (m_Cmd == cmd)
                && m_Data.IsValid() && (m_Cs == CalculateCs());
    }
} ATTR_PACKED;

template<>
struct Frame<void>
{
    uint8_t m_Head;
    uint8_t m_Len;
    uint8_t m_Cmd;
    uint8_t m_Cs;

    Frame() = default;
    constexpr Frame(
            const uint8_t head,
            const uint8_t cmd)
            :
            m_Head(head),
            m_Len(CalculateLen()),
            m_Cmd(cmd),
            m_Cs(CalculateCs())
    {
        // Do nothing.
    }

    constexpr uint8_t CalculateLen() const
    {
        return sizeof(m_Cmd);
    }

    constexpr uint8_t CalculateCs() const
    {
        return (65535U
                - (static_cast<uint16_t>(m_Head) + static_cast<uint16_t>(m_Len)
                        + static_cast<uint16_t>(m_Cmd) - 1U));
    }

    constexpr bool IsValidRequest() const
    {
        return (m_Head == HEAD_REQUEST) && (m_Len == CalculateLen()) && (m_Cs == CalculateCs());
    }
} ATTR_PACKED;

static constexpr Frame<void> REQ_START_PARTICLE_MEASUREMENT(HEAD_REQUEST,
                                                            CMD_START_PARTICLE_MEASUREMENT);
static constexpr Frame<void> REQ_STOP_PARTICLE_MEASUREMENT(HEAD_REQUEST,
                                                           CMD_STOP_PARTICLE_MEASUREMENT);
static constexpr Frame<void> REQ_READ_PARTICLE_MEASUREMENT_RESULTS(
        HEAD_REQUEST, CMD_READ_PARTICLE_MEASUREMENT_RESULTS);
static constexpr Frame<void> REQ_READ_CUSTOMER_ADJUSTMENT_COEFFICIENT(
        HEAD_REQUEST, CMD_READ_CUSTOMER_ADJUSTMENT_COEFFICIENT);
static constexpr Frame<void> REQ_DISABLE_AUTO_SEND(HEAD_REQUEST, CMD_DISABLE_AUTO_SEND);
static constexpr Frame<void> REQ_ENABLE_AUTO_SEND(HEAD_REQUEST, CMD_ENABLE_AUTO_SEND);

static_assert(REQ_START_PARTICLE_MEASUREMENT.IsValidRequest(), "Invalid request!");
static_assert(REQ_STOP_PARTICLE_MEASUREMENT.IsValidRequest(), "Invalid request!");
static_assert(REQ_READ_PARTICLE_MEASUREMENT_RESULTS.IsValidRequest(), "Invalid request!");
static_assert(REQ_READ_CUSTOMER_ADJUSTMENT_COEFFICIENT.IsValidRequest(), "Invalid request!");
static_assert(REQ_DISABLE_AUTO_SEND.IsValidRequest(), "Invalid request!");
static_assert(REQ_ENABLE_AUTO_SEND.IsValidRequest(), "Invalid request!");

struct Ack
{
    uint8_t m_Byte0;
    uint8_t m_Byte1;

    bool operator==(
            const Ack& rRhs) const
    {
        return (m_Byte0 == rRhs.m_Byte0) && (m_Byte1 == rRhs.m_Byte1);
    }
} ATTR_PACKED;

static constexpr Ack ACK_POS { 0xA5U, 0xA5U };
static constexpr Ack ACK_NEG { 0x96U, 0x96U };

struct ParticleMeasurementResults
{
    uint8_t m_DF1;
    uint8_t m_DF2;
    uint8_t m_DF3;
    uint8_t m_DF4;

    static constexpr uint16_t MINIMUM_VALUE_IN_UG_PER_M3 = 0U;
    static constexpr uint16_t MAXIMUM_VALUE_IN_UG_PER_M3 = 1000U;

    uint16_t GetPm2p5InUgPerM3() const
    {
        return (m_DF1 << CHAR_BIT) | m_DF2;
    }

    uint16_t GetPm10InUgPerM3() const
    {
        return (m_DF3 << CHAR_BIT) | m_DF4;
    }

    bool IsValid() const
    {
        static_assert(MINIMUM_VALUE_IN_UG_PER_M3 == 0U, "Use non-zero minimum value in validity check!");
        return (GetPm2p5InUgPerM3() <= MAXIMUM_VALUE_IN_UG_PER_M3)
                && (GetPm10InUgPerM3() <= MAXIMUM_VALUE_IN_UG_PER_M3);
    }
} ATTR_PACKED;

using ParticleMeasurementResultsFrame = Frame<ParticleMeasurementResults>;

struct CustomerAdjustmentCoefficient
{
    uint8_t m_DF1;

    static constexpr uint8_t MINIMUM_VALUE = 30U;
    static constexpr uint8_t MAXIMUM_VALUE = 200U;

    bool IsValid() const
    {
        return (m_DF1 >= MINIMUM_VALUE) && (m_DF1 <= MAXIMUM_VALUE);
    }

    void SetToDefault()
    {
        m_DF1 = 100U;
    }
} ATTR_PACKED;

using CustomerAdjustmentCoefficientFrame = Frame<CustomerAdjustmentCoefficient>;

/// Returns the largest response time in milliseconds for given types.
template<typename T, typename ... Ts>
struct ResponseTime
{
    static constexpr uint16_t VALUE_IN_MS = nonstd::maxv(ResponseTime<T>::VALUE_IN_MS,
                                                         ResponseTime<Ts...>::VALUE_IN_MS);
};

/// An overload which returns response time in milliseconds for a type.
template<typename T>
struct ResponseTime<T>
{
private:
    static constexpr double REQUEST_END_TO_RESPONSE_START_IN_MS = 5.0;
    static constexpr double TRANSFER_TIME_AT_8N1_9600_BPS_IN_MS = 1000.0 * (1 + 8 + 0 + 1)
            * sizeof(T) / 9600;
    static constexpr double ERROR_MARGIN_IN_MS = 2.0;

public:
    static constexpr uint16_t VALUE_IN_MS = REQUEST_END_TO_RESPONSE_START_IN_MS
            + TRANSFER_TIME_AT_8N1_9600_BPS_IN_MS + ERROR_MARGIN_IN_MS + 1.0;
};

}
