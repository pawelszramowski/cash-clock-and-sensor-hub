#pragma once

#include <stdint.h>

#include "common.h"

#include "Hpma115s0/Protocol.hpp"

namespace Hpma115s0Fsm {

class State
{
public:
    virtual ~State()
    {
        // Do nothing.
    }

    virtual void OnEntry() = 0;
    virtual State* OnPoll() = 0;
};

class InitializationState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;

public:
    static constexpr uint16_t INITIALIZATION_TIME_IN_MS = 2000U;
};

class DisableAutoSendState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;
};

class StopParticleMeasurementState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;
};

class SensorDisabledState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;
};

class StartParticleMeasurementState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;
};

class SensorStabilizationState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;

public:
    static constexpr uint16_t STABILIZATION_TIME_IN_MS = 6000U;
};

class SensorEnabledState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;

public:
    static constexpr uint16_t MIN_POLL_PERIOD_IN_MS = 1000U;
};

class ReadParticleMeasurementResultsState: public State
{
    virtual void OnEntry() override;
    virtual State* OnPoll() override;
};

class Machine
{
public:
    struct Pm2p5
    {
        using Value_t = uint16_t;

        Value_t m_ValueInUgPerM3;

        static constexpr Value_t MINIMUM_VALUE_IN_UG_PER_M3 = Hpma115s0Protocol::ParticleMeasurementResults::MINIMUM_VALUE_IN_UG_PER_M3;
        static constexpr Value_t MAXIMUM_VALUE_IN_UG_PER_M3 = Hpma115s0Protocol::ParticleMeasurementResults::MAXIMUM_VALUE_IN_UG_PER_M3;
        static constexpr Value_t RESOLUTION_IN_UG_PER_M3 = 1U;
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 1000U;

        bool IsValid() const
        {
            static_assert(MINIMUM_VALUE_IN_UG_PER_M3 == 0U, "Use non-zero minimum value in validity check!");
            return m_ValueInUgPerM3 <= MAXIMUM_VALUE_IN_UG_PER_M3;
        }

        void Reset()
        {
            m_ValueInUgPerM3 = UINT16_MAX;
        }
    };

    static void Startup()
    {
        m_Pm2p5.Reset();

        m_pCurrentState = &GetInitializationState();
        m_pCurrentState->OnEntry();
    }

    static void Poll()
    {
        ASSERT_ALWAYS(m_pCurrentState != nullptr);
        State* const pNextState = m_pCurrentState->OnPoll();
        if (pNextState != nullptr)
        {
            m_pCurrentState = pNextState;
            m_pCurrentState->OnEntry();
        }
    }

    static void Enable(
            const bool enabled)
    {
        m_Enabled = enabled;
    }

    static bool IsEnabled()
    {
        return m_Enabled;
    }

    static Pm2p5& GetPm2p5()
    {
        return m_Pm2p5;
    }

    static State& GetInitializationState()
    {
        return m_InitializationState;
    }

    static State& GetDisableAutoSendState()
    {
        return m_DisableAutoSendState;
    }

    static State& GetSensorDisabledState()
    {
        return m_SensorDisabledState;
    }

    static State& GetStopParticleMeasurementState()
    {
        return m_StopParticleMeasurementState;
    }

    static State& GetStartParticleMeasurementState()
    {
        return m_StartParticleMeasurementState;
    }

    static State& GetSensorStabilizationState()
    {
        return m_SensorStabilizationState;
    }

    static State& GetSensorEnabledState()
    {
        return m_SensorEnabledState;
    }

    static State& GetReadParticleMeasurementResultsState()
    {
        return m_ReadParticleMeasurementResultsState;
    }

private:
    static InitializationState m_InitializationState;
    static DisableAutoSendState m_DisableAutoSendState;
    static StopParticleMeasurementState m_StopParticleMeasurementState;
    static SensorDisabledState m_SensorDisabledState;
    static StartParticleMeasurementState m_StartParticleMeasurementState;
    static SensorStabilizationState m_SensorStabilizationState;
    static SensorEnabledState m_SensorEnabledState;
    static ReadParticleMeasurementResultsState m_ReadParticleMeasurementResultsState;

    static State* m_pCurrentState;
    static bool m_Enabled;
    static Pm2p5 m_Pm2p5;

    static_assert(Pm2p5::SAMPLING_PERIOD_IN_MS >= SensorEnabledState::MIN_POLL_PERIOD_IN_MS, "Invalid sampling period!");
};

}
