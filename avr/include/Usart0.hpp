#pragma once

#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

#include "common.h"

class Usart0
{
public:
    Usart0() = delete;
    Usart0(
            const Usart0&) = delete;
    Usart0(
            Usart0&&) = delete;
    Usart0& operator =(
            const Usart0&) = delete;
    Usart0& operator =(
            Usart0&&) = delete;
    ~Usart0() = delete;

    class RxErrors
    {
    public:
        bool IsFrameError() const
        {
            return (m_Ucsr0a & BIT_VAL(FE0)) != 0U;
        }
        bool IsDataOverrunError() const
        {
            return (m_Ucsr0a & BIT_VAL(DOR0)) != 0U;
        }
        bool IsParityError() const
        {
            return (m_Ucsr0a & BIT_VAL(UPE0)) != 0U;
        }
        operator bool() const
        {
            return (m_Ucsr0a & (BIT_VAL(FE0) | BIT_VAL(DOR0) | BIT_VAL(UPE0))) != 0U;
        }

    private:
        friend class Usart0;

        void Set(
                const uint8_t ucsr0a)
        {
            m_Ucsr0a = ucsr0a;
        }

        void Clear()
        {
            m_Ucsr0a = 0U;
        }

        uint8_t m_Ucsr0a = 0U;
    };

    enum Parity : uint8_t
    {
        PARITY_NONE = 0U << UPM01 | 0U << UPM00,
        PARITY_EVEN = 1U << UPM01 | 0U << UPM00,
        PARITY_ODD = 1U << UPM01 | 1U << UPM00,
    };

    enum StopBits : uint8_t
    {
        STOP_BITS_1_BIT = 0U << USBS0,
        STOP_BITS_2_BIT = 1U << USBS0,
    };

    enum CharSize : uint8_t
    {
        CHAR_SIZE_5_BITS = 0U << UCSZ01 | 0U << UCSZ00,
        CHAR_SIZE_6_BITS = 0U << UCSZ01 | 1U << UCSZ00,
        CHAR_SIZE_7_BITS = 1U << UCSZ01 | 0U << UCSZ00,
        CHAR_SIZE_8_BITS = 1U << UCSZ01 | 1U << UCSZ00,
    };

    template<uint32_t BAUD_RATE_IN_BPS>
    static void Startup(
            const CharSize charSize,
            const Parity parity,
            const StopBits stopBits)
    {
        static constexpr uint8_t UCSR0A_NORMAL_MODE = 0U << U2X0;
        static constexpr uint8_t UCSR0A_DISABLE_MPM_MODE = 0U << MPCM0;
        static constexpr uint8_t UCSR0B_DISABLE_INTERRUPTS = 0U << RXCIE0 | 0U << TXCIE0
                | 0U << UDRIE0;
        static constexpr uint8_t UCSR0B_DISABLE_TRANSFER = 0U << RXEN0 | 0U << TXEN0;
        static constexpr uint8_t UCSR0C_ASYNCH_UART = 0U << UMSEL01 | 0U << UMSEL00;
        static constexpr uint8_t UCSR0C_PARITY_MASK = BIT_VAL(UPM01) | BIT_VAL(UPM00);
        static constexpr uint8_t UCSR0C_STOP_BITS_MASK = BIT_VAL(USBS0);
        static constexpr uint8_t UCSR0C_CHAR_SIZE_MASK = BIT_VAL(UCSZ01) | BIT_VAL(UCSZ00);

        UCSR0B = UCSR0B_DISABLE_INTERRUPTS | UCSR0B_DISABLE_TRANSFER;
        UCSR0C = UCSR0C_ASYNCH_UART | (parity & UCSR0C_PARITY_MASK)
                | (stopBits & UCSR0C_STOP_BITS_MASK) | (charSize & UCSR0C_CHAR_SIZE_MASK);
        UCSR0A = UCSR0A_NORMAL_MODE | UCSR0A_DISABLE_MPM_MODE;

        UBRR0 = F_CPU / (16U * BAUD_RATE_IN_BPS) - 1U;
    }

    static void EnableTx(
            const bool enable)
    {
        if (enable)
        {
            UCSR0B |= BIT_VAL(TXEN0);
        }
        else
        {
            UCSR0B &= ~BIT_VAL(TXEN0);
        }
    }

    static bool IsTxEmpty()
    {
        return (UCSR0A & BIT_VAL(UDRE0)) != 0U;
    }

    static void Write(
            const void* const pData,
            const small_size_t size)
    {
        auto pByte = static_cast<const uint8_t*>(pData);

        for (auto bytesLeft = size; bytesLeft > 0U; --bytesLeft)
        {
            while (!IsTxEmpty())
            {
                // Do nothing. Purposefully do not reset watchdog here.
            }

            UDR0 = *pByte++;
        }
    }

    template<typename T>
    static void Write(
            const T& rData)
    {
        Write(&rData, sizeof(T));
    }

    static bool IsTxComplete()
    {
        const uint8_t ucsr0a = UCSR0A;
        UCSR0A = ucsr0a & (BIT_VAL(TXC0) | BIT_VAL(U2X0) | BIT_VAL(MPCM0));
        return (ucsr0a & BIT_VAL(TXC0)) != 0U;
    }

    static void EnableRx(
            const bool enable)
    {
        if (enable)
        {
            UCSR0B |= BIT_VAL(RXEN0);
        }
        else
        {
            UCSR0B &= ~BIT_VAL(RXEN0);
        }
    }

    static bool IsRxComplete()
    {
        return (UCSR0A & BIT_VAL(RXC0)) != 0U;
    }

    static small_size_t Read(
            void* const pData,
            const small_size_t expectedSize)
    {
        m_LastRxErrors.Clear();

        auto pByte = static_cast<uint8_t*>(pData);
        small_size_t actualSize = 0U;

        while (actualSize < expectedSize)
        {
            while (!IsRxComplete())
            {
                // Do nothing. Purposefully do not reset watchdog here.
            }

            m_LastRxErrors.Set(UCSR0A);
            *pByte++ = UDR0;
            ++actualSize;

            if (m_LastRxErrors)
            {
                break;
            }
        }

        return actualSize;
    }

    template<typename T>
    static small_size_t Read(
            T& rData)
    {
        return Read(&rData, sizeof(T));
    }

    static small_size_t ReadAvailable(
            void* const pData,
            const small_size_t maxSize)
    {
        m_LastRxErrors.Clear();

        auto pByte = static_cast<uint8_t*>(pData);
        small_size_t actualSize = 0U;

        while (actualSize < maxSize)
        {
            if (!IsRxComplete())
            {
                break;
            }

            m_LastRxErrors.Set(UCSR0A);
            *pByte++ = UDR0;
            ++actualSize;

            if (m_LastRxErrors)
            {
                break;
            }
        }

        return actualSize;
    }

    template<typename T>
    static small_size_t ReadAvailable(
            T& rData)
    {
        return ReadAvailable(&rData, sizeof(T));
    }

    static RxErrors GetLastRxErrors()
    {
        return m_LastRxErrors;
    }

    static void FlushRx()
    {
        m_LastRxErrors.Clear();
        while (IsRxComplete())
        {
            (void)UDR0;
        }
    }

private:
    static RxErrors m_LastRxErrors;
};

