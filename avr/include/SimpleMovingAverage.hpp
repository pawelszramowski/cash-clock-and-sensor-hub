#pragma once

#include <stddef.h>
#include <stdint.h>

#include "common.h"
#include "common.hpp"

/// Simple moving average to smooth out input data fluctuations.
template<typename T, small_size_t AVERAGED_SAMPLES>
class SimpleMovingAverage
{
public:
    void AddSample(
            const T sample)
    {
        m_Samples[m_CurrentSampleIndex] = sample;

        if (++m_CurrentSampleIndex >= AVERAGED_SAMPLES)
        {
            m_CurrentSampleIndex = 0;
            m_IsFilled = true;
        }

        if (m_IsFilled)
        {
            T sum = 0;
            for (Index_t i = 0; i < AVERAGED_SAMPLES; ++i)
            {
                sum += m_Samples[i];
            }

            m_AverageValue = sum / AVERAGED_SAMPLES;
        }
        else
        {
            m_AverageValue = sample;
        }
    }

    T GetAverageValue() const
    {
        return m_AverageValue;
    }

private:
    using Index_t = typename std::conditional<(AVERAGED_SAMPLES <= UINT8_MAX), uint_fast8_t, typename std::conditional<(AVERAGED_SAMPLES <= UINT16_MAX), uint_fast16_t, uint_fast32_t>::type>::type;

    T m_Samples[AVERAGED_SAMPLES] = {};
    Index_t m_CurrentSampleIndex = 0;
    bool m_IsFilled = false;
    T m_AverageValue = 0;
};
