#pragma once

#include <stdint.h>

#include "Bcd/Date.hpp"
#include "Bcd/Time.hpp"
#include "common.h"

#include "I2c/Master.hpp"
#include "Pcf8563.hpp"
#include "Tc/PeriodicTimer.hpp"

/// Wrapper for a real-time clock.
class Rtc
{
public:
    Rtc() = delete;
    Rtc(
            const Rtc&) = delete;
    Rtc(
            Rtc&&) = delete;
    Rtc& operator =(
            const Rtc&) = delete;
    Rtc& operator =(
            Rtc&&) = delete;
    ~Rtc() = delete;

    static constexpr uint32_t MAX_SCL_FREQUENCY_IN_HZ = Pcf8563::MAX_SCL_FREQUENCY_IN_HZ;

    static void Startup()
    {
        Pcf8563::Startup(PCF8563_MAX_STARTUP_TIME_IN_MS);

        m_PollTimer.Start(MIN_POLL_PERIOD_IN_MS, true);

        m_Time.SetToDayStart();
        m_Date.SetToCenturyStart();
        m_IsSetRequired = false;
    }

    static void Poll()
    {
        if (m_PollTimer.IsDue())
        {
            ASSERT_ALWAYS(Pcf8563::GetValidatedTimeDate(m_Time, m_Date, m_IsSetRequired)
                    == I2c::Master::NO_ERROR);
        }
    }

    static bool SetTimeDate(
            const Bcd::Time& rTime,
            const Bcd::Date& rDate)
    {
        switch (Pcf8563::SetValidatedTimeDate(rTime, rDate))
        {
        case I2c::Master::NO_ERROR:
            m_Time = rTime;
            m_Date = rDate;
            m_IsSetRequired = false;
            return true;
        case I2c::Master::ERR_DATA:
            return false;
        default:
            ASSERT_ALWAYS(false);
            return false;
        }
    }

    static const Bcd::Time& GetTime()
    {
        return m_Time;
    }

    static const Bcd::Date& GetDate()
    {
        return m_Date;
    }

    static bool IsSetRequired()
    {
        return m_IsSetRequired;
    }

private:
    static constexpr uint16_t PCF8563_MAX_STARTUP_TIME_IN_MS = 5000U;
    static constexpr uint16_t MIN_POLL_PERIOD_IN_MS = 500U;

    static PeriodicTimer m_PollTimer;

    static Bcd::Time m_Time;
    static Bcd::Date m_Date;
    static bool m_IsSetRequired;
};
