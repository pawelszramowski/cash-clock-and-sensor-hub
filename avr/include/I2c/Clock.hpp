#pragma once

#include <avr/io.h>
#include <stdint.h>

namespace I2cUtils {

/// Wrapper for TWI clock prescaler calculations.
class Clock
{
public:
    Clock() = delete;
    Clock(
            const Clock&) = delete;
    Clock(
            Clock&&) = delete;
    Clock& operator =(
            const Clock&) = delete;
    Clock& operator =(
            Clock&&) = delete;
    ~Clock() = delete;

private:
    struct Prescaler
    {
        static constexpr uint16_t INVALID_VALUE = 0U;
        static constexpr uint8_t INVALID_PS = 0U;

        uint16_t m_Value;
        uint8_t m_Ps;
    };

    template<const Prescaler& PRESCALER>
    static constexpr uint16_t GetPrescalerValue(
            const uint32_t sclFrequencyInHz)
    {
        return (GetBitRateRegisterValue(PRESCALER.m_Value, sclFrequencyInHz) <= UINT8_MAX) ?
                PRESCALER.m_Value : Prescaler::INVALID_VALUE;
    }

    template<const Prescaler& PRESCALER0, const Prescaler& PRESCALER1,
            const Prescaler& ...PRESCALERS>
    static constexpr uint16_t GetPrescalerValue(
            const uint32_t sclFrequencyInHz)
    {
        return (GetPrescalerValue<PRESCALER0>(sclFrequencyInHz) != Prescaler::INVALID_VALUE) ?
                GetPrescalerValue<PRESCALER0>(sclFrequencyInHz) :
                GetPrescalerValue<PRESCALER1, PRESCALERS...>(sclFrequencyInHz);
    }

    template<const Prescaler& PRESCALER>
    static constexpr uint8_t GetPrescalerSelect(
            const uint16_t prescalerValue)
    {
        return (PRESCALER.m_Value == prescalerValue) ? PRESCALER.m_Ps : Prescaler::INVALID_PS;
    }

    template<const Prescaler& PRESCALER0, const Prescaler& PRESCALER1,
            const Prescaler& ...PRESCALERS>
    static constexpr uint8_t GetPrescalerSelect(
            const uint16_t prescalerValue)
    {
        return (GetPrescalerSelect<PRESCALER0>(prescalerValue) != Prescaler::INVALID_PS) ?
                GetPrescalerSelect<PRESCALER0>(prescalerValue) :
                GetPrescalerSelect<PRESCALER1, PRESCALERS...>(prescalerValue);
    }

    static constexpr Prescaler P0 { 1U, 0U << TWPS1 | 0U << TWPS0 };
    static constexpr Prescaler P1 { 4U, 0U << TWPS1 | 1U << TWPS0 };
    static constexpr Prescaler P2 { 16U, 1U << TWPS1 | 0U << TWPS0 };
    static constexpr Prescaler P3 { 64U, 1U << TWPS1 | 1U << TWPS0 };

public:
    static constexpr uint16_t GetPrescalerValue(
            const uint32_t sclFrequencyInHz)
    {
        return GetPrescalerValue<P0, P1, P2, P3>(sclFrequencyInHz);
    }

    static constexpr uint8_t GetPrescalerSelect(
            const uint16_t prescalerValue)
    {
        return GetPrescalerSelect<P0, P1, P2, P3>(prescalerValue);
    }

    static constexpr uint32_t GetBitRateRegisterValue(
            const uint16_t prescalerValue,
            const uint32_t sclFrequencyInHz)
    {
        return ((prescalerValue != 0U) && (sclFrequencyInHz != 0U)) ?
                ((F_CPU - 16UL * sclFrequencyInHz) / (2UL * prescalerValue * sclFrequencyInHz)) : 0U;
    }
};

}
