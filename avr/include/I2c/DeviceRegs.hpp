#pragma once

#include <stdint.h>

#include "common.h"

#include "I2c/Master.hpp"

namespace I2c {

/// A set of continuous single-byte registers of an I2C slave.
template<uint8_t FIRST_REGISTER_ADDRESS, uint8_t LAST_REGISTER_ADDRESS = FIRST_REGISTER_ADDRESS>
class DeviceRegs
{
    static_assert(FIRST_REGISTER_ADDRESS <= LAST_REGISTER_ADDRESS, "Register addresses must be increasing!");

public:
    /// @param registerAddress Index from FIRST_REGISTER_ADDRESS to LAST_REGISTER_ADDRESS.
    uint8_t& operator [](
            const uint8_t registerAddress)
    {
        ASSERT_ALWAYS((registerAddress >= FIRST_REGISTER_ADDRESS)
                && (registerAddress <= LAST_REGISTER_ADDRESS));
        return m_Buffer[DATA_OFFSET + registerAddress - FIRST_REGISTER_ADDRESS];
    }

    /// Write to device which expects first register address before data
    /// and automatically increments it after each register value.
    I2c::Master::Error Write(
            const uint8_t slaveAddress,
            const bool transmitStop = I2c::Master::TRANSMIT_STOP_DEFAULT) const
    {
        return I2c::Master::Write(slaveAddress, m_Buffer, transmitStop);
    }

    /// Write to device which expects register address before each register value.
    I2c::Master::Error WriteNoAutoIncrement(
            const uint8_t slaveAddress,
            const bool transmitStop = I2c::Master::TRANSMIT_STOP_DEFAULT) const
    {
        if (DATA_SIZE > 1U)
        {
            uint8_t buffer[DATA_SIZE * 2U];
            for (uint8_t i = 0U; i < DATA_SIZE; ++i)
            {
                buffer[ADDRESS_OFFSET + i * 2U] = FIRST_REGISTER_ADDRESS + i;
                buffer[DATA_OFFSET + i * 2U] = m_Buffer[DATA_OFFSET + i];
            }
            return I2c::Master::Write(slaveAddress, buffer, transmitStop);
        }
        else
        {
            return I2c::Master::Write(slaveAddress, m_Buffer, transmitStop);
        }
    }

    I2c::Master::Error Read(
            const uint8_t slaveAddress,
            const bool transmitStop = I2c::Master::TRANSMIT_STOP_DEFAULT)
    {
        return I2c::Master::Read(slaveAddress, FIRST_REGISTER_ADDRESS, &m_Buffer[DATA_OFFSET],
                                 DATA_SIZE,
                                 transmitStop);
    }

    template<typename T>
    T* As()
    {
        static_assert(sizeof(T) == DATA_SIZE, "Invalid size!");
        return reinterpret_cast<T*>(&m_Buffer[DATA_OFFSET]);
    }

private:
    static constexpr uint8_t ADDRESS_OFFSET = 0U;
    static constexpr uint8_t DATA_OFFSET = 1U;
    static constexpr uint8_t DATA_SIZE = LAST_REGISTER_ADDRESS - FIRST_REGISTER_ADDRESS + 1U;

    uint8_t m_Buffer[DATA_OFFSET + DATA_SIZE] = { FIRST_REGISTER_ADDRESS };
};

}
