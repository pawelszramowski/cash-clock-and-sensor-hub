#pragma once

#include <avr/io.h>
#include <avr/wdt.h>
#include <stddef.h>
#include <stdint.h>
#include <util/twi.h>

#include "common.h"
#include "Tc/OneshotTimer.hpp"

#include "I2c/Clock.hpp"

namespace I2c {

/// TWI interface in I2C master mode.
class Master
{
public:
    Master() = delete;
    Master(
            const Master&) = delete;
    Master(
            Master&&) = delete;
    Master& operator =(
            const Master&) = delete;
    Master& operator =(
            Master&&) = delete;
    ~Master() = delete;

    enum Error
    {
        NO_ERROR,
        ERR_PARAM,
        ERR_ADDR_NACK,
        ERR_DATA_NACK,
        ERR_ARB_LOST,
        ERR_BUS,

        // Errors to be used by I2C device drivers.
        ERR_DATA,
        ERR_CRC,
    };

    static constexpr bool TRANSMIT_STOP_DEFAULT = true;

    template<uint32_t SCL_FREQUENCY_IN_HZ>
    static void Startup()
    {
        static constexpr auto PRESCALER = I2cUtils::Clock::GetPrescalerValue(SCL_FREQUENCY_IN_HZ);
        static_assert(PRESCALER != 0U, "Requested SCL frequency is not achievable!");

        TWCR = 0U;
        TWBR = I2cUtils::Clock::GetBitRateRegisterValue(PRESCALER, SCL_FREQUENCY_IN_HZ);
        TWSR = I2cUtils::Clock::GetPrescalerSelect(PRESCALER);
    }

    static bool IsDeviceAvailable(
            const uint8_t slaveAddress,
            const uint16_t maxWaitingTimeInMs = 0U,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        bool isDeviceAvailable;
        OneshotTimer waitTimer;
        waitTimer.Start(maxWaitingTimeInMs);

        do
        {
            isDeviceAvailable = (ProbeDevice(slaveAddress, transmitStop) == NO_ERROR);
            wdt_reset();
        } while (!isDeviceAvailable && !waitTimer.IsExpired() && waitTimer.IsStarted());

        return isDeviceAvailable;
    }

    static Error Write(
            const uint8_t slaveAddress,
            const void* const pData,
            const small_size_t size,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        return Transfer(GetWriteAddress(slaveAddress), const_cast<void*>(pData), size, transmitStop);
    }

    static Error Read(
            const uint8_t slaveAddress,
            void* const pData,
            const small_size_t size,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        return Transfer(GetReadAddress(slaveAddress), pData, size, transmitStop);
    }

    static Error Read(
            const uint8_t slaveAddress,
            const uint8_t registerAddress,
            void* const pData,
            const small_size_t size,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        auto error = Write(slaveAddress, registerAddress, transmitStop);

        if (error == NO_ERROR)
        {
            error = Read(slaveAddress, pData, size, transmitStop);
        }

        return error;
    }

    static Error Transfer(
            void* const pData,
            const small_size_t size,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        if ((pData == nullptr) || (size == 0U))
        {
            return ERR_PARAM;
        }

        auto pByte = static_cast<uint8_t*>(pData);
        const auto slaveAddress = *pByte++;

        return Transfer(slaveAddress, pByte, size - sizeof(slaveAddress), transmitStop);
    }

    template<typename T>
    static Error Write(
            const uint8_t slaveAddress,
            const T& rData,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        return Write(slaveAddress, &rData, sizeof(T), transmitStop);
    }

    template<typename T>
    static Error Read(
            const uint8_t slaveAddress,
            T& rData,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        return Read(slaveAddress, &rData, sizeof(T), transmitStop);
    }

    template<typename T>
    static Error Read(
            const uint8_t slaveAddress,
            const uint8_t registerAddress,
            T& rData,
            const bool transmitStop = TRANSMIT_STOP_DEFAULT)
    {
        return Read(slaveAddress, registerAddress, &rData, sizeof(T), transmitStop);
    }

private:
    static uint8_t GetWriteAddress(
            const uint8_t slaveAddress)
    {
        return slaveAddress & ~SLA_RW_MASK;
    }

    static uint8_t GetReadAddress(
            const uint8_t slaveAddress)
    {
        return slaveAddress | SLA_RW_MASK;
    }

    static Error ProbeDevice(
            const uint8_t slaveAddress,
            const bool transmitStop)
    {
        return Write(slaveAddress, nullptr, 0U, transmitStop);
    }

    /// @param size Size of data to be transferred. Transmitting Stop condition after acknowledged
    /// SLA+R is not supported by TWI interface (TWSTO remains set indefinitely), so at least one
    /// data byte must be read in such case.
    static Error Transfer(
            const uint8_t slaveAddress,
            void* const pData,
            const small_size_t size,
            const bool transmitStop)
    {
        if (slaveAddress == GetReadAddress(slaveAddress))
        {
            if ((size == 0U) || (pData == nullptr))
            {
                return ERR_PARAM;
            }
        }
        else
        {
            if ((size > 0U) && (pData == nullptr))
            {
                return ERR_PARAM;
            }
        }

        TransmitStart();

        auto pByte = static_cast<uint8_t*>(pData);
        auto bytesLeft = size;

        while (true)
        {
            WaitForBusEvent();

            switch (TW_STATUS)
            {
            case TW_START: // Start condition transmitted.
            case TW_REP_START: // Repeated Start condition transmitted.
                TransmitByte(slaveAddress);

                break;
            case TW_MT_SLA_NACK: // SLA+W transmitted, NACK received.
            case TW_MR_SLA_NACK: // SLA+R transmitted, NACK received.
                if (transmitStop)
                {
                    TransmitStop();
                }
                return ERR_ADDR_NACK;

                break;
            case TW_MT_ARB_LOST: // Arbitration lost in SLA+R/W or data.
                // TW_MR_ARB_LOST has got the same value as TW_MT_ARB_LOST.
                ReleaseBus();
                return ERR_ARB_LOST;

                break;
            case TW_MT_SLA_ACK: // SLA+W transmitted, ACK received.
            case TW_MT_DATA_ACK: // Data transmitted, ACK received.
                if (bytesLeft > 0U)
                {
                    TransmitByte(*pByte++);
                    --bytesLeft;
                }
                else
                {
                    if (transmitStop)
                    {
                        TransmitStop();
                    }
                    return NO_ERROR;
                }

                break;
            case TW_MT_DATA_NACK: // Data transmitted, NACK received.
                if (transmitStop)
                {
                    TransmitStop();
                }
                return ERR_DATA_NACK;

                break;
            case TW_MR_SLA_ACK: // SLA+R transmitted, ACK received.
                ASSERT_ALWAYS(bytesLeft > 0U);

                ReceiveByte(bytesLeft == 1U);
                --bytesLeft;

                break;
            case TW_MR_DATA_ACK: // Data received, ACK returned.
                ASSERT_ALWAYS(bytesLeft > 0U);

                *pByte++ = GetReceivedByte();
                ReceiveByte(bytesLeft == 1U);
                --bytesLeft;

                break;
            case TW_MR_DATA_NACK: // Data received, NACK returned.
                ASSERT_ALWAYS(bytesLeft == 0U);

                *pByte++ = GetReceivedByte();

                if (transmitStop)
                {
                    TransmitStop();
                }
                return NO_ERROR;

                break;
            case TW_BUS_ERROR: // Illegal Start or Stop condition.
                ClearError();
                return ERR_BUS;

                break;
            case TW_NO_INFO: // No state information available, interrupt flag not set.
            default: // Slave Transmitter or Slave Receiver states.
                ASSERT_ALWAYS(false);

                break;
            }
        }
    }

    static void WaitForBusEvent()
    {
        while ((TWCR & BIT_VAL(TWINT)) == 0U)
        {
            // Do nothing.
        }
    }

    static void TransmitStart()
    {
        TWCR = BIT_VAL(TWINT) | BIT_VAL(TWSTA) | BIT_VAL(TWEN);
    }

    static void TransmitByte(
            const uint8_t data)
    {
        TWDR = data;
        TWCR = BIT_VAL(TWINT) | BIT_VAL(TWEN);
    }

    static void ReceiveByte(
            const bool isLastInTransfer)
    {
        TWCR = isLastInTransfer ? (BIT_VAL(TWINT) | BIT_VAL(TWEN)) :
                                  (BIT_VAL(TWINT) | BIT_VAL(TWEN) | BIT_VAL(TWEA));
    }

    static uint8_t GetReceivedByte()
    {
        return TWDR;
    }

    static void TransmitStop()
    {
        TWCR = BIT_VAL(TWINT) | BIT_VAL(TWSTO) | BIT_VAL(TWEN);

        while ((TWCR & BIT_VAL(TWSTO)) != 0U)
        {
            // Do nothing.
        }
    }

    static void ReleaseBus()
    {
        TWCR = BIT_VAL(TWINT) | BIT_VAL(TWEN);
    }

    static void ClearError()
    {
        TransmitStop();
    }

    static constexpr uint8_t SLA_RW_MASK = BIT_VAL(0U);
};

}
