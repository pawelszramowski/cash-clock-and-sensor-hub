#pragma once

#include <limits.h>
#include <stdint.h>

#include "common.h"

#include "I2c/Master.hpp"
#include "Tc/PeriodicTimer.hpp"

/// SHT2X I2C humidity and temperature sensor.
class Sht2x
{
public:
    Sht2x() = delete;
    Sht2x(
            const Sht2x&) = delete;
    Sht2x(
            Sht2x&&) = delete;
    Sht2x& operator =(
            const Sht2x&) = delete;
    Sht2x& operator =(
            Sht2x&&) = delete;
    ~Sht2x() = delete;

    struct Temperature
    {
        using Value_t = int16_t;

        Value_t m_ValueIn0p01DegC;

        static constexpr Value_t MINIMUM_VALUE_IN_0P01_DEG_C = -4000;
        static constexpr Value_t MAXIMUM_VALUE_IN_0P01_DEG_C = 12500;
        static constexpr Value_t RESOLUTION_IN_0P01_DEG_C = 1;
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 1000U;

        bool IsValid() const
        {
            return (m_ValueIn0p01DegC >= MINIMUM_VALUE_IN_0P01_DEG_C)
                    && (m_ValueIn0p01DegC <= MAXIMUM_VALUE_IN_0P01_DEG_C);
        }

        void Reset()
        {
            m_ValueIn0p01DegC = INT16_MAX;
        }
    };

    struct RelativeHumidity
    {
        using Value_t = uint16_t;

        Value_t m_ValueIn0p01Pct;

        static constexpr Value_t MINIMUM_VALUE_IN_0P01_PCT = 0U;
        static constexpr Value_t MAXIMUM_VALUE_IN_0P01_PCT = 10000U;
        static constexpr Value_t RESOLUTION_IN_0P01_PCT = 4U;
        static constexpr uint16_t SAMPLING_PERIOD_IN_MS = 1000U;

        bool IsValid() const
        {
            static_assert(MINIMUM_VALUE_IN_0P01_PCT == 0U,
                    "Use non-zero minimum value in validity check!");
            return m_ValueIn0p01Pct <= MAXIMUM_VALUE_IN_0P01_PCT;
        }

        void Reset()
        {
            m_ValueIn0p01Pct = UINT16_MAX;
        }
    };

    static constexpr uint32_t MAX_SCL_FREQUENCY_IN_HZ = 400000UL;

    static void Startup()
    {
        ASSERT_ALWAYS(
                I2c::Master::IsDeviceAvailable(I2C_ADDRESS,
                                               MAX_STARTUP_TIME_IN_MS + MAX_CONVERSION_TIME_IN_MS));
        ASSERT_ALWAYS(VerifyConfiguration() == I2c::Master::NO_ERROR);
        ASSERT_ALWAYS(IssueCommand(COMMAND_TRIGGER_T_NO_HOLD) == I2c::Master::NO_ERROR);

        m_PollTimer.Start(MIN_POLL_PERIOD_IN_MS, true);

        m_Temperature.Reset();
        m_RelativeHumidity.Reset();
    }

    static void Poll()
    {
        if (m_PollTimer.IsDue())
        {
            uint16_t rawValue = 0U;
            const auto error = ReadMeasurementResult(rawValue);

            if (error == I2c::Master::NO_ERROR)
            {
                const bool isRawValueHumidity = ((rawValue & STATUS_MASK) != 0U);
                rawValue &= ~(STATUS_MASK | RESERVED0_MASK);

                Command nextCommand;
                if (isRawValueHumidity)
                {
                    m_RelativeHumidity.m_ValueIn0p01Pct = CalculateRelativeHumidityIn0p01Pct(
                            rawValue);
                    ASSERT_ALWAYS(m_RelativeHumidity.IsValid());

                    nextCommand = COMMAND_TRIGGER_T_NO_HOLD;
                }
                else
                {
                    m_Temperature.m_ValueIn0p01DegC = CalculateTemperatureIn0p01DegC(rawValue);
                    ASSERT_ALWAYS(m_Temperature.IsValid());

                    nextCommand = COMMAND_TRIGGER_RH_NO_HOLD;
                }

                ASSERT_ALWAYS(IssueCommand(nextCommand) == I2c::Master::NO_ERROR);
            }
            else
            {
                ASSERT_ALWAYS(error == I2c::Master::ERR_ADDR_NACK);
            }
        }
    }

    static const Temperature& GetTemperature()
    {
        return m_Temperature;
    }

    static const RelativeHumidity& GetRelativeHumidity()
    {
        return m_RelativeHumidity;
    }

private:
    enum Command : uint8_t
    {
        COMMAND_TRIGGER_T_HOLD = 0xE3U,
        COMMAND_TRIGGER_RH_HOLD = 0xE5U,
        COMMAND_TRIGGER_T_NO_HOLD = 0xF3U,
        COMMAND_TRIGGER_RH_NO_HOLD = 0xF5U,
        COMMAND_WRITE_USER_REGISTER = 0xE6U,
        COMMAND_READ_USER_REGISTER = 0xE7U,
        COMMAND_SOFT_RESET = 0xFEU,
    };

    enum UserRegisterBit : uint8_t
    {
        BIT_RESOLUTION1 = 7U,
        BIT_END_OF_BATTERY = 6U,
        BIT_ENABLE_HEATER = 2U,
        BIT_DISABLE_OTP_RELOAD = 1U,
        BIT_RESOLUTION0 = 0U
    };

    enum MeasurementResultBit : uint8_t
    {
        BIT_STATUS = 1U,
        BIT_RESERVED0 = 0U
    };

    enum Resolution : uint8_t
    {
        RESOLUTION_RH_12B_T_14B = 0U << BIT_RESOLUTION1 | 0U << BIT_RESOLUTION0,
        RESOLUTION_RH_8B_T_12B = 0U << BIT_RESOLUTION1 | 1U << BIT_RESOLUTION0,
        RESOLUTION_RH_10B_T_13B = 1U << BIT_RESOLUTION1 | 0U << BIT_RESOLUTION0,
        RESOLUTION_RH_11B_T_11B = 1U << BIT_RESOLUTION1 | 1U << BIT_RESOLUTION0
    };

    static I2c::Master::Error IssueCommand(
            const Command command)
    {
        return I2c::Master::Write(I2C_ADDRESS, command);
    }

    static I2c::Master::Error VerifyConfiguration()
    {
        uint8_t userRegister = 0U;

        auto error = I2c::Master::Read(I2C_ADDRESS, COMMAND_READ_USER_REGISTER, userRegister);

        if ((error == I2c::Master::NO_ERROR)
                && ((userRegister & USER_REGISTER_MASK) != USER_REGISTER_DEFAULT))
        {
            error = I2c::Master::ERR_DATA;
        }

        return error;
    }

    static I2c::Master::Error ReadMeasurementResult(
            uint16_t& rResult)
    {
        enum
        {
            OFFSET_MSB = 0,
            OFFSET_LSB = 1,
            BYTE_COUNT = 2
        };
        struct
        {
            uint8_t m_Data[BYTE_COUNT];
            uint8_t m_Crc;
        } ATTR_PACKED result;

        auto error = I2c::Master::Read(I2C_ADDRESS, result);

        if (error == I2c::Master::NO_ERROR)
        {
            if (CalculateCrc(result.m_Data) == result.m_Crc)
            {
                rResult = (result.m_Data[OFFSET_MSB] << CHAR_BIT) | result.m_Data[OFFSET_LSB];
            }
            else
            {
                error = I2c::Master::ERR_CRC;
            }
        }

        return error;
    }

    static uint8_t CalculateCrc(
            const void* const pData,
            const small_size_t size)
    {
        auto crc = CRC_INIT;

        auto pByte = reinterpret_cast<const uint8_t*>(pData);
        auto bytesLeft = size;

        while (bytesLeft-- > 0U)
        {
            crc ^= *pByte++;

            for (uint_fast8_t bitsLeft = CHAR_BIT; bitsLeft > 0U; --bitsLeft)
            {
                if ((crc & BIT_VAL(CHAR_BIT - 1)) != 0U)
                {
                    crc <<= 1;
                    crc ^= CRC_POLYNOMIAL;
                }
                else
                {
                    crc <<= 1;
                }
            }
        }

        return crc;
    }

    template<typename T>
    static uint8_t CalculateCrc(
            const T& rData)
    {
        return CalculateCrc(&rData, sizeof(T));
    }

    static Temperature::Value_t CalculateTemperatureIn0p01DegC(
            const uint16_t rawValue)
    {
        // FIXME Offset is added to the temperature to correct for board-level heating!
        return ( // -76759040L // no offset
        -79216640L // offset -1.5*C
        //-80035840L // offset -2.0*C
                + 4393L * static_cast<int32_t>(rawValue)) / 16384L;
    }

    static RelativeHumidity::Value_t CalculateRelativeHumidityIn0p01Pct(
            const uint16_t rawValue)
    {
        return (-9830400L + 3125L * static_cast<int32_t>(rawValue)) / 16384L;
    }

    static constexpr uint8_t I2C_ADDRESS = 0x80U;

    static constexpr uint16_t MAX_STARTUP_TIME_IN_MS = 15U;
    static constexpr uint16_t MAX_CONVERSION_TIME_IN_MS = 85U;

    /// Sensor should not be active more than 10% of the time to keep
    /// self-heating below 0.1*C according to the data sheet.
    static constexpr uint16_t MIN_POLL_PERIOD_IN_MS = 500U;

    static constexpr uint8_t USER_REGISTER_MASK = BIT_VAL(BIT_RESOLUTION1)
            | BIT_VAL(BIT_END_OF_BATTERY) | BIT_VAL(BIT_ENABLE_HEATER)
            | BIT_VAL(BIT_DISABLE_OTP_RELOAD) | BIT_VAL(BIT_RESOLUTION0);

    static constexpr uint8_t STATUS_MASK = BIT_VAL(BIT_STATUS);
    static constexpr uint8_t RESERVED0_MASK = BIT_VAL(BIT_RESERVED0);

    static constexpr uint8_t USER_REGISTER_DEFAULT = RESOLUTION_RH_12B_T_14B
            | 0U << BIT_END_OF_BATTERY | 0U << BIT_ENABLE_HEATER
            | 1U << BIT_DISABLE_OTP_RELOAD;

    /// P(x) = x^8 + x^5 + x^4 + 1 = 0b100110001
    static constexpr uint8_t CRC_POLYNOMIAL = 0x31U;
    static constexpr uint8_t CRC_INIT = 0x00U;

    static PeriodicTimer m_PollTimer;

    static Temperature m_Temperature;
    static RelativeHumidity m_RelativeHumidity;

    static_assert(Temperature::SAMPLING_PERIOD_IN_MS >= 2U * MIN_POLL_PERIOD_IN_MS, "Invalid sampling period!");
    static_assert(RelativeHumidity::SAMPLING_PERIOD_IN_MS >= 2U * MIN_POLL_PERIOD_IN_MS, "Invalid sampling period!");
};
