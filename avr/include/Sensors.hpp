#pragma once

#include <avr/eeprom.h>
#include <stdint.h>

#include "AnalogSensors.hpp"
#include "Bme280.hpp"
#include "Hpma115s0/Hpma115s0.hpp"
#include "PushButton.hpp"
#include "Sht2x.hpp"

/// Wrapper for analog and digital sensors used to decouple
/// sensor data consumers from producers.
class Sensors
{
public:
    Sensors() = delete;
    Sensors(
            const Sensors&) = delete;
    Sensors(
            Sensors&&) = delete;
    Sensors& operator =(
            const Sensors&) = delete;
    Sensors& operator =(
            Sensors&&) = delete;
    ~Sensors() = delete;

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A != 0
    using AtmosphericPressure = Bme280::AtmosphericPressure;
    using Temperature = Bme280::Temperature;
    using RelativeHumidity = Bme280::RelativeHumidity;
#else
    using AtmosphericPressure = AnalogSensors::AtmosphericPressure;
    using Temperature = Sht2x::Temperature;
    using RelativeHumidity = Sht2x::RelativeHumidity;
#endif
    using Pm2p5 = Hpma115s0::Pm2p5;
    using BacklightDutyCycle = AnalogSensors::BacklightDutyCycle;

    static constexpr uint32_t MAX_SCL_FREQUENCY_IN_HZ =
            (USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A != 0) ?
                                                               Bme280::MAX_SCL_FREQUENCY_IN_HZ :
                                                               Sht2x::MAX_SCL_FREQUENCY_IN_HZ;

    static void Startup()
    {
        PushButton::Startup();
        Hpma115s0::Startup();
        SetHpma115s0StateAtPowerOn();
        AnalogSensors::Startup();
        (USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A != 0) ? Bme280::Startup() : Sht2x::Startup();
    }

    static void Poll()
    {
        PushButton::Poll();
        if (PushButton::WasPressed())
        {
            Hpma115s0::Enable(!Hpma115s0::IsEnabled());
        }
        Hpma115s0::Poll();
        AnalogSensors::Poll();
        (USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A != 0) ? Bme280::Poll() : Sht2x::Poll();
    }

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A != 0
    static const AtmosphericPressure& GetAtmosphericPressure()
    {
        return Bme280::GetAtmosphericPressure();
    }

    static const Temperature& GetTemperature()
    {
        return Bme280::GetTemperature();
    }

    static const RelativeHumidity& GetRelativeHumidity()
    {
        return Bme280::GetRelativeHumidity();
    }
#else
    static const AtmosphericPressure& GetAtmosphericPressure()
    {
        return AnalogSensors::GetAtmosphericPressure();
    }

    static const Temperature& GetTemperature()
    {
        return Sht2x::GetTemperature();
    }

    static const RelativeHumidity& GetRelativeHumidity()
    {
        return Sht2x::GetRelativeHumidity();
    }
#endif

    static const Pm2p5& GetPm2p5()
    {
        return Hpma115s0::GetPm2p5();
    }

    static const BacklightDutyCycle& GetBacklightDutyCycle()
    {
        return AnalogSensors::GetBacklightDutyCycle();
    }

private:
    static void SetHpma115s0StateAtPowerOn();

    static uint8_t m_Hpma115s0StateAtPowerOnE EEMEM;

    enum : decltype(m_Hpma115s0StateAtPowerOnE)
    {
        HPMA115S0_STATE_AT_POWER_ON_ENABLED_MARKER = 0x00U,
        HPMA115S0_STATE_AT_POWER_ON_DISABLED_MARKER = 0xFFU
    };

};
