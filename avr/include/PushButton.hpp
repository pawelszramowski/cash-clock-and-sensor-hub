#pragma once

#include <stdint.h>

#include "Gpio/Input.hpp"
#include "Tc/OneshotTimer.hpp"

class PushButton
{
public:
    PushButton() = delete;
    PushButton(
            const PushButton&) = delete;
    PushButton(
            PushButton&&) = delete;
    PushButton& operator =(
            const PushButton&) = delete;
    PushButton& operator =(
            PushButton&&) = delete;
    ~PushButton() = delete;

    static void Startup()
    {
        static constexpr bool ENABLE_INTERNAL_PULL_UP_RESISTOR = true;
        ButtonInput::Startup(ENABLE_INTERNAL_PULL_UP_RESISTOR);
    }

    static void Poll()
    {
        const bool currentState = ButtonInput::GetState();

        if (currentState != m_PreviousState)
        {
            m_DebouncingTimer.Start(MIN_DEBOUNCING_TIME_IN_MS);
        }
        else if (m_DebouncingTimer.IsExpired())
        {
            m_DebouncingTimer.Stop();

            if (!m_IsPressed && (currentState == true))
            {
                m_WasPressed = true;
            }

            m_IsPressed = currentState;
        }

        m_PreviousState = currentState;
    }

    static bool IsPressed()
    {
        Poll();
        return m_IsPressed;
    }

    static bool WasPressed()
    {
        Poll();
        const bool wasPressed = m_WasPressed;
        m_WasPressed = false;
        return wasPressed;
    }

    static bool IsUnstable()
    {
        Poll();
        return m_DebouncingTimer.IsStarted();
    }

private:
    using ButtonInput = Gpio::Input<'C', 3, false>;

    static constexpr uint16_t MIN_DEBOUNCING_TIME_IN_MS = 50U;

    static OneshotTimer m_DebouncingTimer;
    static bool m_PreviousState;

    static bool m_IsPressed;
    static bool m_WasPressed;
};

