#include "AnalogSensors.hpp"

AnalogSensors::AdcSamples AnalogSensors::m_AdcSamples {};

#if USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0
bool AnalogSensors::m_IsAtmosphericPressureOutdated { false };
AnalogSensors::AtmosphericPressure AnalogSensors::m_AtmosphericPressure {};
#endif

bool AnalogSensors::m_IsBacklightDutyCycleOutdated { false };
AnalogSensors::BacklightDutyCycle AnalogSensors::m_BacklightDutyCycle {};
