#include "Hpma115s0/Fsm.hpp"

#include <stddef.h>

#include "Gpio/PushPullOutput.hpp"
#include "Tc/OneshotTimer.hpp"
#include "Tc/StopwatchTimer.hpp"
#include "Tc/SystemTimer.hpp"
#include "Usart0.hpp"

/// Sensor operation:
/// @startuml
/// [*] --> AutoSendEnabled
/// AutoSendEnabled --> AutoSendEnabled: auto send timeout (1 s) / send protocol frame
/// AutoSendEnabled --> AutoSendDisabled: Stop Auto Send received / send Pos ACK
/// AutoSendEnabled --> AutoSendEnabled: receive error or timeout (5 ms) / send Neg ACK
/// AutoSendDisabled --> AutoSendDisabled: receive error or timeout (5 ms) / send Neg ACK
/// AutoSendDisabled --> AutoSendEnabled: Start Auto Send received / send Pos ACK
/// @enduml

using RedLedOutput = Gpio::PushPullOutput<'C', 2, false>;

namespace SharedHelpers
{

static OneshotTimer f_OneshotTimer;

class Uart
{
public:
    enum ReceiveStatus
    {
        RECEIVED_ACK_POS_WITH_DATA,
        RECEIVED_ACK_POS,
        RECEIVED_ACK_NEG,
        RECEIVE_ERROR,
        RECEIVE_TIMEOUT,
    };

    static void Startup()
    {
        Usart0::Startup<9600U>(Usart0::CHAR_SIZE_8_BITS, Usart0::PARITY_NONE,
                               Usart0::STOP_BITS_1_BIT);
        Usart0::EnableTx(true);
        Usart0::EnableRx(true);
    }

    static void TransmitRequest(
            const void* const pRequestFrame,
            const small_size_t size)
    {
        Usart0::Write(pRequestFrame, size);
        Usart0::FlushRx();
        m_ResponseTimer.Start();
        m_ResponseSize = 0U;
    }

    template<typename T>
    static void TransmitRequest(
            const T& rRequestFrame)
    {
        TransmitRequest(&rRequestFrame, sizeof(T));
    }

    static ReceiveStatus ReceiveResponse(
            const uint16_t timeoutInMs,
            const small_size_t posAckWithDataSize = 0U)
    {
        ASSERT_ALWAYS(m_ResponseTimer.IsStarted() && (m_ResponseSize == 0U));
        ASSERT_ALWAYS((posAckWithDataSize == 0U)
                || ((posAckWithDataSize > sizeof(Hpma115s0Protocol::Ack))
                        && (posAckWithDataSize <= sizeof(m_ResponseBuffer))));

        ReceiveStatus status;
        while (true)
        {
            if (!m_ResponseTimer.HasPassed(timeoutInMs + SystemTimer::RESOLUTION_IN_MS))
            {
                m_ResponseSize += Usart0::ReadAvailable(&m_ResponseBuffer[m_ResponseSize],
                                                        sizeof(m_ResponseBuffer) - m_ResponseSize);

                const auto lastRxErrors = Usart0::GetLastRxErrors();
                if (lastRxErrors)
                {
                    status = RECEIVE_ERROR;
                    break;
                }

                auto pAck = GetResponse<Hpma115s0Protocol::Ack>();
                if (pAck != nullptr)
                {
                    if (*pAck == Hpma115s0Protocol::ACK_NEG)
                    {
                        status = RECEIVED_ACK_NEG;
                        break;
                    }
                    else if (*pAck == Hpma115s0Protocol::ACK_POS)
                    {
                        status = RECEIVED_ACK_POS;
                        break;
                    }
                }

                if ((posAckWithDataSize > 0U) && (m_ResponseSize == posAckWithDataSize))
                {
                    status = RECEIVED_ACK_POS_WITH_DATA;
                    break;
                }
            }
            else
            {
                status = RECEIVE_TIMEOUT;
                break;
            }
        }
        m_ResponseTimer.Stop();

        return status;
    }

    template<typename T>
    static const T* GetResponse()
    {
        return (m_ResponseSize == sizeof(T)) ? reinterpret_cast<const T*>(&m_ResponseBuffer[0]) :
                                               nullptr;
    }

    static void FlushReceived()
    {
        Usart0::FlushRx();
    }

    static bool IsAnythingReceived()
    {
        return Usart0::IsRxComplete();
    }

private:
    static uint8_t m_ResponseBuffer[sizeof(Hpma115s0Protocol::ParticleMeasurementResultsFrame)];
    static small_size_t m_ResponseSize;
    static StopwatchTimer m_ResponseTimer;
};

uint8_t Uart::m_ResponseBuffer[] {};
small_size_t Uart::m_ResponseSize {};
StopwatchTimer Uart::m_ResponseTimer {};

}

namespace Hpma115s0Fsm
{

void InitializationState::OnEntry()
{
    SharedHelpers::f_OneshotTimer.Start(INITIALIZATION_TIME_IN_MS);

    SharedHelpers::Uart::Startup();

    RedLedOutput::Startup();

    Machine::GetPm2p5().Reset();
}

State* InitializationState::OnPoll()
{
    return SharedHelpers::f_OneshotTimer.IsExpired() ? &Machine::GetDisableAutoSendState() : nullptr;
}

void DisableAutoSendState::OnEntry()
{
    SharedHelpers::Uart::TransmitRequest(Hpma115s0Protocol::REQ_DISABLE_AUTO_SEND);
}

State* DisableAutoSendState::OnPoll()
{
    const auto status = SharedHelpers::Uart::ReceiveResponse(
            Hpma115s0Protocol::ResponseTime<Hpma115s0Protocol::Ack>::VALUE_IN_MS);

    if (status == SharedHelpers::Uart::RECEIVED_ACK_POS)
    {
        return Machine::IsEnabled() ?
                                      &Machine::GetStartParticleMeasurementState() :
                                      &Machine::GetStopParticleMeasurementState();
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

void StopParticleMeasurementState::OnEntry()
{
    SharedHelpers::Uart::TransmitRequest(Hpma115s0Protocol::REQ_STOP_PARTICLE_MEASUREMENT);
}

State* StopParticleMeasurementState::OnPoll()
{
    const auto status = SharedHelpers::Uart::ReceiveResponse(
            Hpma115s0Protocol::ResponseTime<Hpma115s0Protocol::Ack>::VALUE_IN_MS);

    if (status == SharedHelpers::Uart::RECEIVED_ACK_POS)
    {
        return &Machine::GetSensorDisabledState();
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

void SensorDisabledState::OnEntry()
{
    SharedHelpers::Uart::FlushReceived();

    RedLedOutput::SetState(false);

    Machine::GetPm2p5().Reset();
}

State* SensorDisabledState::OnPoll()
{
    if (!SharedHelpers::Uart::IsAnythingReceived())
    {
        return !Machine::IsEnabled() ? nullptr : &Machine::GetStartParticleMeasurementState();
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

void StartParticleMeasurementState::OnEntry()
{
    SharedHelpers::Uart::TransmitRequest(Hpma115s0Protocol::REQ_START_PARTICLE_MEASUREMENT);
}

State* StartParticleMeasurementState::OnPoll()
{
    const auto status = SharedHelpers::Uart::ReceiveResponse(
            Hpma115s0Protocol::ResponseTime<Hpma115s0Protocol::Ack>::VALUE_IN_MS);

    if (status == SharedHelpers::Uart::RECEIVED_ACK_POS)
    {
        return &Machine::GetSensorStabilizationState();
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

void SensorStabilizationState::OnEntry()
{
    SharedHelpers::f_OneshotTimer.Start(STABILIZATION_TIME_IN_MS);

    SharedHelpers::Uart::FlushReceived();

    RedLedOutput::SetState(true);
}

State* SensorStabilizationState::OnPoll()
{
    if (!SharedHelpers::Uart::IsAnythingReceived())
    {
        if (Machine::IsEnabled())
        {
            return SharedHelpers::f_OneshotTimer.IsExpired() ?
                                                               &Machine::GetSensorEnabledState() :
                                                               nullptr;
        }
        else
        {
            return &Machine::GetStopParticleMeasurementState();
        }
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

void SensorEnabledState::OnEntry()
{
    SharedHelpers::f_OneshotTimer.Start(MIN_POLL_PERIOD_IN_MS);

    SharedHelpers::Uart::FlushReceived();
}

State* SensorEnabledState::OnPoll()
{
    if (!SharedHelpers::Uart::IsAnythingReceived())
    {
        if (Machine::IsEnabled())
        {
            return SharedHelpers::f_OneshotTimer.IsExpired() ?
                    &Machine::GetReadParticleMeasurementResultsState() : nullptr;
        }
        else
        {
            return &Machine::GetStopParticleMeasurementState();
        }
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

void ReadParticleMeasurementResultsState::OnEntry()
{
    SharedHelpers::Uart::TransmitRequest(Hpma115s0Protocol::REQ_READ_PARTICLE_MEASUREMENT_RESULTS);
}

State* ReadParticleMeasurementResultsState::OnPoll()
{
    const auto status = SharedHelpers::Uart::ReceiveResponse(
            Hpma115s0Protocol::ResponseTime<Hpma115s0Protocol::ParticleMeasurementResultsFrame,
                    Hpma115s0Protocol::Ack>::VALUE_IN_MS,
            sizeof(Hpma115s0Protocol::ParticleMeasurementResultsFrame));

    if (status == SharedHelpers::Uart::RECEIVED_ACK_POS_WITH_DATA)
    {
        auto pFrame = SharedHelpers::Uart::GetResponse<
                Hpma115s0Protocol::ParticleMeasurementResultsFrame>();
        ASSERT_ALWAYS(pFrame != nullptr);

        if (pFrame->IsValidResponseTo(
                Hpma115s0Protocol::REQ_READ_PARTICLE_MEASUREMENT_RESULTS.m_Cmd))
        {
            Machine::GetPm2p5().m_ValueInUgPerM3 = pFrame->m_Data.GetPm2p5InUgPerM3();

            return &Machine::GetSensorEnabledState();
        }
        else
        {
            return &Machine::GetInitializationState();
        }
    }
    else
    {
        return &Machine::GetInitializationState();
    }
}

InitializationState Machine::m_InitializationState {};
DisableAutoSendState Machine::m_DisableAutoSendState {};
StopParticleMeasurementState Machine::m_StopParticleMeasurementState {};
SensorDisabledState Machine::m_SensorDisabledState {};
StartParticleMeasurementState Machine::m_StartParticleMeasurementState {};
SensorStabilizationState Machine::m_SensorStabilizationState {};
SensorEnabledState Machine::m_SensorEnabledState {};
ReadParticleMeasurementResultsState Machine::m_ReadParticleMeasurementResultsState {};

State* Machine::m_pCurrentState { nullptr };
bool Machine::m_Enabled { false };
Machine::Pm2p5 Machine::m_Pm2p5 {};

}
