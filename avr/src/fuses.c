// @formatter:off

#include <avr/io.h>
#include <avr/fuse.h>
#include <avr/lock.h>

#include "common.h"

#if !(defined(__AVR_ATmega168__) \
        || defined(__AVR_ATmega168A__) \
        || defined(__AVR_ATmega168P__) \
        || defined(__AVR_ATmega168PA__))
#error "Only Atmel ATmega168/P/A/PA AVR microcontroller is supported!"
#endif

ATTR_USED
const FUSES = {
    /// External 16MHz crystal resonator,
    /// start-up time PWRDWN/RESET: 16k CK/14 CK + 65ms.
    .low = 0xFFU,
    /// Serial program downloading enabled,
    /// brown-out detection level at 4.3V.
    .high = FUSE_SPIEN & FUSE_BODLEVEL1 & FUSE_BODLEVEL0,
#if FUSE_MEMORY_SIZE == 3
    /// Boot reset vector disabled.
    .extended = 0xFFU,
#endif
};

ATTR_USED
const LOCKBITS = LOCKBITS_DEFAULT;
