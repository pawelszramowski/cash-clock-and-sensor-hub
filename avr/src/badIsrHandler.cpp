#include <avr/interrupt.h>

#include "common.h"
#include "Mcu.hpp"

/// @warning This interrupt handler must not return as it corrupts the machine state!
ISR(BADISR_vect, ISR_NAKED)
{
    /// Make sure that __zero_reg__ actually contains a zero, since it may not always be the case
    /// on ISR entry.
    asm volatile ("eor __zero_reg__, __zero_reg__");

    ASSERT_ALWAYS(false);
    Mcu::Reset();
}

