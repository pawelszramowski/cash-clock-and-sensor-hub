#include <avr/wdt.h>
#include <stdint.h>
#include <util/atomic.h>

#include "Backlight.hpp"
#include "common.h"
#include "common.hpp"
#include "Display.hpp"
#include "I2c/Master.hpp"
#include "Mcu.hpp"
#include "Rtc.hpp"
#include "Sensors.hpp"
#include "Tc/SystemTimer.hpp"
#include "Usb/Hid/Collection.hpp"
#include "Usb/Hid/CollectionConfig.hpp"
#include "Usb/Hid/ControlEp.hpp"
#include "Usb/Hid/IntrInEp.hpp"
#include "Usb/Hid/RtcUpdate/Performer.hpp"
#include "Usb/Hid/Sensor/GenericSensor.hpp"
#include "Usb/Hid/Sensor/SingleValue.hpp"
#include "Usb/Hid/Sensor/Usage.hpp"
#include "Usb/VbusSense.hpp"

static void PollAll()
{
    wdt_reset();

    Mcu::Poll();
    SystemTimer::Poll();
    Sensors::Poll();
    Rtc::Poll();
    Display::Update(Rtc::GetTime(), Rtc::GetDate(), Rtc::IsSetRequired(),
                    Sensors::GetAtmosphericPressure(),
                    Sensors::GetTemperature(),
                    Sensors::GetRelativeHumidity(),
                    Sensors::GetPm2p5());

    Backlight::SetDutyCycle(Sensors::GetBacklightDutyCycle().m_Value);
}

int main()
{
    static constexpr uint32_t BOARD_MAX_SCL_FREQUENCY_IN_HZ = 100000U;

    Mcu::Startup();
    Display::EarlyStartup();
    Backlight::Startup();

    Mcu::ConfigureWatchdog(WDTO_60MS);

    SystemTimer::Startup();
    I2c::Master::Startup<nonstd::minv(BOARD_MAX_SCL_FREQUENCY_IN_HZ,
                                      Sensors::MAX_SCL_FREQUENCY_IN_HZ,
                                      Rtc::MAX_SCL_FREQUENCY_IN_HZ)>();
    Sensors::Startup();
    Rtc::Startup();
    Display::LateStartup();
    Usb::VbusSense::Startup();

    while (true)
    {
        if (Usb::VbusSense::IsHostDetected())
        {
            NONATOMIC_BLOCK(NONATOMIC_RESTORESTATE)
            {
                Usb::Hid::Collection hidCollection;

                Usb::Hid::RtcUpdate::Performer rtcUpdatePerformer(
                        hidCollection.m_RtcUpdateReceiver);

                Usb::Hid::IntrInEp hidIntrInEp(hidCollection);
                Usb::Hid::ControlEp::Startup(hidCollection);

                while (Usb::VbusSense::IsHostDetected())
                {
                    PollAll();

                    hidCollection.m_AtmosphericPressure.SetSensorState(
                            Sensors::GetAtmosphericPressure().IsValid() ?
                                    Usb::Hid::Sensor::SensorState::READY :
                                    Usb::Hid::Sensor::SensorState::ERROR);
                    hidCollection.m_AtmosphericPressure.SetValue(
                            Usb::Hid::Sensor::AtmosphericPressure::ConvertToSensorValue(
                                    Sensors::GetAtmosphericPressure().m_ValueInPa));

                    hidCollection.m_Temperature.SetSensorState(
                            Sensors::GetTemperature().IsValid() ?
                                    Usb::Hid::Sensor::SensorState::READY :
                                    Usb::Hid::Sensor::SensorState::ERROR);
                    hidCollection.m_Temperature.SetValue(
                            Usb::Hid::Sensor::Temperature::ConvertToSensorValue(
                                    Sensors::GetTemperature().m_ValueIn0p01DegC));

                    hidCollection.m_RelativeHumidity.SetSensorState(
                            Sensors::GetRelativeHumidity().IsValid() ?
                                    Usb::Hid::Sensor::SensorState::READY :
                                    Usb::Hid::Sensor::SensorState::ERROR);
                    hidCollection.m_RelativeHumidity.SetValue(
                            Usb::Hid::Sensor::RelativeHumidity::ConvertToSensorValue(
                                    Sensors::GetRelativeHumidity().m_ValueIn0p01Pct));

                    hidCollection.m_Pm2p5.SetSensorState(
                            Sensors::GetPm2p5().IsValid() ?
                                    Usb::Hid::Sensor::SensorState::READY :
                                    Usb::Hid::Sensor::SensorState::UNKNOWN);
                    hidCollection.m_Pm2p5.SetValue(
                            Usb::Hid::Sensor::Pm2p5::ConvertToSensorValue(
                                    Sensors::GetPm2p5().m_ValueInUgPerM3));

                    Usb::Hid::ControlEp::Poll();
                    hidIntrInEp.Poll();
                }

                Usb::Hid::ControlEp::Shutdown();
            }
        }
        else
        {
            PollAll();
        }
    }

    ASSERT_ALWAYS(false);
    return 0;
}
