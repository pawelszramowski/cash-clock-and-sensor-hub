// @formatter:off

#include <avr/pgmspace.h>
#include <stdint.h>

#include "common.h"
#include "Usb/vusb.h"

// NOTE: This is a C file since ISO C++ does not allow C99 designated initializers.

#if USB_CFG_INTERFACE_CLASS == 3
    #include "hid_sensor_spec_macros.h"
    #include "Usb/Hid/reportIds.h"

    #define HID_USAGE_PAGE_VENDOR_DEFINED   0x06, 0xA0, 0xFF
    #define HID_USAGE_VENDOR_DEFINED        0x09, 0x01
    #define HID_UNIT_UG_PER_M3              0x66, 0x11, 0x0D // ug/m3 == g/cm3

    static_assert((USB_HID_REPORT_ID_RTC_UPDATE == 1U)
                  && (USB_HID_REPORT_ID_ATMOSPHERIC_PRESSURE_SENSOR == 2U)
                  && (USB_HID_REPORT_ID_TEMPERATURE_SENSOR == 3U)
                  && (USB_HID_REPORT_ID_RELATIVE_HUMIDITY_SENSOR == 4U)
                  && (USB_HID_REPORT_ID_PM2P5_SENSOR == 5U),
                  "Report IDs do NOT match HID report descriptor!");

    static const uint8_t f_HidReportDescriptor[] PROGMEM =
    {
        HID_USAGE_PAGE_VENDOR_DEFINED,
        HID_USAGE_VENDOR_DEFINED,
        HID_COLLECTION(Application),
            HID_REPORT_ID(USB_HID_REPORT_ID_RTC_UPDATE),
            HID_USAGE_VENDOR_DEFINED,
            HID_LOGICAL_MIN_8(0x00),
            HID_LOGICAL_MAX_8(0xFF),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(6),
            HID_OUTPUT(Data_Var_Abs),
        HID_END_COLLECTION,

        HID_USAGE_PAGE_SENSOR,
        HID_USAGE_SENSOR_TYPE_COLLECTION,
        HID_COLLECTION(Application),

            HID_REPORT_ID(USB_HID_REPORT_ID_ATMOSPHERIC_PRESSURE_SENSOR),
            // hid_sensor_spec_report_descriptors.h
            // 4.3.9    Environmental: Atmospheric Pressure
            // bar_report_descriptor[]
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_TYPE_ENVIRONMENTAL_ATMOSPHERIC_PRESSURE,
            HID_COLLECTION(Physical),

                //feature reports (xmit/receive)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_PROPERTY_SENSOR_CONNECTION_TYPE,  // NAry
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(2),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_INTEGRATED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_ATTACHED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_WAKE_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_UNDEFINED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D1_LOW_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D2_STANDBY_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D3_SLEEP_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D4_POWER_OFF_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                HID_FEATURE(Data_Arr_Abs),
                HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORT_INTERVAL,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_32(0xFF,0xFF,0xFF,0xFF),
                HID_REPORT_SIZE(32),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0),
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_ATMOSPHERIC_PRESSURE,HID_USAGE_SENSOR_DATA_MOD_CHANGE_SENSITIVITY_ABS),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0C), // scale default unit Bar to provide 4 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_ATMOSPHERIC_PRESSURE,HID_USAGE_SENSOR_DATA_MOD_MAX),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0C), // scale default unit Bar to provide 4 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_ATMOSPHERIC_PRESSURE,HID_USAGE_SENSOR_DATA_MOD_MIN),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0C), // scale default unit Bar to provide 4 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),

                //input reports (transmit)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_EVENT,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_EVENT_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_PROPERTY_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL,
                    HID_USAGE_SENSOR_EVENT_POLL_RESPONSE_SEL,
                    HID_USAGE_SENSOR_EVENT_CHANGE_SENSITIVITY_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_ATMOSPHERIC_PRESSURE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0C), // scale default unit Bar to provide 4 digits past the decimal point
                HID_INPUT(Data_Var_Abs),

            HID_END_COLLECTION,

            HID_REPORT_ID(USB_HID_REPORT_ID_TEMPERATURE_SENSOR),
            // hid_sensor_spec_report_descriptors.h
            // 4.3.11    Environmental: Temperature
            // temp_report_descriptor[]
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_TYPE_ENVIRONMENTAL_TEMPERATURE,
            HID_COLLECTION(Physical),

                //feature reports (xmit/receive)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_PROPERTY_SENSOR_CONNECTION_TYPE,  // NAry
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(2),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_INTEGRATED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_ATTACHED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_WAKE_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_UNDEFINED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D1_LOW_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D2_STANDBY_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D3_SLEEP_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D4_POWER_OFF_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORT_INTERVAL,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_32(0xFF,0xFF,0xFF,0xFF),
                HID_REPORT_SIZE(32),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0),
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_TEMPERATURE,HID_USAGE_SENSOR_DATA_MOD_CHANGE_SENSITIVITY_ABS),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit Celsius to provide 2 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_TEMPERATURE,HID_USAGE_SENSOR_DATA_MOD_MAX),
                HID_LOGICAL_MIN_16(0x01,0x80), //    LOGICAL_MINIMUM (-32767)
                HID_LOGICAL_MAX_16(0xFF,0x7F), //    LOGICAL_MAXIMUM (32767)
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit Celsius to provide 2 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_TEMPERATURE,HID_USAGE_SENSOR_DATA_MOD_MIN),
                HID_LOGICAL_MIN_16(0x01,0x80), //    LOGICAL_MINIMUM (-32767)
                HID_LOGICAL_MAX_16(0xFF,0x7F), //    LOGICAL_MAXIMUM (32767)
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit Celsius to provide 2 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),

                //input reports (transmit)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_EVENT,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(16),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_EVENT_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_PROPERTY_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL,
                    HID_USAGE_SENSOR_EVENT_POLL_RESPONSE_SEL,
                    HID_USAGE_SENSOR_EVENT_CHANGE_SENSITIVITY_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_TEMPERATURE,
                HID_LOGICAL_MIN_16(0x01,0x80), //    LOGICAL_MINIMUM (-32767)
                HID_LOGICAL_MAX_16(0xFF,0x7F), //    LOGICAL_MAXIMUM (32767)
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit Celsius to provide 2 digits past the decimal point
                HID_INPUT(Data_Var_Abs),

            HID_END_COLLECTION,

            HID_REPORT_ID(USB_HID_REPORT_ID_RELATIVE_HUMIDITY_SENSOR),
            // hid_sensor_spec_report_descriptors.h
            // 4.3.10    Environmental: Humidity
            // hyg_report_descriptor[]
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_TYPE_ENVIRONMENTAL_HUMIDITY,
            HID_COLLECTION(Physical),

                //feature reports (xmit/receive)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_PROPERTY_SENSOR_CONNECTION_TYPE,  // NAry
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(2),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_INTEGRATED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_ATTACHED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_WAKE_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_UNDEFINED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D1_LOW_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D2_STANDBY_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D3_SLEEP_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D4_POWER_OFF_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORT_INTERVAL,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_32(0xFF,0xFF,0xFF,0xFF),
                HID_REPORT_SIZE(32),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0),
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_RELATIVE_HUMIDITY,HID_USAGE_SENSOR_DATA_MOD_CHANGE_SENSITIVITY_ABS),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0x10,0x27), // 10000 = 0.00 to 100.00 percent with 2 digits past decimal point
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_RELATIVE_HUMIDITY,HID_USAGE_SENSOR_DATA_MOD_MAX),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0x10,0x27), // 10000 = 0.00 to 100.00 percent with 2 digits past decimal point
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_RELATIVE_HUMIDITY,HID_USAGE_SENSOR_DATA_MOD_MIN),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0x10,0x27), // 10000 = 0.00 to 100.00 percent with 2 digits past decimal point
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past the decimal point
                HID_FEATURE(Data_Var_Abs),

                //input reports (transmit)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_EVENT,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_EVENT_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_PROPERTY_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL,
                    HID_USAGE_SENSOR_EVENT_POLL_RESPONSE_SEL,
                    HID_USAGE_SENSOR_EVENT_CHANGE_SENSITIVITY_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_DATA_ENVIRONMENTAL_RELATIVE_HUMIDITY,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0x10,0x27), // 10000 = 0.00 to 100.00 percent with 2 digits past decimal point
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0x0E), // scale default unit percent to provide 2 digits past the decimal point
                HID_INPUT(Data_Var_Abs),

            HID_END_COLLECTION,

            HID_REPORT_ID(USB_HID_REPORT_ID_PM2P5_SENSOR),
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_TYPE_OTHER_CUSTOM,
            HID_COLLECTION(Physical),

                //feature reports (xmit/receive)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_PROPERTY_SENSOR_CONNECTION_TYPE,  // NAry
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(2),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_INTEGRATED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_ATTACHED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_WAKE_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_UNDEFINED_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D1_LOW_POWER_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D2_STANDBY_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D3_SLEEP_WITH_WAKE_SEL,
                    HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D4_POWER_OFF_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_FEATURE(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_PROPERTY_REPORT_INTERVAL,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_32(0xFF,0xFF,0xFF,0xFF),
                HID_REPORT_SIZE(32),
                HID_REPORT_COUNT(1),
                HID_UNIT_EXPONENT(0),
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_CUSTOM_VALUE,HID_USAGE_SENSOR_DATA_MOD_CHANGE_SENSITIVITY_ABS),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_UG_PER_M3,
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_CUSTOM_VALUE,HID_USAGE_SENSOR_DATA_MOD_MAX),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_UG_PER_M3,
                HID_FEATURE(Data_Var_Abs),
                HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_CUSTOM_VALUE,HID_USAGE_SENSOR_DATA_MOD_MIN),
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_UG_PER_M3,
                HID_FEATURE(Data_Var_Abs),

                //input reports (transmit)
                HID_USAGE_PAGE_SENSOR,
                HID_USAGE_SENSOR_STATE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(6),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_STATE_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_STATE_READY_SEL,
                    HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL,
                    HID_USAGE_SENSOR_STATE_NO_DATA_SEL,
                    HID_USAGE_SENSOR_STATE_INITIALIZING_SEL,
                    HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL,
                    HID_USAGE_SENSOR_STATE_ERROR_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_EVENT,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_8(5),
                HID_REPORT_SIZE(8),
                HID_REPORT_COUNT(1),
                HID_COLLECTION(Logical),
                    HID_USAGE_SENSOR_EVENT_UNKNOWN_SEL,
                    HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_PROPERTY_CHANGED_SEL,
                    HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL,
                    HID_USAGE_SENSOR_EVENT_POLL_RESPONSE_SEL,
                    HID_USAGE_SENSOR_EVENT_CHANGE_SENSITIVITY_SEL,
                    HID_INPUT(Data_Arr_Abs),
                    HID_END_COLLECTION,
                HID_USAGE_SENSOR_DATA_CUSTOM_VALUE,
                HID_LOGICAL_MIN_8(0),
                HID_LOGICAL_MAX_16(0xFF,0xFF),
                HID_REPORT_SIZE(16),
                HID_REPORT_COUNT(1),
                HID_UNIT_UG_PER_M3,
                HID_INPUT(Data_Var_Abs),

            HID_END_COLLECTION,

        HID_END_COLLECTION,
    };
#endif

typedef struct
{
    uint8_t     m_bLength;
    uint8_t     m_bDescriptorType;
    uint16_t    m_wTotalLength;
    uint8_t     m_bNumInterfaces;
    uint8_t     m_bConfigurationValue;
    uint8_t     m_iConfiguration;
    uint8_t     m_bmAttributes;
    uint8_t     m_MaxPower;
} ATTR_PACKED UsbConfigurationDescriptor;
static_assert(sizeof(UsbConfigurationDescriptor) == 9, "Unexpected configuration descriptor size!");

typedef struct
{
    uint8_t     m_bLength;
    uint8_t     m_bDescriptorType;
    uint8_t     m_bInterfaceNumber;
    uint8_t     m_bAlternateSetting;
    uint8_t     m_bNumEndpoints;
    uint8_t     m_bInterfaceClass;
    uint8_t     m_bInterfaceSubClass;
    uint8_t     m_bInterfaceProtocol;
    uint8_t     m_iInterface;
} ATTR_PACKED UsbInterfaceDescriptor;
static_assert(sizeof(UsbInterfaceDescriptor) == 9, "Unexpected interface descriptor size!");

typedef struct
{
    uint8_t     m_bLength;
    uint8_t     m_bDescriptorType;
    uint16_t    m_bcdHID;
    uint8_t     m_bCountryCode;
    uint8_t     m_bNumDescriptors;
    uint8_t     m_bReportDescriptorType;
    uint16_t    m_wDescriptorLength;
} ATTR_PACKED UsbHidDescriptor;
static_assert(sizeof(UsbHidDescriptor) == 9, "Unexpected HID descriptor size!");

typedef struct
{
    uint8_t     m_bLength;
    uint8_t     m_bDescriptorType;
    uint8_t     m_bEndpointAddress;
    uint8_t     m_bmAttributes;
    uint16_t    m_wMaxPacketSize;
    uint8_t     m_bInterval;
} ATTR_PACKED UsbEndpointDescriptor;
static_assert(sizeof(UsbEndpointDescriptor) == 7, "Unexpected endpoint descriptor size!");

typedef struct
{
    UsbConfigurationDescriptor  m_ConfigurationDescriptor;
    UsbInterfaceDescriptor      m_InterfaceDescriptor;
#if USB_CFG_INTERFACE_CLASS == 3
    UsbHidDescriptor            m_HidDescriptor;
#endif
#if USB_CFG_HAVE_INTRIN_ENDPOINT != 0
    UsbEndpointDescriptor       m_EndpointDescriptor0;
#if USB_CFG_HAVE_INTRIN_ENDPOINT3 != 0
    UsbEndpointDescriptor       m_EndpointDescriptor1;
#endif
#endif
} ATTR_PACKED UsbCfgIfEpDescriptors;

static const UsbCfgIfEpDescriptors f_CfgIfEpDescriptors PROGMEM =
{
    .m_ConfigurationDescriptor =
    {
        .m_bLength                  = sizeof(UsbConfigurationDescriptor),
        .m_bDescriptorType          = USBDESCR_CONFIG,
        .m_wTotalLength             = sizeof(UsbCfgIfEpDescriptors),
        .m_bNumInterfaces           = 1,
        .m_bConfigurationValue      = 1,
        .m_iConfiguration           = 0,
    #if USB_CFG_IS_SELF_POWERED
        .m_bmAttributes             = BIT_VAL(7) | USBATTR_SELFPOWER,
    #else
        .m_bmAttributes             = BIT_VAL(7),
    #endif
        .m_MaxPower                 = USB_CFG_MAX_BUS_POWER / 2,
    },
    .m_InterfaceDescriptor =
    {
        .m_bLength                  = sizeof(UsbInterfaceDescriptor),
        .m_bDescriptorType          = USBDESCR_INTERFACE,
        .m_bInterfaceNumber         = 0,
        .m_bAlternateSetting        = 0,
        .m_bNumEndpoints            = (USB_CFG_HAVE_INTRIN_ENDPOINT != 0) + (USB_CFG_HAVE_INTRIN_ENDPOINT3 != 0),
        .m_bInterfaceClass          = USB_CFG_INTERFACE_CLASS,
        .m_bInterfaceSubClass       = USB_CFG_INTERFACE_SUBCLASS,
        .m_bInterfaceProtocol       = USB_CFG_INTERFACE_PROTOCOL,
        .m_iInterface               = 0,
    },
#if USB_CFG_INTERFACE_CLASS == 3
    .m_HidDescriptor =
    {
        .m_bLength                  = sizeof(UsbHidDescriptor),
        .m_bDescriptorType          = USBDESCR_HID,
        .m_bcdHID                   = 0x0101,
        .m_bCountryCode             = 0,
        .m_bNumDescriptors          = 1,
        .m_bReportDescriptorType    = USBDESCR_HID_REPORT,
        .m_wDescriptorLength        = sizeof(f_HidReportDescriptor),
    },
#endif
#if USB_CFG_HAVE_INTRIN_ENDPOINT != 0
    .m_EndpointDescriptor0 =
    {
        .m_bLength                  = sizeof(UsbEndpointDescriptor),
        .m_bDescriptorType          = USBDESCR_ENDPOINT,
        .m_bEndpointAddress         = USBRQ_DIR_DEVICE_TO_HOST | 1,
        .m_bmAttributes             = 0x3,
        .m_wMaxPacketSize           = 8,
        .m_bInterval                = USB_CFG_INTR_POLL_INTERVAL,
    },
#if USB_CFG_HAVE_INTRIN_ENDPOINT3 != 0
    .m_EndpointDescriptor1 =
    {
        .m_bLength                  = sizeof(UsbEndpointDescriptor),
        .m_bDescriptorType          = USBDESCR_ENDPOINT,
        .m_bEndpointAddress         = USBRQ_DIR_DEVICE_TO_HOST | USB_CFG_EP3_NUMBER,
        .m_bmAttributes             = 0x3,
        .m_wMaxPacketSize           = 8,
        .m_bInterval                = USB_CFG_INTR_POLL_INTERVAL,
    },
#endif
#endif
};

usbMsgLen_t usbFunctionDescriptor(
        struct usbRequest *pRequest)
{
    const uint8_t descriptorType = pRequest->wValue.bytes[1];

    switch (descriptorType)
    {
    case USBDESCR_CONFIG:
        usbMsgPtr = (usbMsgPtr_t)&f_CfgIfEpDescriptors;
        return sizeof(f_CfgIfEpDescriptors);
#if USB_CFG_INTERFACE_CLASS == 3
    case USBDESCR_HID:
        usbMsgPtr = (usbMsgPtr_t)&f_CfgIfEpDescriptors.m_HidDescriptor;
        return sizeof(f_CfgIfEpDescriptors.m_HidDescriptor);
    case USBDESCR_HID_REPORT:
        usbMsgPtr = (usbMsgPtr_t)f_HidReportDescriptor;
        return sizeof(f_HidReportDescriptor);
#endif
    default:
        return 0;
    }
}
