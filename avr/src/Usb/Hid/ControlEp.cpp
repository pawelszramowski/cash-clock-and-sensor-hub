#include "Usb/Hid/ControlEp.hpp"
#include "Usb/vusb.h"

Usb::Hid::Device* Usb::Hid::ControlEp::m_pDevice { nullptr };
uint8_t* Usb::Hid::ControlEp::m_pInBuffer { nullptr };
usbMsgLen_t Usb::Hid::ControlEp::m_InBufferSize { 0U };

Usb::Hid::Report* Usb::Hid::ControlEp::m_pReport { nullptr };
bool Usb::Hid::ControlEp::m_IsStalled { false };

uint8_t* Usb::Hid::ControlEp::m_pCurrentByte { nullptr };
usbMsgLen_t Usb::Hid::ControlEp::m_BytesLeft { 0U };

usbMsgLen_t usbFunctionSetup(
        uchar data[8])
{
    return Usb::Hid::ControlEp::HandleSetupTransaction(
            reinterpret_cast<const usbRequest_t*>(data));
}

uchar usbFunctionWrite(
        uchar* data,
        uchar len)
{
    return Usb::Hid::ControlEp::HandleOutTransaction(data, len);
}

uchar usbFunctionRead(
        uchar* data,
        uchar len)
{
    return Usb::Hid::ControlEp::HandleInTransaction(data, len);
}
