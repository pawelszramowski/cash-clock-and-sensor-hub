#include <stddef.h>
#include <stdint.h>

#include "common.h"

/// Delete operator stub provided only to allow linking application containing classes
/// with virtual destructors which implicitly define deleting destructors as well.
///
/// @see https://eli.thegreenplace.net/2015/c-deleting-destructors-and-virtual-operator-delete/
void operator delete(
        void*)
{
    ASSERT_ALWAYS(false);
}

/// Sized version of delete operator stub.
///
/// @see http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3536.html
void operator delete(
        void*,
        size_t)
{
    ASSERT_ALWAYS(false);
}

/// Incomplete initialization guard type. Actually specified to have 64 bits
/// but it's irrelevant to following implementations.
///
/// @see http://itanium-cxx-abi.github.io/cxx-abi/abi.html#guards
struct __cxa_guard_type;

/// Check if guarded object should be initialized.
///
/// @see http://itanium-cxx-abi.github.io/cxx-abi/abi.html#once-ctor
extern "C" int __cxa_guard_acquire(
        __cxa_guard_type* pGuardObject)
{
    const auto pGuardFirstByte = reinterpret_cast<uint8_t*>(pGuardObject);
    return (*pGuardFirstByte == 0U) ? 1 : 0;
}

/// Mark guarded object as initialized.
///
/// @see http://itanium-cxx-abi.github.io/cxx-abi/abi.html#once-ctor
extern "C" void __cxa_guard_release(
        __cxa_guard_type* pGuardObject)
{
    const auto pGuardFirstByte = reinterpret_cast<uint8_t*>(pGuardObject);
    *pGuardFirstByte = UINT8_MAX;
}
