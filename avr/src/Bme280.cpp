#include "Bme280.hpp"

PeriodicTimer Bme280::m_PollTimer {};

bme280_calib_data Bme280::m_CalibrationData {};

Bme280::AtmosphericPressure Bme280::m_AtmosphericPressure {};
Bme280::Temperature Bme280::m_Temperature {};
Bme280::RelativeHumidity Bme280::m_RelativeHumidity {};
