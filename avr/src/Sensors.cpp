#include "Sensors.hpp"

#include <avr/eeprom.h>
#include <avr/wdt.h>

#include "Display.hpp"
#include "PushButton.hpp"

void Sensors::SetHpma115s0StateAtPowerOn()
{
    bool hpma115s0EnabledAtPowerOn = (eeprom_read_byte(&m_Hpma115s0StateAtPowerOnE)
            == HPMA115S0_STATE_AT_POWER_ON_ENABLED_MARKER);

    while (PushButton::IsUnstable())
    {
        wdt_reset();
    }

    if (PushButton::WasPressed())
    {
        hpma115s0EnabledAtPowerOn = !hpma115s0EnabledAtPowerOn;

        eeprom_write_byte(
                &m_Hpma115s0StateAtPowerOnE,
                hpma115s0EnabledAtPowerOn ?
                        HPMA115S0_STATE_AT_POWER_ON_ENABLED_MARKER :
                        HPMA115S0_STATE_AT_POWER_ON_DISABLED_MARKER);

        Display::ForcePm2p5SensorPowerOnInfo(hpma115s0EnabledAtPowerOn);
    }

    Hpma115s0::Enable(hpma115s0EnabledAtPowerOn);
}

uint8_t Sensors::m_Hpma115s0StateAtPowerOnE { HPMA115S0_STATE_AT_POWER_ON_DISABLED_MARKER };
