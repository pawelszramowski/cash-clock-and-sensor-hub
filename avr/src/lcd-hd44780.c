// @formatter:off

/** \ingroup lcd_hd44780
 * \{ */

////////////////////////////////////////////////////////////////////////////////
/* Dołączenie bibliotek */
////////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "common.h"
#include "lcd-hd44780.h"

////////////////////////////////////////////////////////////////////////////////
/* Definicje stałych programowych */
////////////////////////////////////////////////////////////////////////////////

/** \brief Czas potrzebny na wykonanie operacji [us] (z wyjątkiem Clear Display
 * i Return Home). */
#define LCD_OPERATION_TIME_US       50

/** \brief Czas potrzebny na wykonanie operacji Clear Display i Return
 * Home [ms]. */
#define LCD_LONG_OPERATION_TIME_MS  1.6

/** \brief Długość dodatniego impulsu na linii Enable [us]. */
#define LCD_ENABLE_PULSE_WIDTH_US   0.25

/** \brief Czas pomiędzy impulsami na linii Enable [us]. */
#define LCD_ENABLE_CYCLE_TIME_US    0.5

/** \brief Opóźnienie od zbocza dodatniego na linii Enable do ustawienia linii
 * danych [us]. */
#define LCD_DATA_DELAY_TIME_US      0.5

/** \brief Komenda Function Set. */
#define LCD_CMD_FUNCTION_SET        BIT_VAL(5)

/** \brief Modyfikator komendy Function Set do wyboru interfejsu 8-bitowego. */
#define LCD_MOD_INTERFACE_8B        BIT_VAL(4)

/** \brief Modyfikator komendy Function Set do wyboru interfejsu 4-bitowego. */
#define LCD_MOD_INTERFACE_4B        0x00

/** \brief Modyfikator komendy Function Set do wyboru 2 linii. */
#define LCD_MOD_2_LINES             BIT_VAL(3)

/** \brief Modyfikator komendy Function Set do wyboru 1 linii. */
#define LCD_MOD_1_LINE              0x00

/** \brief Modyfikator komendy Function Set do wyboru znaków 5x10. */
#define LCD_MOD_CHARS_5x10          BIT_VAL(2)

/** \brief Modyfikator komendy Function Set do wyboru znaków 5x8. */
#define LCD_MOD_CHARS_5x8           0x00

////////////////////////////////////////////////////////////////////////////////
/* Deklaracje funkcji statycznych */
////////////////////////////////////////////////////////////////////////////////

/** \brief Wysłanie dodatniego impulsu na linię Enable. */
static void PulseEnable(
    void);

/** \brief Zapisanie jednego bajtu do wyświetlacza. */
static void WriteByte(
    const uint8_t data);

#if LCD_USE_RW != 0
    /** \brief Odczytanie jednego bajtu z wyświetlacza. */
    static uint8_t ReadByte(
        void);
#endif

////////////////////////////////////////////////////////////////////////////////
/* Definicje funkcji */
////////////////////////////////////////////////////////////////////////////////

/** */
void LCD_Init(
    void)
{
    /* ustawienie kierunku pracy linii Enable, Registers Select i Read/Write
     * na wyjście */
    DDR(LCD_E_PORT)  |= BIT_VAL(LCD_E);
    DDR(LCD_RS_PORT) |= BIT_VAL(LCD_RS);
#if LCD_USE_RW != 0
    DDR(LCD_RW_PORT) |= BIT_VAL(LCD_RW);
#endif

    /* ustawienie linii kontrolnych w stan niski */
    PORT(LCD_E_PORT)  &= ~BIT_VAL(LCD_E);
    PORT(LCD_RS_PORT) &= ~BIT_VAL(LCD_RS);
#if LCD_USE_RW != 0
    PORT(LCD_RW_PORT) &= ~BIT_VAL(LCD_RW);
#endif

#if LCD_USE_4B_INTERFACE != 0
    /* ustawienie kierunku pracy linii danych na wyjście */
    DDR(LCD_DB7_PORT) |= BIT_VAL(LCD_DB7);
    DDR(LCD_DB6_PORT) |= BIT_VAL(LCD_DB6);
    DDR(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    DDR(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);

    /* ustawienie stanu linii danych na 0011
     * (Function Set - Interface 8-bit) */
    PORT(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
    PORT(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
    PORT(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    PORT(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);

    /* trzy dodatnie impulsy na linii Enable z opóźnieniami */
    PulseEnable();
    _delay_ms(4.2);
    PulseEnable();
    _delay_us(200);
    PulseEnable();
    _delay_us(LCD_OPERATION_TIME_US);

    /* ustawienie stanu linii danych na 0010
     * (Function Set - Interface 4-bit) */
    PORT(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);

    /* dodatni impuls na linii Enable z opóźnieniem */
    PulseEnable();
    _delay_us(LCD_OPERATION_TIME_US);

    /* teraz można normalnie wysyłać instrukcje i sprawdzać flagę
     * zajętości */

    /* konfiguracja wyświetlacza */
#if LCD_USE_2_LINES != 0
    LCD_WriteInstruction(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_4B
        | LCD_MOD_2_LINES | LCD_MOD_CHARS_5x8);
#else
    LCD_WriteInstruction(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_4B
        | LCD_MOD_1_LINE | LCD_MOD_CHARS_5x8);
#endif
#else
    /* trzykrotne wysłanie instrukcji Function Set - Interface 8-bit z
     * opóźnieniami */
    WriteByte(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_8B);
    _delay_ms(4.2);
    WriteByte(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_8B);
    _delay_us(200);
    WriteByte(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_8B);

    /* teraz można normalnie wysyłać instrukcje i sprawdzać flagę
     * zajętości */

    /* konfiguracja wyświetlacza */
#if LCD_USE_2_LINES != 0
    LCD_WriteInstruction(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_8B
        | LCD_MOD_2_LINES | LCD_MOD_CHARS_5x8);
#else
    LCD_WriteInstruction(LCD_CMD_FUNCTION_SET | LCD_MOD_INTERFACE_8B
        | LCD_MOD_1_LINE | LCD_MOD_CHARS_5x8);
#endif
#endif

    /* wyłączenie wyświetlacza oraz kursora, wyczyszczenie pamięci i ustawienie
     * inkrementacji licznika adresowego */
    LCD_WriteInstruction(LCD_CMD_DISPLAY_CONTROL | LCD_MOD_DISPLAY_OFF
        | LCD_MOD_CURSOR_OFF | LCD_MOD_CURSOR_BLINK_OFF);
    LCD_ClearDisplay();
    LCD_WriteInstruction(LCD_CMD_ENTRY_MODE_SET | LCD_MOD_ADDRESS_INC
        | LCD_MOD_SHIFT_DISABLE);
}

/** */
void LCD_WriteString(
    const char *pString)
{
    if (NULL == pString)
        return;

    /* deklaracje zmiennych */
    uint8_t singleChar;

    while ((singleChar = (uint8_t)*pString++) != '\0')
        LCD_WriteData(singleChar);
}

/** */
void LCD_WriteStringP(
    const char *pStringP)
{
    if (NULL == pStringP)
        return;

    /* deklaracje zmiennych */
    uint8_t singleChar;

    while ((singleChar = pgm_read_byte(pStringP++)) != '\0')
        LCD_WriteData(singleChar);
}

/** */
void LCD_WriteCgramP(
    const uint8_t *pDataP,
    const uint8_t charCount,
    const char address)
{
    if (NULL == pDataP)
        return;

    LCD_WriteInstruction(LCD_CMD_SET_CGRAM_ADDRESS | (address * LCD_BYTES_PER_CHAR));

    for (uint8_t dataCount = charCount * LCD_BYTES_PER_CHAR; dataCount > 0; --dataCount)
        LCD_WriteData(pgm_read_byte(pDataP++));
}

/** */
void LCD_ClearCgram(
    void)
{
    LCD_WriteInstruction(LCD_CMD_SET_CGRAM_ADDRESS | 0x00);

    for (uint8_t i = LCD_BYTES_PER_CHAR * LCD_CHARS_PER_CGRAM; i > 0; --i)
        LCD_WriteData(0x00);
}

/** */
void LCD_WriteData(
    const uint8_t data)
{
#if LCD_USE_RW != 0
    /* ewentualne odczekanie, aż wyświetlacz zakończy wewnętrzną operację */
    while (LCD_ReadInstruction() & BIT_VAL(LCD_BUSY_FLAG))
        {/* do nothing */}
#endif

    /* ustawienie stanu wysokiego na linii Registers Select */
    PORT(LCD_RS_PORT) |= BIT_VAL(LCD_RS);

    /* wysłanie danej do wyświetlacza */
    WriteByte(data);

#if LCD_USE_RW == 0
    /* odczekanie, aż wyświetlacz zakończy wewnętrzną operację (w przypadku
     * instrukcji Clear Display i Return Home trzeba odczekać jeszcze
     * dodatkowo co najmniej 1.5ms) */
    _delay_us(LCD_OPERATION_TIME_US);
#endif
}

/** */
void LCD_WriteInstruction(
    const uint8_t instruction)
{
#if LCD_USE_RW != 0
    /* ewentualne odczekanie, aż wyświetlacz zakończy wewnętrzną operację */
    while (LCD_ReadInstruction() & BIT_VAL(LCD_BUSY_FLAG))
        {/* do nothing */}
#endif

    /* ustawienie stanu niskiego na linii Registers Select */
    PORT(LCD_RS_PORT) &= ~BIT_VAL(LCD_RS);

    /* wysłanie instrukcji do wyświetlacza */
    WriteByte(instruction);

#if LCD_USE_RW == 0
    /* odczekanie, aż wyświetlacz zakończy wewnętrzną operację */
    if (instruction == LCD_CMD_CLEAR_DISPLAY || instruction == LCD_CMD_RETURN_HOME)
        _delay_ms(LCD_LONG_OPERATION_TIME_MS);
    else
        _delay_us(LCD_OPERATION_TIME_US);
#endif
}

#if LCD_USE_RW != 0 && (LCD_READ_ONLY_BF == 0)
    /** */
    uint8_t LCD_ReadData(
        void)
    {
        /* ewentualne odczekanie, aż wyświetlacz zakończy wewnętrzną operację */
        while (LCD_ReadInstruction() & BIT_VAL(LCD_BUSY_FLAG))
            {/* do nothing */}

        /* ustawienie stanu wysokiego na linii Registers Select */
        PORT(LCD_RS_PORT) |= BIT_VAL(LCD_RS);

        /* odczytanie danej z wyświetlacza */
        return (ReadByte());
    }
#endif

#if LCD_USE_RW != 0
    /** */
    inline uint8_t LCD_ReadInstruction(
        void)
    {
        /* ustawienie stanu niskiego na linii Registers Select */
        PORT(LCD_RS_PORT) &= ~BIT_VAL(LCD_RS);

        /* odczytanie instrukcji z wyświetlacza */
        return (ReadByte());
    }
#endif

/** */
static inline void PulseEnable(
    void)
{
    PORT(LCD_E_PORT) |= BIT_VAL(LCD_E);
    _delay_us(LCD_ENABLE_PULSE_WIDTH_US);
    PORT(LCD_E_PORT) &= ~BIT_VAL(LCD_E);
}

/** */
static void WriteByte(
    const uint8_t data)
{
#if LCD_USE_RW != 0
    /* ustawienie stanu niskiego na linii Read/Write */
    PORT(LCD_RW_PORT) &= ~BIT_VAL(LCD_RW);
#endif

#if LCD_USE_4B_INTERFACE != 0
    /* ustawienie kierunku pracy linii danych na wyjście */
    DDR(LCD_DB7_PORT) |= BIT_VAL(LCD_DB7);
    DDR(LCD_DB6_PORT) |= BIT_VAL(LCD_DB6);
    DDR(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    DDR(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);

    /* ustawienie stanu linii danych zgodnie ze starszą tetradą danej */
    if (data & BIT_VAL(7))
        PORT(LCD_DB7_PORT) |= BIT_VAL(LCD_DB7);
    else
        PORT(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
    if (data & BIT_VAL(6))
        PORT(LCD_DB6_PORT) |= BIT_VAL(LCD_DB6);
    else
        PORT(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
    if (data & BIT_VAL(5))
        PORT(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    else
        PORT(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
    if (data & BIT_VAL(4))
        PORT(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);
    else
        PORT(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);

    /* dodatni impuls na linii Enable */
    PulseEnable();

    /* ustawienie stanu linii danych zgodnie z młodszą tetradą danej */
    if (data & BIT_VAL(3))
        PORT(LCD_DB7_PORT) |= BIT_VAL(LCD_DB7);
    else
        PORT(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
    if (data & BIT_VAL(2))
        PORT(LCD_DB6_PORT) |= BIT_VAL(LCD_DB6);
    else
        PORT(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
    if (data & BIT_VAL(1))
        PORT(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    else
        PORT(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
    if (data & BIT_VAL(0))
        PORT(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);
    else
        PORT(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);

    /* dodatni impuls na linii Enable */
    PulseEnable();
#else
    /* ustawienie kierunku pracy linii danych na wyjście */
    DDR(LCD_DB7_PORT) |= BIT_VAL(LCD_DB7);
    DDR(LCD_DB6_PORT) |= BIT_VAL(LCD_DB6);
    DDR(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    DDR(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);
    DDR(LCD_DB3_PORT) |= BIT_VAL(LCD_DB3);
    DDR(LCD_DB2_PORT) |= BIT_VAL(LCD_DB2);
    DDR(LCD_DB1_PORT) |= BIT_VAL(LCD_DB1);
    DDR(LCD_DB0_PORT) |= BIT_VAL(LCD_DB0);

    /* ustawienie stanu linii danych */
    if (data & BIT_VAL(7))
        PORT(LCD_DB7_PORT) |= BIT_VAL(LCD_DB7);
    else
        PORT(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
    if (data & BIT_VAL(6))
        PORT(LCD_DB6_PORT) |= BIT_VAL(LCD_DB6);
    else
        PORT(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
    if (data & BIT_VAL(5))
        PORT(LCD_DB5_PORT) |= BIT_VAL(LCD_DB5);
    else
        PORT(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
    if (data & BIT_VAL(4))
        PORT(LCD_DB4_PORT) |= BIT_VAL(LCD_DB4);
    else
        PORT(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);
    if (data & BIT_VAL(3))
        PORT(LCD_DB3_PORT) |= BIT_VAL(LCD_DB3);
    else
        PORT(LCD_DB3_PORT) &= ~BIT_VAL(LCD_DB3);
    if (data & BIT_VAL(2))
        PORT(LCD_DB2_PORT) |= BIT_VAL(LCD_DB2);
    else
        PORT(LCD_DB2_PORT) &= ~BIT_VAL(LCD_DB2);
    if (data & BIT_VAL(1))
        PORT(LCD_DB1_PORT) |= BIT_VAL(LCD_DB1);
    else
        PORT(LCD_DB1_PORT) &= ~BIT_VAL(LCD_DB1);
    if (data & BIT_VAL(0))
        PORT(LCD_DB0_PORT) |= BIT_VAL(LCD_DB0);
    else
        PORT(LCD_DB0_PORT) &= ~BIT_VAL(LCD_DB0);

    /* dodatni impuls na linii Enable */
    PulseEnable();
#endif
}

#if LCD_USE_RW != 0
    /** */
    static uint8_t ReadByte(
        void)
    {
        /* definicje zmiennych */
        uint8_t data = 0;

        /* ustawienie stanu wysokiego na linii Read/Write */
        PORT(LCD_RW_PORT) |= BIT_VAL(LCD_RW);

    #if LCD_USE_4B_INTERFACE != 0
        /* ustawienie kierunku pracy linii danych na wejście */
        DDR(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
        DDR(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
        DDR(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
        DDR(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);

        /* wyłączenie rezystorów podciagających na liniach danych */
        PORT(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
        PORT(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
        PORT(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
        PORT(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);

        /* ustawienie stanu wysokiego na linii Enable */
        PORT(LCD_E_PORT) |= BIT_VAL(LCD_E);
        _delay_us(LCD_DATA_DELAY_TIME_US);

        /* odczytanie starszej tetrady */
        if (PIN(LCD_DB7_PORT) & BIT_VAL(LCD_DB7))
            data |= BIT_VAL(7);
    #if LCD_READ_ONLY_BF == 0
        if (PIN(LCD_DB6_PORT) & BIT_VAL(LCD_DB6))
            data |= BIT_VAL(6);
        if (PIN(LCD_DB5_PORT) & BIT_VAL(LCD_DB5))
            data |= BIT_VAL(5);
        if (PIN(LCD_DB4_PORT) & BIT_VAL(LCD_DB4))
            data |= BIT_VAL(4);
    #endif

        /* ustawienie stanu niskiego na linii Enable */
        PORT(LCD_E_PORT) &= ~BIT_VAL(LCD_E);

        /* opóźnienie przed odczytaniem młodszej tetrady */
        _delay_us(LCD_ENABLE_CYCLE_TIME_US);

        /* ustawienie stanu wysokiego na linii Enable */
        PORT(LCD_E_PORT) |= BIT_VAL(LCD_E);
        _delay_us(LCD_DATA_DELAY_TIME_US);

    #if LCD_READ_ONLY_BF == 0
        /* odczytanie młodszej tetrady */
        if (PIN(LCD_DB7_PORT) & BIT_VAL(LCD_DB7))
            data |= BIT_VAL(3);
        if (PIN(LCD_DB6_PORT) & BIT_VAL(LCD_DB6))
            data |= BIT_VAL(2);
        if (PIN(LCD_DB5_PORT) & BIT_VAL(LCD_DB5))
            data |= BIT_VAL(1);
        if (PIN(LCD_DB4_PORT) & BIT_VAL(LCD_DB4))
            data |= BIT_VAL(0);
    #endif

        /* ustawienie stanu niskiego na linii Enable */
        PORT(LCD_E_PORT) &= ~BIT_VAL(LCD_E);
    #else
        /* ustawienie kierunku pracy linii danych na wejście */
        DDR(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
        DDR(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
        DDR(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
        DDR(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);
        DDR(LCD_DB3_PORT) &= ~BIT_VAL(LCD_DB3);
        DDR(LCD_DB2_PORT) &= ~BIT_VAL(LCD_DB2);
        DDR(LCD_DB1_PORT) &= ~BIT_VAL(LCD_DB1);
        DDR(LCD_DB0_PORT) &= ~BIT_VAL(LCD_DB0);

        /* wyłączenie rezystorów podciagających na liniach danych */
        PORT(LCD_DB7_PORT) &= ~BIT_VAL(LCD_DB7);
        PORT(LCD_DB6_PORT) &= ~BIT_VAL(LCD_DB6);
        PORT(LCD_DB5_PORT) &= ~BIT_VAL(LCD_DB5);
        PORT(LCD_DB4_PORT) &= ~BIT_VAL(LCD_DB4);
        PORT(LCD_DB3_PORT) &= ~BIT_VAL(LCD_DB3);
        PORT(LCD_DB2_PORT) &= ~BIT_VAL(LCD_DB2);
        PORT(LCD_DB1_PORT) &= ~BIT_VAL(LCD_DB1);
        PORT(LCD_DB0_PORT) &= ~BIT_VAL(LCD_DB0);

        /* ustawienie stanu wysokiego na linii Enable */
        PORT(LCD_E_PORT) |= BIT_VAL(LCD_E);
        _delay_us(LCD_DATA_DELAY_TIME_US);

        /* odczytanie danej */
        if (PIN(LCD_DB7_PORT) & BIT_VAL(LCD_DB7))
            data |= BIT_VAL(7);
    #if LCD_READ_ONLY_BF == 0
        if (PIN(LCD_DB6_PORT) & BIT_VAL(LCD_DB6))
            data |= BIT_VAL(6);
        if (PIN(LCD_DB5_PORT) & BIT_VAL(LCD_DB5))
            data |= BIT_VAL(5);
        if (PIN(LCD_DB4_PORT) & BIT_VAL(LCD_DB4))
            data |= BIT_VAL(4);
        if (PIN(LCD_DB3_PORT) & BIT_VAL(LCD_DB3))
            data |= BIT_VAL(3);
        if (PIN(LCD_DB2_PORT) & BIT_VAL(LCD_DB2))
            data |= BIT_VAL(2);
        if (PIN(LCD_DB1_PORT) & BIT_VAL(LCD_DB1))
            data |= BIT_VAL(1);
        if (PIN(LCD_DB0_PORT) & BIT_VAL(LCD_DB0))
            data |= BIT_VAL(0);
    #endif

        /* ustawienie stanu niskiego na linii Enable */
        PORT(LCD_E_PORT) &= ~BIT_VAL(LCD_E);
    #endif

        return (data);
    }
#endif

/** \} */
