#include "PushButton.hpp"

OneshotTimer PushButton::m_DebouncingTimer {};
bool PushButton::m_PreviousState { false };

bool PushButton::m_IsPressed { false };
bool PushButton::m_WasPressed { false };
