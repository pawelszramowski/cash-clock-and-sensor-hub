#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <stdint.h>

#include "Backlight.hpp"
#include "common.h"
#include "Display.hpp"
#include "Mcu.hpp"
#include "Usb/vusb.h"

ATTR_NO_RETURN
void g_HandleAssertFailure(
        const char *pFileNameP,
        const uint16_t lineNum)
{
    cli();

    static bool isAssertBeingHandled = false;

    if (!isAssertBeingHandled)
    {
        isAssertBeingHandled = true;

        wdt_disable();
        usbDeviceDisconnect();
        Backlight::SetDutyCycle(Backlight::DUTY_CYCLE_ALWAYS_ON);
        Display::ForceFailedAssertInfo(pFileNameP, lineNum, PSTR("Build " __DATE__ " " __TIME__),
                                       Display::SCROLL_INDEFINITELY);
    }

    Mcu::Reset();
}
