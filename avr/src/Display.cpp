#include "Display.hpp"

constexpr uint8_t Display::CGRAM_PATTERN_DEGREES_CELSIUS[];
constexpr uint8_t Display::CGRAM_PATTERN_MICRO[];
PeriodicTimer Display::m_UpdateTimer {};
bool Display::m_IsTimeDateBlankingActive { false };
