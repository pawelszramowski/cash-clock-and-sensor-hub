#include "Mcu.hpp"

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdint.h>

#include "common.h"

#include "stackmon.h"

void Mcu::Poll()
{
    ASSERT_ALWAYS(StackCount() >= MIN_FREE_STACK_SIZE_IN_BYTES);
}

/// @warning This interrupt handler must not return as it corrupts the machine state!
ISR(WDT_vect, ISR_NAKED)
{
    /// Make sure that __zero_reg__ actually contains a zero, since it may not always be the case
    /// on ISR entry.
    asm volatile ("eor __zero_reg__, __zero_reg__");

    /// Pop program counter from the stack where it's been pushed on interrupt entry.
    uint16_t programCounter;
    asm volatile (
            "pop %B0" "\n\t"
            "pop %A0"
            : "=r" (programCounter)
    );
    const uint16_t flashAddress = programCounter * sizeof(uint16_t);

    g_HandleAssertFailure(PSTR("InstrAddrAtWatchdogTimeout"), flashAddress);
    Mcu::Reset();
}
