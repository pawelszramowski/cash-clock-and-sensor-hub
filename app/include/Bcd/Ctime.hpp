#pragma once

#include <ctime>
#include <stdexcept>

#include "Bcd/Date.hpp"
#include "Bcd/Time.hpp"
#include "Bcd/Value.hpp"

namespace Bcd {

/// Adapter between C++ Standard Library and Bcd::Time/Bcd::Date.
class Ctime
{
public:
    Ctime() = delete;
    Ctime(
            const Ctime&) = delete;
    Ctime(
            Ctime&&) = delete;
    Ctime& operator =(
            const Ctime&) = delete;
    Ctime& operator =(
            Ctime&&) = delete;
    ~Ctime() = delete;

    static void SetTimeDateToNow(
            Time& rDstTime,
            Date& rDstDate)
    {
        const std::time_t epochTime = std::time(nullptr);
        if (epochTime == static_cast<std::time_t>(-1))
        {
            throw std::runtime_error("Unable to retrieve system time!");
        }

        const std::tm* const pLocalTime = std::localtime(&epochTime);
        if (pLocalTime == nullptr)
        {
            throw std::runtime_error("Unable to convert system time to local time!");
        }

        SetTimeDate(rDstTime, rDstDate, pLocalTime);
    }

    static void SetTimeDate(
            Time& rDstTime,
            Date& rDstDate,
            const std::tm* const pSrcTm)
    {
        if (pSrcTm != nullptr)
        {
            rDstTime.m_Hour = BinToBcd(pSrcTm->tm_hour - 0 + Time::HOUR_MIN);
            rDstTime.m_Minute = BinToBcd(pSrcTm->tm_min - 0 + Time::MINUTE_MIN);
            rDstTime.m_Second = BinToBcd(pSrcTm->tm_sec - 0 + Time::SECOND_MIN);

            if (!rDstTime.IsValid())
            {
                throw std::runtime_error("Invalid time provided!");
            }

            rDstDate.m_Year = BinToBcd(pSrcTm->tm_year - 100 + Date::YEAR_MIN);
            rDstDate.m_Month = BinToBcd(pSrcTm->tm_mon - 0 + Date::MONTH_JANUARY);
            rDstDate.m_Day = BinToBcd(pSrcTm->tm_mday - 1 + Date::DAY_FIRST);
            rDstDate.m_Weekday = BinToBcd(pSrcTm->tm_wday - 0 + Date::WEEKDAY_SUNDAY);

            if (!rDstDate.IsValid())
            {
                throw std::runtime_error("Invalid date provided!");
            }
        }
    }

    static void SetTimeDate(
            std::tm* const pDstTm,
            const Time& rSrcTime,
            const Date& rSrcDate)
    {
        if (pDstTm != nullptr)
        {
            pDstTm->tm_hour = BcdToBin(rSrcTime.m_Hour) + 0 - Time::HOUR_MIN;
            pDstTm->tm_min = BcdToBin(rSrcTime.m_Minute) + 0 - Time::MINUTE_MIN;
            pDstTm->tm_sec = BcdToBin(rSrcTime.m_Second) + 0 - Time::SECOND_MIN;

            pDstTm->tm_year = BcdToBin(rSrcDate.m_Year) + 100 - Date::YEAR_MIN;
            pDstTm->tm_mon = BcdToBin(rSrcDate.m_Month) + 0 - Date::MONTH_JANUARY;
            pDstTm->tm_mday = BcdToBin(rSrcDate.m_Day) + 1 - Date::DAY_FIRST;
            pDstTm->tm_wday = BcdToBin(rSrcDate.m_Weekday) + 0 - Date::WEEKDAY_SUNDAY;
            pDstTm->tm_yday = 0;
            pDstTm->tm_isdst = 0;
        }
    }
};

}
