cmake_minimum_required(VERSION 3.18)
project(StreamHelpers
    LANGUAGES
        CXX
)
add_library(StreamHelpers INTERFACE)
target_include_directories(StreamHelpers
    INTERFACE
        .
)
target_sources(StreamHelpers
    INTERFACE
        # Non-source files are only listed for user convenience.
        StreamHex.hpp
        StreamStateSaver.hpp
)
