#pragma once

#include <iostream>

/// @copyright http://stackoverflow.com/questions/2273330/restore-the-state-of-stdcout-after-manipulating-it
class StreamStateSaver
{
public:
    StreamStateSaver() = delete;
    explicit StreamStateSaver(
            std::ostream& rStream)
            : m_rStream(rStream),
              m_Fill(rStream.fill()),
              m_Flags(rStream.flags()),
              m_Precision(rStream.precision()),
              m_Width(rStream.width())
    {
        // Do nothing.
    }
    StreamStateSaver(
            const StreamStateSaver&) = delete;
    StreamStateSaver(
            StreamStateSaver&&) = delete;
    StreamStateSaver& operator =(
            const StreamStateSaver&) = delete;
    StreamStateSaver& operator =(
            StreamStateSaver&&) = delete;
    ~StreamStateSaver()
    {
        m_rStream.width(m_Width);
        m_rStream.precision(m_Precision);
        m_rStream.flags(m_Flags);
        m_rStream.fill(m_Fill);
    }

private:
    std::ostream& m_rStream;
    const std::ostream::char_type m_Fill;
    const std::ios_base::fmtflags m_Flags;
    const std::streamsize m_Precision;
    const std::streamsize m_Width;
};
