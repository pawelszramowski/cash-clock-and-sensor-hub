#pragma once

#include <climits>
#include <iomanip>
#include <iostream>

#include "StreamStateSaver.hpp"

class StreamHex
{
public:
    friend std::ostream& operator <<(
            std::ostream&,
            const StreamHex&);

    StreamHex() = delete;
    template<typename T>
    explicit StreamHex(
            const T value,
            const bool showBase = true,
            const size_t digitsNum = (sizeof(T) * CHAR_BIT / BITS_PER_HEX_DIGIT))
            : m_Value(static_cast<unsigned long long>(value)),
              m_ShowBase(showBase),
              m_DigitsNum(digitsNum)
    {
        // Do nothing.
    }
    StreamHex(
            const StreamHex&) = default;
    StreamHex(
            StreamHex&&) = default;
    StreamHex& operator =(
            const StreamHex&) = delete;
    StreamHex& operator =(
            StreamHex&&) = delete;
    ~StreamHex() = default;

private:
    static constexpr size_t BITS_PER_HEX_DIGIT = 4U;

    const unsigned long long m_Value;
    const bool m_ShowBase;
    const size_t m_DigitsNum;
};

inline std::ostream& operator <<(
        std::ostream& rStream,
        const StreamHex& rStreamHex)
{
    const StreamStateSaver streamStateSaver(rStream);

    if (rStreamHex.m_ShowBase)
    {
        rStream << "0x";
    }

    rStream << std::internal << std::setfill('0') << std::uppercase << std::hex
            << std::setw(rStreamHex.m_DigitsNum) << +rStreamHex.m_Value;

    return rStream;
}
