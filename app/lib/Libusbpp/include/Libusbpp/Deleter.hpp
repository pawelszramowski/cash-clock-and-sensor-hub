#pragma once

#include <libusb-1.0/libusb.h>

namespace Libusbpp {

class Deleter
{
public:
    void operator()(
            libusb_context* const pContext)
    {
        libusb_exit(pContext);
    }

    void operator()(
            libusb_device** const pDevices)
    {
        static constexpr int UNREF_DEVICES = 1;
        libusb_free_device_list(pDevices, UNREF_DEVICES);
    }

    void operator()(
            libusb_device* const pDevice)
    {
        libusb_unref_device(pDevice);
    }

    void operator()(
            libusb_device_handle* const pDeviceHandle)
    {
        libusb_close(pDeviceHandle);
    }

    void operator()(
            libusb_config_descriptor* const pConfigDescriptor)
    {
        libusb_free_config_descriptor(pConfigDescriptor);
    }

    void operator()(
            libusb_ss_endpoint_companion_descriptor* const pSsEndpointCompanionDescriptor)
    {
        libusb_free_ss_endpoint_companion_descriptor(pSsEndpointCompanionDescriptor);
    }

    void operator()(
            libusb_bos_descriptor* const pBosDescriptor)
    {
        libusb_free_bos_descriptor(pBosDescriptor);
    }

    void operator()(
            libusb_usb_2_0_extension_descriptor* const pUsb2p0ExtensionDescriptor)
    {
        libusb_free_usb_2_0_extension_descriptor(pUsb2p0ExtensionDescriptor);
    }

    void operator()(
            libusb_ss_usb_device_capability_descriptor* const pSsUsbDeviceCapabilityDescriptor)
    {
        libusb_free_ss_usb_device_capability_descriptor(pSsUsbDeviceCapabilityDescriptor);
    }

    void operator()(
            libusb_container_id_descriptor* const pContainerIdDescriptor)
    {
        libusb_free_container_id_descriptor(pContainerIdDescriptor);
    }
};

}
