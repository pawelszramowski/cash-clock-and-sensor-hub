#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "Libusbpp/Deleter.hpp"
#include "Libusbpp/LangIdCodec.hpp"
#include "Libusbpp/SyncIo/Transfer.hpp"

struct libusb_device;

namespace Libusbpp {

class Device;

/// @note Interface objects and RAII are not used instead of claim and release functions since
/// the benefit was deemed not worth the effort, considering that a list of smart pointers to
/// interface objects would have to be maintained by this class.
class DeviceHandle
{
public:
    static constexpr uint16_t DEFAULT_LANGID = 0U;

    DeviceHandle() = delete;
    explicit DeviceHandle(
            const std::shared_ptr<Device>& rpDevice,
            libusb_device* const pRawDevice);
    DeviceHandle(
            const DeviceHandle&) = delete;
    DeviceHandle(
            DeviceHandle&&) = delete;
    DeviceHandle& operator =(
            const DeviceHandle&) = delete;
    DeviceHandle& operator =(
            DeviceHandle&&) = delete;
    ~DeviceHandle();

    std::shared_ptr<Device> GetDevice() const;

    const std::vector<uint16_t>& GetLangIds();

    bool IsLangIdSupported(
            const uint16_t langId);

    std::string GetStringDescriptor(
            const uint8_t descriptorIndex,
            uint16_t langId = DEFAULT_LANGID);

    auto GetStringDescriptor(
            const uint8_t descriptorIndex,
            const std::string& rLangName)
    {
        return GetStringDescriptor(descriptorIndex, LangIdCodec::GetId(rLangName));
    }

    std::string GetManufacturer(
            const uint16_t langId = DEFAULT_LANGID);

    auto GetManufacturer(
            const std::string& rLangName)
    {
        return GetManufacturer(LangIdCodec::GetId(rLangName));
    }

    std::string GetProduct(
            const uint16_t langId = DEFAULT_LANGID);

    auto GetProduct(
            const std::string& rLangName)
    {
        return GetProduct(LangIdCodec::GetId(rLangName));
    }

    std::string GetSerialNumber(
            const uint16_t langId = DEFAULT_LANGID);

    auto GetSerialNumber(
            const std::string& rLangName)
    {
        return GetSerialNumber(LangIdCodec::GetId(rLangName));
    }

    void ClaimInterface(
            const int interfaceNum);

    void ReleaseInterface(
            const int interfaceNum);

    int PerformTransfer(
            SyncIo::Transfer& rTransfer,
            const unsigned int timeoutInMs = SyncIo::Transfer::NO_TIMEOUT) const;

private:
    static constexpr uint8_t LANGID_DESCRIPTOR_INDEX = 0U;

    static libusb_device_handle* OpenDevice(
            libusb_device* const pRawDevice);

    bool IsInterfaceClaimed(
            const int interfaceNum) const;

    // WARNING: Order of declaration is important as resources
    // will be released in the inverse order of declaration!
    const std::shared_ptr<Device> m_pDevice;
    const std::unique_ptr<libusb_device_handle, Deleter> m_pHandle;
    std::vector<int> m_ClaimedInterfaces;
    std::vector<uint16_t> m_LangIds;
};

}
