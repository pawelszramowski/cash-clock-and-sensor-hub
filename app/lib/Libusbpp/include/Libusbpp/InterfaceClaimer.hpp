#pragma once

#include <memory>

#include "Libusbpp/DeviceHandle.hpp"

namespace Libusbpp {

class InterfaceClaimer
{
public:
    InterfaceClaimer() = delete;
    explicit InterfaceClaimer(
            const std::shared_ptr<DeviceHandle>& rpDeviceHandle,
            const int interfaceNum)
            : m_pDeviceHandle(rpDeviceHandle),
              m_InterfaceNum(interfaceNum)
    {
        m_pDeviceHandle->ClaimInterface(m_InterfaceNum);
    }
    InterfaceClaimer(
            const InterfaceClaimer&) = delete;
    InterfaceClaimer(
            InterfaceClaimer&&) = delete;
    InterfaceClaimer& operator =(
            const InterfaceClaimer&) = delete;
    InterfaceClaimer& operator =(
            InterfaceClaimer&&) = delete;
    ~InterfaceClaimer()
    {
        // Prevent throwing an exception in a destructor.
        try
        {
            m_pDeviceHandle->ReleaseInterface(m_InterfaceNum);
        }
        catch (...)
        {
            // Do nothing.
        }
    }

private:
    const std::shared_ptr<DeviceHandle> m_pDeviceHandle;
    const int m_InterfaceNum;
};

}
