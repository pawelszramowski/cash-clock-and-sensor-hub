#pragma once

#include <stdint.h>

namespace Libusbpp {
namespace Descriptor {

#pragma pack(push, 1)
struct Hid
{
    uint8_t m_bLength;
    uint8_t m_bDescriptorType;
    uint16_t m_bcdHID;
    uint8_t m_bCountryCode;
    uint8_t m_bNumDescriptors;
    uint8_t m_bReportDescriptorType;
    uint16_t m_wDescriptorLength;
};
#pragma pack(pop)

static_assert(sizeof(Hid) == 9, "Unexpected HID descriptor size!");

}
}
