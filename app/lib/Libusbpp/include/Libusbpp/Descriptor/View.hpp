#pragma once

#include <cstdint>
#include <iostream>
#include <stdexcept>

#include "StreamHex.hpp"

namespace Libusbpp {
namespace Descriptor {

class View
{
public:
    explicit View(
            const void* const pDescriptor,
            const size_t bufferSizeInBytes)
            : m_pDescriptor(pDescriptor)
    {
        if (pDescriptor == nullptr)
        {
            throw std::runtime_error("Invalid pointer to descriptor data provided!");
        }

        if (bufferSizeInBytes <= HEADER_LENGTH)
        {
            throw std::runtime_error("Invalid descriptor data length provided!");
        }

        if ((GetLength() <= HEADER_LENGTH) || (GetLength() > bufferSizeInBytes))
        {
            throw std::runtime_error("Invalid bLength field in a descriptor!");
        }
    }
    virtual ~View() = default;

    virtual std::ostream& Print(
            std::ostream& rStream) const
    {
        rStream << "Unknown descriptor: ";

        const uint8_t* pByte = static_cast<const uint8_t*>(GetRawDescriptor());
        for (size_t bytesLeft = GetLength(); bytesLeft > 0U; --bytesLeft)
        {
            static constexpr bool DO_NOT_SHOW_BASE = false;
            rStream << StreamHex(*pByte++, DO_NOT_SHOW_BASE);
        }

        return rStream;
    }

    const void* GetRawDescriptor() const
    {
        return m_pDescriptor;
    }

    uint8_t GetLength() const
    {
        return static_cast<const uint8_t*>(GetRawDescriptor())[LENGTH_OFFSET];
    }

    uint8_t GetType() const
    {
        return static_cast<const uint8_t*>(GetRawDescriptor())[TYPE_OFFSET];
    }

    const void* GetRawData() const
    {
        return &static_cast<const uint8_t*>(GetRawDescriptor())[DATA_OFFSET];
    }

    uint8_t GetDataLength() const
    {
        return GetLength() - HEADER_LENGTH;
    }

private:
    static constexpr uint8_t LENGTH_OFFSET = 0U;
    static constexpr uint8_t TYPE_OFFSET = 1U;
    static constexpr uint8_t DATA_OFFSET = 2U;

    static constexpr uint8_t HEADER_LENGTH = DATA_OFFSET;

    const void* const m_pDescriptor;
};

}
}
