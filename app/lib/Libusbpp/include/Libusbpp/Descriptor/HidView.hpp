#pragma once

#include <libusb-1.0/libusb.h>
#include <iostream>
#include <stdexcept>

#include "Libusbpp/Descriptor/Hid.hpp"
#include "Libusbpp/Descriptor/View.hpp"

namespace Libusbpp {
namespace Descriptor {

class HidView: public View
{
public:
    explicit HidView(
            const void* const pDescriptor,
            const size_t bufferSizeInBytes)
            : View(pDescriptor, bufferSizeInBytes)
    {
        if (GetType() != LIBUSB_DT_HID)
        {
            throw std::runtime_error("Invalid bDescriptorType field in a HID descriptor!");
        }

        if (GetLength() < sizeof(Hid))
        {
            throw std::runtime_error("Invalid bLength field in a HID descriptor!");
        }
    }

    virtual std::ostream& Print(
            std::ostream& rStream) const override
    {
        return rStream << GetDescriptor();
    }

    const Hid& GetDescriptor() const
    {
        return *static_cast<const Hid*>(GetRawDescriptor());
    }
};

}
}
