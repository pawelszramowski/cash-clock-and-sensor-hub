#pragma once

#include <libusb-1.0/libusb.h>
#include <memory>

#include "Libusbpp/Descriptor/HidView.hpp"
#include "Libusbpp/Descriptor/StringView.hpp"
#include "Libusbpp/Descriptor/View.hpp"

namespace Libusbpp {
namespace Descriptor {

class ViewFactory
{
public:
    ViewFactory() = delete;
    ViewFactory(
            const ViewFactory&) = delete;
    ViewFactory(
            ViewFactory&&) = delete;
    ViewFactory& operator =(
            const ViewFactory&) = delete;
    ViewFactory& operator =(
            ViewFactory&&) = delete;
    ~ViewFactory() = delete;

    static std::unique_ptr<View> CreateView(
            const void* const pDescriptor,
            const size_t sizeInBytes)
    {
        const View view(pDescriptor, sizeInBytes);

        switch (view.GetType())
        {
        case LIBUSB_DT_HID:
            return std::make_unique<HidView>(pDescriptor, sizeInBytes);
        case LIBUSB_DT_STRING:
            return std::make_unique<StringView>(pDescriptor, sizeInBytes);
        default:
            return std::make_unique<View>(view);
        }
    }
};

}
}
