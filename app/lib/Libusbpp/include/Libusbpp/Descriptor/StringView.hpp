#pragma once

#include <libusb-1.0/libusb.h>
#include <codecvt>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <string>

#include "Libusbpp/Descriptor/View.hpp"

namespace Libusbpp {
namespace Descriptor {

class StringView: public View
{
    static_assert(sizeof(char16_t) == 2U, "Each character in a string descriptor takes 2 bytes!");

public:
    static constexpr size_t MAX_LENGTH = UINT8_MAX;

    StringView(
            const void* const pDescriptor,
            const size_t bufferSizeInBytes)
            : View(pDescriptor, bufferSizeInBytes)
    {
        if (GetType() != LIBUSB_DT_STRING)
        {
            throw std::runtime_error("Invalid bDescriptorType field in a string descriptor!");
        }

        if (GetDataLength() % sizeof(char16_t) != 0)
        {
            throw std::runtime_error("Invalid bLength field in a string descriptor!");
        }
    }

    virtual std::ostream& Print(
            std::ostream& rStream) const override
    {
        return rStream << "String descriptor = " << GetUtf8();
    }

    std::u16string GetUtf16() const
    {
        return std::u16string(static_cast<const char16_t*>(GetRawData()),
                              GetDataLength() / sizeof(char16_t));
    }

    std::string GetUtf8() const
    {
        std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> converter;
        return converter.to_bytes(GetUtf16());
    }
};

}
}
