#pragma once

#include <iostream>

struct libusb_config_descriptor;
struct libusb_device_descriptor;
struct libusb_endpoint_descriptor;
struct libusb_interface;
struct libusb_interface_descriptor;

namespace Libusbpp {
namespace Descriptor {
struct Hid;
class View;
}
}

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_device_descriptor& rDeviceDescriptor);

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_config_descriptor& rConfigDescriptor);

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_interface& rInterface);

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_interface_descriptor& rInterfaceDescriptor);

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_endpoint_descriptor& rEndpointDescriptor);

std::ostream& operator <<(
        std::ostream& rStream,
        const Libusbpp::Descriptor::Hid& rHidDescriptor);

std::ostream& operator <<(
        std::ostream& rStream,
        const Libusbpp::Descriptor::View& rDescriptorView);
