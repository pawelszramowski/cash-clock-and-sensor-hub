#pragma once

#include <memory>

#include "Libusbpp/Deleter.hpp"

struct libusb_config_descriptor;
struct libusb_context;
struct libusb_device;
struct libusb_device_descriptor;

namespace Libusbpp {

class Deleter;
class DeviceHandle;

class Device: public std::enable_shared_from_this<Device>
{
public:
    Device() = delete;
    explicit Device(
            const std::shared_ptr<libusb_context>& rpContext,
            libusb_device* const pRawDevice);
    Device(
            const Device&) = delete;
    Device(
            Device&&) = delete;
    Device& operator =(
            const Device&) = delete;
    Device& operator =(
            Device&&) = delete;
    ~Device() = default;

    const libusb_device_descriptor& GetDeviceDescriptor();

    const libusb_config_descriptor& GetActiveConfigDescriptor();

    std::shared_ptr<DeviceHandle> GetHandle();

private:
    // WARNING: Order of declaration is important as resources
    // will be released in the inverse order of declaration!
    const std::shared_ptr<libusb_context> m_pContext;
    const std::unique_ptr<libusb_device, Deleter> m_pDevice;
    std::unique_ptr<libusb_device_descriptor> m_pDeviceDescriptor;
    std::unique_ptr<libusb_config_descriptor, Deleter> m_pConfigDescriptor;
    std::weak_ptr<DeviceHandle> m_pObservedHandle;
};

}
