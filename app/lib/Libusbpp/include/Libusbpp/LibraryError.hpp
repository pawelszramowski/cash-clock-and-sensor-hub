#pragma once

#include <libusb-1.0/libusb.h>
#include <stdexcept>
#include <string>

namespace Libusbpp {

class LibraryError: public std::runtime_error
{
public:
    /// @note This constructor will also be used for ssize_t error code (with implicit conversion
    /// on systems where ssize_t is equivalent to long long int).
    explicit LibraryError(
            const std::string& rWhere,
            const int errorCode)
            : LibraryError(rWhere, static_cast<libusb_error>(errorCode))
    {
        // Do nothing.
    }
    explicit LibraryError(
            const std::string& rWhere,
            const libusb_error errorCode)
            : std::runtime_error(BuildMessage(rWhere, errorCode)),
              m_ErrorCode(errorCode)
    {
        // Do nothing.
    }

    libusb_error GetErrorCode() const
    {
        return m_ErrorCode;
    }

private:
    static const std::string BuildMessage(
            const std::string& rWhere,
            const libusb_error errorCode)
    {
        const std::string error = std::string(libusb_error_name(errorCode))
                + " (" + std::string(libusb_strerror(errorCode)) + ")";

        if (!rWhere.empty())
        {
            return rWhere + ": " + error;
        }
        else
        {
            return error;
        }
    }

    const libusb_error m_ErrorCode;
};

}
