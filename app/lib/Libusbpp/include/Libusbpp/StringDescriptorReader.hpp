#pragma once

#include <libusb-1.0/libusb.h>
#include <cstdint>
#include <string>

#include "Libusbpp/Descriptor/StringView.hpp"
#include "Libusbpp/LibraryError.hpp"

struct libusb_device_handle;

namespace Libusbpp {

class StringDescriptorReader
{
public:
    explicit StringDescriptorReader(
            const uint8_t descriptorIndex,
            const uint16_t languageId)
            : m_DescriptorIndex(descriptorIndex),
              m_LanguageId(languageId),
              m_Content()
    {
        // Do nothing.
    }

    void ReadFromDevice(
            libusb_device_handle* const pRawDeviceHandle)
    {
        const int result = libusb_get_string_descriptor(pRawDeviceHandle,
                                                        m_DescriptorIndex,
                                                        m_LanguageId, m_Content,
                                                        sizeof(m_Content));

        if (result < 0)
        {
            throw LibraryError("libusb_get_string_descriptor", result);
        }
    }

    std::string GetUtf8() const
    {
        return Descriptor::StringView(m_Content, sizeof(m_Content)).GetUtf8();
    }

    std::u16string GetUtf16() const
    {
        return Descriptor::StringView(m_Content, sizeof(m_Content)).GetUtf16();
    }

private:
    const uint8_t m_DescriptorIndex;
    const uint16_t m_LanguageId;
    unsigned char m_Content[Descriptor::StringView::MAX_LENGTH];
};

}
