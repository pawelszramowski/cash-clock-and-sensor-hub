#pragma once

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <stdexcept>
#include <string>

namespace Libusbpp {

class LangIdCodec
{
public:
    LangIdCodec() = delete;
    LangIdCodec(
            const LangIdCodec&) = delete;
    LangIdCodec(
            LangIdCodec&&) = delete;
    LangIdCodec& operator =(
            const LangIdCodec&) = delete;
    LangIdCodec& operator =(
            LangIdCodec&&) = delete;
    ~LangIdCodec() = delete;

    static const std::string GetName(
            const uint16_t id)
    {
        const auto found = std::find_if(std::begin(m_Languages), std::end(m_Languages),
                                        [id](const auto& language)
                                        { return language.m_Id == id; });
        if (found == std::end(m_Languages))
        {
            throw std::invalid_argument("Language with given ID not found!");
        }
        else
        {
            return std::string(found->m_pName);
        }
    }
    static uint16_t GetId(
            const std::string& rName)
    {
        const auto found = std::find_if(std::begin(m_Languages), std::end(m_Languages),
                                        [&rName](const auto& language)
                                        { return language.m_pName == rName; });
        if (found == std::end(m_Languages))
        {
            throw std::invalid_argument("Language with given name not found!");
        }
        else
        {
            return found->m_Id;
        }
    }

private:
    static constexpr struct Language
    {
        uint16_t m_Id;
        const char* m_pName;
    } m_Languages[] =
            {
                    { 0x0436, "Afrikaans" },
                    { 0x041C, "Albanian" },
                    { 0x0401, "Arabic (Saudi Arabia)" },
                    { 0x0801, "Arabic (Iraq)" },
                    { 0x0C01, "Arabic (Egypt)" },
                    { 0x1001, "Arabic (Libya)" },
                    { 0x1401, "Arabic (Algeria)" },
                    { 0x1801, "Arabic (Morocco)" },
                    { 0x1C01, "Arabic (Tunisia)" },
                    { 0x2001, "Arabic (Oman)" },
                    { 0x2401, "Arabic (Yemen)" },
                    { 0x2801, "Arabic (Syria)" },
                    { 0x2C01, "Arabic (Jordan)" },
                    { 0x3001, "Arabic (Lebanon)" },
                    { 0x3401, "Arabic (Kuwait)" },
                    { 0x3801, "Arabic (U.A.E.)" },
                    { 0x3C01, "Arabic (Bahrain)" },
                    { 0x4001, "Arabic (Qatar)" },
                    { 0x042B, "Armenian" },
                    { 0x044D, "Assamese" },
                    { 0x042C, "Azeri (Latin)" },
                    { 0x082C, "Azeri (Cyrillic)" },
                    { 0x042D, "Basque" },
                    { 0x0423, "Belarussian" },
                    { 0x0445, "Bengali" },
                    { 0x0402, "Bulgarian" },
                    { 0x0455, "Burmese" },
                    { 0x0403, "Catalan" },
                    { 0x0404, "Chinese (Taiwan)" },
                    { 0x0804, "Chinese (PRC)" },
                    { 0x0C04, "Chinese (Hong Kong SAR, PRC)" },
                    { 0x1004, "Chinese (Singapore)" },
                    { 0x1404, "Chinese (Macau SAR)" },
                    { 0x041A, "Croatian" },
                    { 0x0405, "Czech" },
                    { 0x0406, "Danish" },
                    { 0x0413, "Dutch (Netherlands)" },
                    { 0x0813, "Dutch (Belgium)" },
                    { 0x0409, "English (United States)" },
                    { 0x0809, "English (United Kingdom)" },
                    { 0x0C09, "English (Australian)" },
                    { 0x1009, "English (Canadian)" },
                    { 0x1409, "English (New Zealand)" },
                    { 0x1809, "English (Ireland)" },
                    { 0x1C09, "English (South Africa)" },
                    { 0x2009, "English (Jamaica)" },
                    { 0x2409, "English (Caribbean)" },
                    { 0x2809, "English (Belize)" },
                    { 0x2C09, "English (Trinidad)" },
                    { 0x3009, "English (Zimbabwe)" },
                    { 0x3409, "English (Philippines)" },
                    { 0x0425, "Estonian" },
                    { 0x0438, "Faeroese" },
                    { 0x0429, "Farsi" },
                    { 0x040B, "Finnish" },
                    { 0x040C, "French (Standard)" },
                    { 0x080C, "French (Belgian)" },
                    { 0x0C0C, "French (Canadian)" },
                    { 0x100C, "French (Switzerland)" },
                    { 0x140C, "French (Luxembourg)" },
                    { 0x180C, "French (Monaco)" },
                    { 0x0437, "Georgian" },
                    { 0x0407, "German (Standard)" },
                    { 0x0807, "German (Switzerland)" },
                    { 0x0C07, "German (Austria)" },
                    { 0x1007, "German (Luxembourg)" },
                    { 0x1407, "German (Liechtenstein)" },
                    { 0x0408, "Greek" },
                    { 0x0447, "Gujarati" },
                    { 0x040D, "Hebrew" },
                    { 0x0439, "Hindi" },
                    { 0x040E, "Hungarian" },
                    { 0x040F, "Icelandic" },
                    { 0x0421, "Indonesian" },
                    { 0x0410, "Italian (Standard)" },
                    { 0x0810, "Italian (Switzerland)" },
                    { 0x0411, "Japanese" },
                    { 0x044B, "Kannada" },
                    { 0x0860, "Kashmiri (India)" },
                    { 0x043F, "Kazakh" },
                    { 0x0457, "Konkani" },
                    { 0x0412, "Korean" },
                    { 0x0812, "Korean (Johab)" },
                    { 0x0426, "Latvian" },
                    { 0x0427, "Lithuanian" },
                    { 0x0827, "Lithuanian (Classic)" },
                    { 0x042F, "Macedonian" },
                    { 0x043E, "Malay (Malaysian)" },
                    { 0x083E, "Malay (Brunei Darussalam)" },
                    { 0x044C, "Malayalam" },
                    { 0x0458, "Manipuri" },
                    { 0x044E, "Marathi" },
                    { 0x0861, "Nepali (India)" },
                    { 0x0414, "Norwegian (Bokmal)" },
                    { 0x0814, "Norwegian (Nynorsk)" },
                    { 0x0448, "Oriya" },
                    { 0x0415, "Polish" },
                    { 0x0416, "Portuguese (Brazil)" },
                    { 0x0816, "Portuguese (Standard)" },
                    { 0x0446, "Punjabi" },
                    { 0x0418, "Romanian" },
                    { 0x0419, "Russian" },
                    { 0x044F, "Sanskrit" },
                    { 0x0C1A, "Serbian (Cyrillic)" },
                    { 0x081A, "Serbian (Latin)" },
                    { 0x0459, "Sindhi" },
                    { 0x041B, "Slovak" },
                    { 0x0424, "Slovenian" },
                    { 0x040A, "Spanish (Traditional Sort)" },
                    { 0x080A, "Spanish (Mexican)" },
                    { 0x0C0A, "Spanish (Modern Sort)" },
                    { 0x100A, "Spanish (Guatemala)" },
                    { 0x140A, "Spanish (Costa Rica)" },
                    { 0x180A, "Spanish (Panama)" },
                    { 0x1C0A, "Spanish (Dominican Republic)" },
                    { 0x200A, "Spanish (Venezuela)" },
                    { 0x240A, "Spanish (Colombia)" },
                    { 0x280A, "Spanish (Peru)" },
                    { 0x2C0A, "Spanish (Argentina)" },
                    { 0x300A, "Spanish (Ecuador)" },
                    { 0x340A, "Spanish (Chile)" },
                    { 0x380A, "Spanish (Uruguay)" },
                    { 0x3C0A, "Spanish (Paraguay)" },
                    { 0x400A, "Spanish (Bolivia)" },
                    { 0x440A, "Spanish (El Salvador)" },
                    { 0x480A, "Spanish (Honduras)" },
                    { 0x4C0A, "Spanish (Nicaragua)" },
                    { 0x500A, "Spanish (Puerto Rico)" },
                    { 0x0430, "Sutu" },
                    { 0x0441, "Swahili (Kenya)" },
                    { 0x041D, "Swedish" },
                    { 0x081D, "Swedish (Finland)" },
                    { 0x0449, "Tamil" },
                    { 0x0444, "Tatar (Tatarstan)" },
                    { 0x044A, "Telugu" },
                    { 0x041E, "Thai" },
                    { 0x041F, "Turkish" },
                    { 0x0422, "Ukrainian" },
                    { 0x0420, "Urdu (Pakistan)" },
                    { 0x0820, "Urdu (India)" },
                    { 0x0443, "Uzbek (Latin)" },
                    { 0x0843, "Uzbek (Cyrillic)" },
                    { 0x042A, "Vietnamese" },
                    { 0x04FF, "HID (Usage Data Descriptor)" },
                    { 0xF0FF, "HID (Vendor Defined 1)" },
                    { 0xF4FF, "HID (Vendor Defined 2)" },
                    { 0xF8FF, "HID (Vendor Defined 3)" },
                    { 0xFCFF, "HID (Vendor Defined 4)" },
            };
};

}
