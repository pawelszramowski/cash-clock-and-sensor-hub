#pragma once

#include <libusb-1.0/libusb.h>
#include <cstdint>

#include "Libusbpp/LibraryError.hpp"

#include "Libusbpp/SyncIo/Transfer.hpp"

namespace Libusbpp {
namespace SyncIo {

class InterruptTransfer: public Transfer
{
public:
    /// @pre Interface must be claimed before communicating with its endpoints!
    /// @note Less bytes than expected may be received without an error since it's the device
    /// who decides what data to send to the host.
    virtual int Perform(
            libusb_device_handle* const pRawHandle,
            const unsigned int timeoutInMs) override
    {
        int transferredBytes = 0;
        const int result = libusb_interrupt_transfer(pRawHandle, m_Endpoint,
                                                     static_cast<unsigned char*>(m_pData),
                                                     m_LengthInBytes, &transferredBytes,
                                                     timeoutInMs);

        if ((result == LIBUSB_ERROR_TIMEOUT) && (transferredBytes > 0))
        {
            throw std::runtime_error(
                    "Time-out before transferring all the bytes of interrupt transfer!");
        }
        else if (result != LIBUSB_SUCCESS)
        {
            throw LibraryError("libusb_interrupt_transfer", result);
        }
        else
        {
            return transferredBytes;
        }
    }

protected:
    InterruptTransfer() = default;
    InterruptTransfer(
            const InterruptTransfer&) = default;
    InterruptTransfer(
            InterruptTransfer&&) = default;
    InterruptTransfer& operator =(
            const InterruptTransfer&) = default;
    InterruptTransfer& operator =(
            InterruptTransfer&&) = default;
    virtual ~InterruptTransfer() = default;

    void SetEndpoint(
            const libusb_endpoint_direction direction,
            const uint8_t number)
    {
        m_Endpoint = (direction & LIBUSB_ENDPOINT_DIR_MASK)
                | (number & LIBUSB_ENDPOINT_ADDRESS_MASK);
    }

    void SetData(
            void* const pData,
            const int length)
    {
        m_pData = pData;
        m_LengthInBytes = length;
    }

    template<typename T>
    void SetData(
            T& rData)
    {
        m_pData = &rData;
        m_LengthInBytes = sizeof(rData);
    }

private:
    uint8_t m_Endpoint = 0U;
    void* m_pData = nullptr;
    int m_LengthInBytes = 0;
};

}
}
