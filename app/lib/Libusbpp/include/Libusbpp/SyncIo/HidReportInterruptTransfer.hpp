#pragma once

#include <libusb-1.0/libusb.h>
#include <cstdint>

#include "Libusbpp/SyncIo/InterruptTransfer.hpp"

namespace Libusbpp {
namespace SyncIo {

class HidReportInterruptTransfer: public InterruptTransfer
{
public:
    HidReportInterruptTransfer() = delete;
    template<typename T>
    explicit HidReportInterruptTransfer(
            T& rReport,
            const libusb_endpoint_direction direction,
            const uint8_t endpointNum)
            : InterruptTransfer()
    {
        SetEndpoint(direction, endpointNum);
        SetData(rReport);
    }
    HidReportInterruptTransfer(
            const HidReportInterruptTransfer&) = default;
    HidReportInterruptTransfer(
            HidReportInterruptTransfer&&) = default;
    HidReportInterruptTransfer& operator =(
            const HidReportInterruptTransfer&) = default;
    HidReportInterruptTransfer& operator =(
            HidReportInterruptTransfer&&) = default;
    virtual ~HidReportInterruptTransfer() = default;
};

}
}
