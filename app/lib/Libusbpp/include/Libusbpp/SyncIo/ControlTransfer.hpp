#pragma once

#include <libusb-1.0/libusb.h>
#include <cstdint>

#include "Libusbpp/LibraryError.hpp"

#include "Libusbpp/SyncIo/Transfer.hpp"

namespace Libusbpp {
namespace SyncIo {

class ControlTransfer: public Transfer
{
public:
    virtual int Perform(
            libusb_device_handle* const pRawHandle,
            const unsigned int timeoutInMs) override
    {
        const int result = libusb_control_transfer(pRawHandle, m_bmRequestType, m_bRequest,
                                                   m_wValue,
                                                   m_wIndex,
                                                   static_cast<unsigned char*>(m_pData),
                                                   m_wLength, timeoutInMs);

        if (result < 0)
        {
            throw LibraryError("libusb_control_transfer", result);
        }
        else if (static_cast<uint16_t>(result) != m_wLength)
        {
            throw std::runtime_error("Number of transferred bytes doesn't match data size!");
        }
        else
        {
            return result;
        }
    }

protected:
    ControlTransfer() = default;
    ControlTransfer(
            const ControlTransfer&) = default;
    ControlTransfer(
            ControlTransfer&&) = default;
    ControlTransfer& operator =(
            const ControlTransfer&) = default;
    ControlTransfer& operator =(
            ControlTransfer&&) = default;
    virtual ~ControlTransfer() = default;

    void SetRequestType(
            const libusb_endpoint_direction direction,
            const libusb_request_type type,
            const libusb_request_recipient recipient)
    {
        m_bmRequestType = (direction & LIBUSB_ENDPOINT_DIR_MASK) | type | recipient;
    }

    void SetRequest(
            const uint8_t bRequest)
    {
        m_bRequest = bRequest;
    }

    void SetIndex(
            const uint16_t wIndex)
    {
        m_wIndex = wIndex;
    }

    void SetValue(
            const uint16_t wValue)
    {
        m_wValue = wValue;
    }

    void SetData(
            void* const pData,
            const uint16_t wLength)
    {
        m_pData = pData;
        m_wLength = wLength;
    }

    template<typename T>
    void SetData(
            T& rData)
    {
        m_pData = &rData;
        m_wLength = sizeof(rData);
    }

private:
    uint8_t m_bmRequestType = 0U;
    uint8_t m_bRequest = 0U;
    uint16_t m_wValue = 0U;
    uint16_t m_wIndex = 0U;
    void* m_pData = nullptr;
    uint16_t m_wLength = 0U;
};

}
}
