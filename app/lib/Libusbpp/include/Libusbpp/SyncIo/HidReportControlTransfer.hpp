#pragma once

#include <libusb-1.0/libusb.h>
#include <climits>
#include <cstdint>
#include <stdexcept>

#include "Usb/Hid/Constants.hpp"

#include "Libusbpp/SyncIo/ControlTransfer.hpp"

namespace Libusbpp {
namespace SyncIo {

class HidReportControlTransfer: public ControlTransfer
{
public:
    HidReportControlTransfer() = delete;
    template<typename T>
    explicit HidReportControlTransfer(
            T& rReport,
            const libusb_endpoint_direction direction,
            const Usb::Hid::ReportType reportType,
            const uint8_t reportId,
            const uint16_t interface = 0U)
            : ControlTransfer()
    {
        SetDirection(direction);
        SetReport(reportType, reportId);
        SetInterface(interface);
        SetData(rReport);

        if ((reportId != Usb::Hid::REPORT_ID_UNUSED)
                && ((direction & LIBUSB_ENDPOINT_DIR_MASK) == LIBUSB_ENDPOINT_OUT))
        {
            const uint8_t* const pFirstReportByte = reinterpret_cast<const uint8_t*>(&rReport);
            if (*pFirstReportByte != reportId)
            {
                throw std::invalid_argument(
                        "If ID is used then the transmitted report must start with it!");
            }
        }
    }
    HidReportControlTransfer(
            const HidReportControlTransfer&) = default;
    HidReportControlTransfer(
            HidReportControlTransfer&&) = default;
    HidReportControlTransfer& operator =(
            const HidReportControlTransfer&) = default;
    HidReportControlTransfer& operator =(
            HidReportControlTransfer&&) = default;
    virtual ~HidReportControlTransfer() = default;

protected:
    void SetDirection(
            const libusb_endpoint_direction direction)
    {
        SetRequestType(direction, LIBUSB_REQUEST_TYPE_CLASS, LIBUSB_RECIPIENT_INTERFACE);
        SetRequest(
                ((direction & LIBUSB_ENDPOINT_DIR_MASK) == LIBUSB_ENDPOINT_IN) ? Usb::Hid::GET_REPORT :
                                                                                 Usb::Hid::SET_REPORT);
    }

    void SetReport(
            const Usb::Hid::ReportType reportType,
            const uint8_t reportId)
    {
        SetValue((reportType << CHAR_BIT) | reportId);
    }

    void SetInterface(
            const uint16_t interface)
    {
        SetIndex(interface);
    }
};

}
}
