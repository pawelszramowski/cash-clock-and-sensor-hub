#pragma once

struct libusb_device_handle;

namespace Libusbpp {
namespace SyncIo {

class Transfer
{
public:
    static constexpr unsigned int NO_TIMEOUT = 0U;

    Transfer() = default;
    Transfer(
            const Transfer&) = default;
    Transfer(
            Transfer&&) = default;
    Transfer& operator =(
            const Transfer&) = default;
    Transfer& operator =(
            Transfer&&) = default;
    virtual ~Transfer() = default;

    /// @retval Number of transferred bytes.
    virtual int Perform(
            libusb_device_handle* const pRawHandle,
            const unsigned int timeoutInMs) = 0;
};

}
}
