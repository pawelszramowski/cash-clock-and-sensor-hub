#pragma once

#include <libusb-1.0/libusb.h>
#include <memory>
#include <vector>

#include "Libusbpp/Deleter.hpp"
#include "Libusbpp/Device.hpp"
#include "Libusbpp/LibraryError.hpp"

namespace Libusbpp {

class Context
{
public:
    Context() = delete;
    explicit Context(
            libusb_log_level logLevel = LIBUSB_LOG_LEVEL_NONE)
            : m_pContext(InitContext(), Deleter()),
              m_pDevices()
    {
        libusb_set_option(m_pContext.get(), LIBUSB_OPTION_LOG_LEVEL, logLevel);
    }
    Context(
            const Context&) = delete;
    Context(
            Context&&) = delete;
    Context& operator =(
            const Context&) = delete;
    Context& operator =(
            Context&&) = delete;
    ~Context() = default;

    const std::vector<std::shared_ptr<Device>>& GetAllDevices()
    {
        if (!m_pDevices)
        {
            libusb_device** pRawDevices = nullptr;
            const ssize_t result = libusb_get_device_list(m_pContext.get(), &pRawDevices);

            const std::unique_ptr<libusb_device*, Deleter> pDevices(pRawDevices, Deleter());

            if (result < 0)
            {
                throw LibraryError("libusb_get_device_list", result);
            }
            else
            {
                m_pDevices = std::make_unique<std::vector<std::shared_ptr<Device>>>(result);
                for (ssize_t i = 0; i < result; ++i)
                {
                    m_pDevices->at(i) = std::make_shared<Device>(m_pContext, pDevices.get()[i]);
                }
            }
        }

        return *m_pDevices.get();
    }

private:
    static libusb_context* InitContext()
    {
        libusb_context* pRawContext = nullptr;
        const int result = libusb_init(&pRawContext);

        if (result != LIBUSB_SUCCESS)
        {
            throw LibraryError("libusb_init", result);
        }
        else
        {
            return pRawContext;
        }
    }

    std::shared_ptr<libusb_context> m_pContext;
    std::unique_ptr<std::vector<std::shared_ptr<Device>>> m_pDevices;
};

}
