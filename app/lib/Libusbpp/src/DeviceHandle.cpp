#include "Libusbpp/DeviceHandle.hpp"

#include <libusb-1.0/libusb.h>
#include <algorithm>
#include <iterator>
#include <stdexcept>

#include "Libusbpp/Device.hpp"
#include "Libusbpp/LibraryError.hpp"
#include "Libusbpp/StringDescriptorReader.hpp"

using namespace Libusbpp;

DeviceHandle::DeviceHandle(
        const std::shared_ptr<Device>& rpDevice,
        libusb_device* const pRawDevice)
        : m_pDevice(rpDevice),
          m_pHandle(OpenDevice(pRawDevice), Deleter()),
          m_ClaimedInterfaces(),
          m_LangIds()
{
    if (!m_pDevice)
    {
        throw std::invalid_argument("Invalid Libusbpp::Device!");
    }
    else if (!m_pHandle)
    {
        throw std::invalid_argument("Invalid Libusb device handle!");
    }
}

DeviceHandle::~DeviceHandle()
{
    while (!m_ClaimedInterfaces.empty())
    {
        // Prevent throwing an exception in a destructor.
        try
        {
            ReleaseInterface(m_ClaimedInterfaces.back());
        }
        catch (...)
        {
            // Do nothing.
        }
    }
}

std::shared_ptr<Device> DeviceHandle::GetDevice() const
{
    return m_pDevice;
}

const std::vector<uint16_t>& DeviceHandle::GetLangIds()
{
    if (m_LangIds.empty())
    {
        StringDescriptorReader langIdsDescriptor(LANGID_DESCRIPTOR_INDEX, DEFAULT_LANGID);
        langIdsDescriptor.ReadFromDevice(m_pHandle.get());

        const auto& rLangIds = langIdsDescriptor.GetUtf16();
        m_LangIds.assign(rLangIds.begin(), rLangIds.end());
        if (m_LangIds.empty())
        {
            throw std::runtime_error("Device has got an empty LangId string descriptor!");
        }
    }

    return m_LangIds;
}

bool DeviceHandle::IsLangIdSupported(
        const uint16_t langId)
{
    const auto langIds = GetLangIds();
    return std::find(langIds.begin(), langIds.end(), langId) != langIds.end();
}

std::string DeviceHandle::GetStringDescriptor(
        const uint8_t descriptorIndex,
        uint16_t langId)
{
    if (descriptorIndex != LANGID_DESCRIPTOR_INDEX)
    {
        if (langId == DEFAULT_LANGID)
        {
            langId = GetLangIds().front();
        }
        else if (!IsLangIdSupported(langId))
        {
            throw std::runtime_error("Given language is not supported by the device!");
        }

        StringDescriptorReader stringDescriptor(descriptorIndex, langId);
        stringDescriptor.ReadFromDevice(m_pHandle.get());
        return stringDescriptor.GetUtf8();
    }
    else
    {
        return std::string();
    }
}

std::string DeviceHandle::GetManufacturer(
        const uint16_t langId)
{
    return GetStringDescriptor(m_pDevice->GetDeviceDescriptor().iManufacturer, langId);
}

std::string DeviceHandle::GetProduct(
        const uint16_t langId)
{
    return GetStringDescriptor(m_pDevice->GetDeviceDescriptor().iProduct, langId);
}

std::string DeviceHandle::GetSerialNumber(
        const uint16_t langId)
{
    return GetStringDescriptor(m_pDevice->GetDeviceDescriptor().iSerialNumber, langId);
}

void DeviceHandle::ClaimInterface(
        const int interfaceNum)
{
    if (IsInterfaceClaimed(interfaceNum))
    {
        throw std::runtime_error("Interface is already claimed!");
    }
    else
    {
        const int result = libusb_claim_interface(m_pHandle.get(), interfaceNum);

        if (result != LIBUSB_SUCCESS)
        {
            throw LibraryError("libusb_claim_interface", result);
        }
        else
        {
            (void)m_ClaimedInterfaces.push_back(interfaceNum);
        }
    }
}

void DeviceHandle::ReleaseInterface(
        const int interfaceNum)
{
    if (!IsInterfaceClaimed(interfaceNum))
    {
        throw std::runtime_error("Interface is not claimed!");
    }
    else
    {
        (void)m_ClaimedInterfaces.erase(
                std::find(m_ClaimedInterfaces.begin(), m_ClaimedInterfaces.end(), interfaceNum));

        const int result = libusb_release_interface(m_pHandle.get(), interfaceNum);

        if (result != LIBUSB_SUCCESS)
        {
            throw LibraryError("libusb_release_interface", result);
        }
    }
}

int DeviceHandle::PerformTransfer(
        SyncIo::Transfer& rTransfer,
        const unsigned int timeoutInMs) const
{
    return rTransfer.Perform(m_pHandle.get(), timeoutInMs);
}

libusb_device_handle* DeviceHandle::OpenDevice(
        libusb_device* const pRawDevice)
{
    libusb_device_handle* pRawHandle = nullptr;
    const int result = libusb_open(pRawDevice, &pRawHandle);

    if (result != LIBUSB_SUCCESS)
    {
        throw LibraryError("libusb_open", result);
    }
    else
    {
        return pRawHandle;
    }
}

bool DeviceHandle::IsInterfaceClaimed(
        const int interfaceNum) const
{
    return std::find(m_ClaimedInterfaces.begin(), m_ClaimedInterfaces.end(), interfaceNum)
            != m_ClaimedInterfaces.end();
}
