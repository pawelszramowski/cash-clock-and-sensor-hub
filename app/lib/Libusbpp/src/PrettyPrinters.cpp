#include "Libusbpp/PrettyPrinters.hpp"

#include <libusb-1.0/libusb.h>
#include <cstdint>
#include <iostream>
#include <memory>

#include "Libusbpp/Descriptor/Hid.hpp"
#include "Libusbpp/Descriptor/View.hpp"
#include "Libusbpp/Descriptor/ViewFactory.hpp"
#include "StreamHex.hpp"

// NOTE: + operator is used with char values to print them as numbers instead of characters.

static std::ostream& PrintExtraDescriptor(
        std::ostream& rStream,
        const unsigned char* const pExtra,
        const int extraLength)
{
    if (pExtra != nullptr)
    {
        int currentOffset = 0;
        int bytesLeft = extraLength;

        while (bytesLeft > 0)
        {
            const auto pView = Libusbpp::Descriptor::ViewFactory::CreateView(&pExtra[currentOffset],
                                                                             bytesLeft);

            rStream << '\n' << *pView;

            currentOffset += pView->GetLength();
            bytesLeft -= pView->GetLength();
        }
    }

    return rStream;
}

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_device_descriptor& rDeviceDescriptor)
{
    rStream << "Device descriptor:\n"
            << "- bLen = " << +rDeviceDescriptor.bLength << '\n'
            << "- bDescType = " << +rDeviceDescriptor.bDescriptorType << '\n'
            << "- bcdUSB = " << StreamHex(rDeviceDescriptor.bcdUSB) << '\n'
            << "- bDeviceClass = " << +rDeviceDescriptor.bDeviceClass << '\n'
            << "- bDeviceSubClass = " << +rDeviceDescriptor.bDeviceSubClass << '\n'
            << "- bDeviceProtocol = " << +rDeviceDescriptor.bDeviceProtocol << '\n'
            << "- bMaxPacketSize0 = " << +rDeviceDescriptor.bMaxPacketSize0 << '\n'
            << "- idVendor = " << StreamHex(rDeviceDescriptor.idVendor) << '\n'
            << "- idProduct = " << StreamHex(rDeviceDescriptor.idProduct) << '\n'
            << "- bcdDevice = " << StreamHex(rDeviceDescriptor.bcdDevice) << '\n'
            << "- iManufacturer = " << +rDeviceDescriptor.iManufacturer << '\n'
            << "- iProduct = " << +rDeviceDescriptor.iProduct << '\n'
            << "- iSerialNumber = " << +rDeviceDescriptor.iSerialNumber << '\n'
            << "- bNumConfigurations = " << +rDeviceDescriptor.bNumConfigurations;

    return rStream;
}

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_config_descriptor& rConfigDescriptor)
{
    rStream << "Configuration descriptor:\n"
            << "- bLength = " << +rConfigDescriptor.bLength << '\n'
            << "- bDescriptorType = " << +rConfigDescriptor.bDescriptorType << '\n'
            << "- wTotalLength = " << +rConfigDescriptor.wTotalLength << '\n'
            << "- bNumInterfaces = " << +rConfigDescriptor.bNumInterfaces << '\n'
            << "- bConfigurationValue = " << +rConfigDescriptor.bConfigurationValue << '\n'
            << "- iConfiguration = " << +rConfigDescriptor.iConfiguration << '\n'
            << "- bmAttributes = " << StreamHex(rConfigDescriptor.bmAttributes) << '\n'
            << "- MaxPower = " << +rConfigDescriptor.MaxPower;

    for (uint8_t i = 0U; i < rConfigDescriptor.bNumInterfaces; ++i)
    {
        rStream << "\n- interface " << +i << ":\n" << *rConfigDescriptor.interface;
    }

    return PrintExtraDescriptor(rStream, rConfigDescriptor.extra, rConfigDescriptor.extra_length);
}

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_interface& rInterface)
{
    for (int i = 0; i < rInterface.num_altsetting; ++i)
    {
        rStream << rInterface.altsetting[i];
    }

    return rStream;
}

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_interface_descriptor& rInterfaceDescriptor)
{
    rStream << "Interface descriptor:\n"
            << "- bLength = " << +rInterfaceDescriptor.bLength << '\n'
            << "- bDescriptorType = " << +rInterfaceDescriptor.bDescriptorType << '\n'
            << "- bInterfaceNumber = " << +rInterfaceDescriptor.bInterfaceNumber << '\n'
            << "- bAlternateSetting = " << +rInterfaceDescriptor.bAlternateSetting << '\n'
            << "- bNumEndpoints = " << +rInterfaceDescriptor.bNumEndpoints << '\n'
            << "- bInterfaceClass = " << +rInterfaceDescriptor.bInterfaceClass << '\n'
            << "- bInterfaceSubClass = " << +rInterfaceDescriptor.bInterfaceSubClass << '\n'
            << "- bInterfaceProtocol = " << +rInterfaceDescriptor.bInterfaceProtocol << '\n'
            << "- iInterface = " << +rInterfaceDescriptor.iInterface;

    for (uint8_t i = 0; i < rInterfaceDescriptor.bNumEndpoints; ++i)
    {
        rStream << "\n- endpoint " << +i << ":\n" << rInterfaceDescriptor.endpoint[i];
    }

    return PrintExtraDescriptor(rStream, rInterfaceDescriptor.extra,
                                rInterfaceDescriptor.extra_length);
}

std::ostream& operator <<(
        std::ostream& rStream,
        const libusb_endpoint_descriptor& rEndpointDescriptor)
{
    rStream << "Endpoint descriptor:\n"
            << "- bLength = " << +rEndpointDescriptor.bLength << '\n'
            << "- bDescriptorType = " << +rEndpointDescriptor.bDescriptorType << '\n'
            << "- bEndpointAddress = " << StreamHex(rEndpointDescriptor.bEndpointAddress) << '\n'
            << "- bmAttributes = " << StreamHex(rEndpointDescriptor.bmAttributes) << '\n'
            << "- wMaxPacketSize = " << +rEndpointDescriptor.wMaxPacketSize << '\n'
            << "- bInterval = " << +rEndpointDescriptor.bInterval << '\n'
            << "- bRefresh = " << +rEndpointDescriptor.bRefresh << '\n'
            << "- bSynchAddress = " << StreamHex(rEndpointDescriptor.bSynchAddress);

    return PrintExtraDescriptor(rStream, rEndpointDescriptor.extra,
                                rEndpointDescriptor.extra_length);
}

std::ostream& operator <<(
        std::ostream& rStream,
        const Libusbpp::Descriptor::Hid& rHidDescriptor)
{
    rStream << "HID descriptor:\n"
            << "- bLen = " << +rHidDescriptor.m_bLength << '\n'
            << "- bDescType = " << +rHidDescriptor.m_bDescriptorType << '\n'
            << "- bcdHID = " << StreamHex(rHidDescriptor.m_bcdHID) << '\n'
            << "- bCountryCode = " << +rHidDescriptor.m_bCountryCode << '\n'
            << "- bNumDescriptors = " << +rHidDescriptor.m_bNumDescriptors << '\n'
            << "- bReportDescriptorType = " << +rHidDescriptor.m_bReportDescriptorType << '\n'
            << "- wDescriptorLength = " << +rHidDescriptor.m_wDescriptorLength;

    return rStream;
}

std::ostream& operator <<(
        std::ostream& rStream,
        const Libusbpp::Descriptor::View& rDescriptorView)
{
    return rDescriptorView.Print(rStream);
}
