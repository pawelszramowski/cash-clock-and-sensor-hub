#include "Libusbpp/Device.hpp"

#include <libusb-1.0/libusb.h>
#include <algorithm>
#include <stdexcept>

#include "Libusbpp/Deleter.hpp"
#include "Libusbpp/DeviceHandle.hpp"
#include "Libusbpp/LibraryError.hpp"

using namespace Libusbpp;

Device::Device(
        const std::shared_ptr<libusb_context>& rpContext,
        libusb_device* const pRawDevice)
        : m_pContext(rpContext),
          m_pDevice(libusb_ref_device(pRawDevice), Deleter()),
          m_pDeviceDescriptor(),
          m_pObservedHandle()
{
    if (!m_pContext)
    {
        throw std::invalid_argument("Invalid Libusb context!");
    }
    else if (!m_pDevice)
    {
        throw std::invalid_argument("Invalid Libusb device!");
    }
}

const libusb_device_descriptor& Device::GetDeviceDescriptor()
{
    if (!m_pDeviceDescriptor)
    {
        m_pDeviceDescriptor.reset(new libusb_device_descriptor());
        const int result = libusb_get_device_descriptor(m_pDevice.get(),
                                                        m_pDeviceDescriptor.get());

        if (result != LIBUSB_SUCCESS)
        {
            throw LibraryError("libusb_get_device_descriptor", result);
        }
    }

    return *m_pDeviceDescriptor.get();
}

const libusb_config_descriptor& Device::GetActiveConfigDescriptor()
{
    libusb_config_descriptor* pRawConfigDescriptor = nullptr;
    const int result = libusb_get_active_config_descriptor(m_pDevice.get(),
                                                           &pRawConfigDescriptor);

    if (result != LIBUSB_SUCCESS)
    {
        throw LibraryError("libusb_get_active_config_descriptor", result);
    }
    else
    {
        m_pConfigDescriptor =
                std::unique_ptr<libusb_config_descriptor, Deleter>(pRawConfigDescriptor,
                                                                   Deleter());

        return *m_pConfigDescriptor.get();
    }
}

std::shared_ptr<DeviceHandle> Device::GetHandle()
{
    auto pHandle = m_pObservedHandle.lock();

    if (!pHandle)
    {
        pHandle = std::make_shared<DeviceHandle>(shared_from_this(), m_pDevice.get());
        m_pObservedHandle = pHandle;
    }

    return pHandle;
}
