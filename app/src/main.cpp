#include <libusb-1.0/libusb.h>
#include <cstdint>
#include <cstdlib>
#include <memory>
#include <string>
#include <vector>

#include "Bcd/Ctime.hpp"
#include "Libusbpp/Context.hpp"
#include "Libusbpp/DeviceHandle.hpp"
#include "Libusbpp/SyncIo/HidReportControlTransfer.hpp"
#include "Usb/Hid/Constants.hpp"
#include "Usb/Hid/reportIds.h"
#include "Usb/Hid/RtcUpdate/OutputReportData.hpp"

#if defined(NDEBUG)
#include <iostream>

#include "Libusbpp/PrettyPrinters.hpp"
#include "StreamHex.hpp"
#endif

int main()
{
    static constexpr uint16_t VUSB_HID_VID = 0x16C0;
    static constexpr uint16_t VUSB_HID_PID = 0x05DF;
    static constexpr const char* MANUFACTURER_DESCRIPTOR = "pawel.szramowski@gmail.com";
    static constexpr const char* PRODUCT_DESCRIPTOR = "CASH (Clock And Sensor Hub)";

#if defined(NDEBUG)
    std::cout << "Searching for USB device with:\n"
            << "- VID = " << StreamHex(VUSB_HID_VID) << '\n'
            << "- PID = " << StreamHex(VUSB_HID_PID) << '\n'
            << "- manufacturer descriptor = \"" << MANUFACTURER_DESCRIPTOR << "\"\n"
            << "- product descriptor = \"" << PRODUCT_DESCRIPTOR << "\"\n";
#endif

    int updatedDeviceNum = 0;

    Libusbpp::Context context(LIBUSB_LOG_LEVEL_INFO);
    for (const auto& rpDevice : context.GetAllDevices())
    {
        const auto& rDeviceDescriptor = rpDevice->GetDeviceDescriptor();

        if ((rDeviceDescriptor.idVendor == VUSB_HID_VID)
                && (rDeviceDescriptor.idProduct == VUSB_HID_PID))
        {
#if defined(NDEBUG)
            std::cout << "Found V-USB HID device, attempting to read descriptors...\n";
            std::cout << rDeviceDescriptor << '\n';
            std::cout << rpDevice->GetActiveConfigDescriptor() << '\n';
#endif

            const auto pDeviceHandle = rpDevice->GetHandle();
            const auto manufacturer = pDeviceHandle->GetManufacturer();
            const auto product = pDeviceHandle->GetProduct();

#if defined(NDEBUG)
            std::cout << "Manufacturer descriptor = \"" << manufacturer << "\"\n";
            std::cout << "Product descriptor = \"" << product << "\"\n";
#endif

            if ((manufacturer == MANUFACTURER_DESCRIPTOR) && (product == PRODUCT_DESCRIPTOR))
            {
#if defined(NDEBUG)
                std::cout << "Found matching device, updating RTC..." << '\n';
#endif

                Usb::Hid::RtcUpdate::OutputReportData rtcUpdateReport;
                rtcUpdateReport.m_Id = USB_HID_REPORT_ID_RTC_UPDATE;
                Bcd::Ctime::SetTimeDateToNow(rtcUpdateReport.m_Time, rtcUpdateReport.m_Date);

                Libusbpp::SyncIo::HidReportControlTransfer rtcUpdateTransfer(
                        rtcUpdateReport,
                        LIBUSB_ENDPOINT_OUT,
                        Usb::Hid::OUTPUT_REPORT,
                        rtcUpdateReport.m_Id);
                pDeviceHandle->PerformTransfer(rtcUpdateTransfer);

                ++updatedDeviceNum;

#if defined(NDEBUG)
                std::cout << "Update performed!" << '\n';
#endif
            }
        }
    }

#if defined(NDEBUG)
    std::cout << "Search finished!" << '\n';
#endif

    return (updatedDeviceNum > 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
