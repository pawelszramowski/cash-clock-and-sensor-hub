#pragma once

#if defined(__GNUC__) && defined(__AVR__)
namespace std {

/// Primary template conditionally selecting one of two types.
template<bool Cond, typename Ttrue, typename Tfalse>
struct conditional
{
    typedef Ttrue type;
};

/// Partial template specialization for false.
template<typename Ttrue, typename Tfalse>
struct conditional<false, Ttrue, Tfalse>
{
    typedef Tfalse type;
};

/// Primary template conditionally adding a type.
template<bool Cond, class T = void>
struct enable_if {};

template<class T>
struct enable_if<true, T>
{
    typedef T type;
};

/// Primary template checking if two types are the same.
template<class T, class U>
struct is_same
{
    static constexpr bool value = false;
};

/// Partial template specialization for same types.
template<class T>
struct is_same<T, T>
{
    static constexpr bool value = true;
};

}
#else
#include <type_traits>
#endif

namespace nonstd {

/// An overload which returns the greater of two given values.
template<typename T>
constexpr const T& maxv(
        const T& a,
        const T& b)
{
    return (a < b) ? b : a;
}

/// Returns the greatest of the given values.
template<typename T, typename ...Ts>
constexpr const T& maxv(
        const T& a,
        const T& b,
        const Ts& ...args)
{
    return maxv(maxv(a, b), args...);
}

/// An overload which returns the smaller of two given values.
template<typename T>
constexpr const T& minv(
        const T& a,
        const T& b)
{
    return (b < a) ? b : a;
}

/// Returns the smallest of the given values.
template<typename T, typename ...Ts>
constexpr const T& minv(
        const T& a,
        const T& b,
        const Ts& ...args)
{
    return minv(minv(a, b), args...);
}

}
