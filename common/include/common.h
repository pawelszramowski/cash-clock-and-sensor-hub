#pragma once

#include <stdint.h>

// @formatter:off

#if defined(__cplusplus)
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////
// Generic helper function-like macros
////////////////////////////////////////////////////////////////////////////////

#define ELEMENTS_IN(array)              (sizeof(array) / sizeof(array[0]))

#define BIT_VAL(bitNum)                 (1ULL << (bitNum))
#define BITS_MASK(bitCount)             (BIT_VAL(bitCount) - 1ULL)

#define CONCAT(a, b)                    CONCAT_NO_EXPAND(a, b)
#define CONCAT_NO_EXPAND(a, b)          a##b

#define STRINGIFY(a)                    STRINGIFY_NO_EXPAND(a)
#define STRINGIFY_NO_EXPAND(a)          #a

////////////////////////////////////////////////////////////////////////////////
// Macro tricks
////////////////////////////////////////////////////////////////////////////////

/// Returns the first of at least two arguments.
///
/// Warning "at least one argument is required for the <<...>> in a variadic macro"
/// will be generated for less than two arguments.
#define VA_GET_1ST_ARG_OF_MANY(first, ...)  first

/// Returns the first of one or more arguments.
#define VA_GET_1ST_ARG(...)             VA_GET_1ST_ARG_OF_MANY(__VA_ARGS__, dummy)

////////////////////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
    // Function and variable attributes.
    #define ATTR_SECTION(name)          __attribute__ ((section (name)))
    #define ATTR_UNUSED                 __attribute__ ((unused))
    #define ATTR_USED                   __attribute__ ((used))
    #define ATTR_WEAK                   __attribute__ ((weak))

    // Function attributes.
    #define ATTR_NO_RETURN              __attribute__ ((noreturn))
    #define ATTR_NO_INLINE              __attribute__ ((noinline))
    #define ATTR_ALWAYS_INLINE          __attribute__ ((always_inline))
    #define ATTR_PURE                   __attribute__ ((pure))
    #define ATTR_NAKED                  __attribute__ ((naked))

    // Variable attributes.
    #define ATTR_ALIGN(boundary)        __attribute__ ((aligned (boundary)))
    #if defined(_WIN32) || defined(_WIN64)
        #define ATTR_PACKED             __attribute__ ((packed, gcc_struct))
    #else
        #define ATTR_PACKED             __attribute__ ((packed))
    #endif
#else
    #define ATTR_SECTION(...)
    #define ATTR_UNUSED
    #define ATTR_USED
    #define ATTR_WEAK

    #define ATTR_NO_RETURN
    #define ATTR_NO_INLINE
    #define ATTR_ALWAYS_INLINE
    #define ATTR_PURE
    #define ATTR_NAKED

    #define ATTR_ALIGN(...)
    #define ATTR_PACKED
#endif

////////////////////////////////////////////////////////////////////////////////
// Assertions
////////////////////////////////////////////////////////////////////////////////

#if 201103L <= __cplusplus
    // static_assert is a keyword since C++11.
#elif 201112L <= __STDC_VERSION__
    #define static_assert(cond, msg)    _Static_assert(cond, msg)
#else
    #define static_assert(cond, msg)    typedef char \
                CONCAT(static_assert_at_line_, __LINE__)[(cond) ? 1 : -1]
#endif

#if defined(__GNUC__) && defined(__AVR__)
    #include <stddef.h>
    #include <avr/pgmspace.h>

    #define ASSERT_ALWAYS(cond)         ((cond) ? (void)0 : g_HandleAssertFailure( \
                (USE_BME280_INSTEAD_OF_SHT21_AND_MP3H6115A == 0) ? PSTR(__FILE__) : NULL, \
                __LINE__))

    ATTR_NO_RETURN
    void g_HandleAssertFailure(
            const char* pFileNameP,
            const uint16_t lineNum);
#elif defined(CPPUNIT_TEST)
    #include <cppunit/extensions/HelperMacros.h>
    #define ASSERT_ALWAYS(cond)         CPPUNIT_ASSERT(cond)
#else
    #if defined(NDEBUG)
        #define ASSERT_ALWAYS(cond)     (void)(cond)
    #else
        #include <assert.h>
        #define ASSERT_ALWAYS(cond)     assert(cond)
    #endif
#endif

////////////////////////////////////////////////////////////////////////////////
// AVR-specific
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__) && defined(__AVR__)
    #define PORT(name)                  CONCAT(PORT, name)
    #define PIN(name)                   CONCAT(PIN,  name)
    #define DDR(name)                   CONCAT(DDR,  name)

    typedef uint8_t                     small_size_t;
#endif

#if defined(__cplusplus)
}
#endif
