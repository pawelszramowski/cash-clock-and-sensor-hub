#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

enum UsbHidReportId
{
    USB_HID_REPORT_ID_RTC_UPDATE = 1U,
    USB_HID_REPORT_ID_ATMOSPHERIC_PRESSURE_SENSOR = 2U,
    USB_HID_REPORT_ID_TEMPERATURE_SENSOR = 3U,
    USB_HID_REPORT_ID_RELATIVE_HUMIDITY_SENSOR = 4U,
    USB_HID_REPORT_ID_PM2P5_SENSOR = 5U,
};

#if defined(__cplusplus)
}
#endif
