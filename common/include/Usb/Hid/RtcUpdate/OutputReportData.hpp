#pragma once

#include <stdint.h>

#include "Bcd/Date.hpp"
#include "Bcd/Time.hpp"
#include "common.h"

namespace Usb {
namespace Hid {
namespace RtcUpdate {

/// RTC update output report data received from host.
///
/// @warning It must be a POD since it's exchanged with external devices
/// so it must have a trivial default constructor (not user-provided)!
struct OutputReportData final
{
    uint8_t m_Id;
    Bcd::Date m_Date;
    Bcd::Time m_Time;
}ATTR_PACKED;

static_assert(sizeof(OutputReportData) == 8, "Unexpected report size!");

}
}
}
