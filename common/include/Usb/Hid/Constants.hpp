#pragma once

#include <stdint.h>

namespace Usb {
namespace Hid {

enum Request : uint8_t
{
    GET_REPORT = 0x01,
    GET_IDLE = 0x02,
    GET_PROTOCOL = 0x03,
    SET_REPORT = 0x09,
    SET_IDLE = 0x0A,
    SET_PROTOCOL = 0x0B,
};

enum ReportType : uint8_t
{
    INPUT_REPORT = 0x01,
    OUTPUT_REPORT = 0x02,
    FEATURE_REPORT = 0x03,
};

static constexpr uint8_t REPORT_ID_UNUSED = 0U;

}
}
