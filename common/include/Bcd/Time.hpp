#pragma once

#include "common.h"

#include "Bcd/Value.hpp"

namespace Bcd {

/// 24-hour clock represented in Binary Coded Decimal values.
///
/// @warning It must be a POD since it's exchanged with external devices
/// so it must have a trivial default constructor (not user-provided)!
struct Time final
{
    Bcd_t m_Hour;
    Bcd_t m_Minute;
    Bcd_t m_Second;

    static constexpr Bcd_t HOUR_MIN = 0x00U;
    static constexpr Bcd_t HOUR_MAX = 0x23U;

    static constexpr Bcd_t MINUTE_MIN = 0x00U;
    static constexpr Bcd_t MINUTE_MAX = 0x59U;

    static constexpr Bcd_t SECOND_MIN = 0x00U;
    static constexpr Bcd_t SECOND_MAX = 0x59U;

    bool operator !=(
            const Time& rRhs) const
    {
        return (m_Second != rRhs.m_Second) || (m_Minute != rRhs.m_Minute) || (m_Hour != rRhs.m_Hour);
    }

    bool IsValid() const
    {
        return IsInRange(m_Hour, HOUR_MIN, HOUR_MAX) && IsInRange(m_Minute, MINUTE_MIN, MINUTE_MAX)
                && IsInRange(m_Second, SECOND_MIN, SECOND_MAX);
    }

    void SetToDayStart()
    {
        m_Hour = HOUR_MIN;
        m_Minute = MINUTE_MIN;
        m_Second = SECOND_MIN;
    }

    void SetToDayEnd()
    {
        m_Hour = HOUR_MAX;
        m_Minute = MINUTE_MAX;
        m_Second = SECOND_MAX;
    }
}ATTR_PACKED;

static_assert(sizeof(Time) == 3, "Unexpected Time size!");

}
