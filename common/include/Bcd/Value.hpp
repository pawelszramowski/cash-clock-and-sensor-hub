#pragma once

#include <stdint.h>
#include <stdlib.h>

namespace Bcd {

using Bcd_t = uint8_t;

static inline Bcd_t BinToBcd(
        const uint8_t bin)
{
    const div_t divBy10 = div(bin, 10);

    return ((divBy10.quot % 10) << 4U) | divBy10.rem;
}

static inline uint8_t BcdToBin(
        const Bcd_t bcd)
{
    // BCD ones already have the correct weight, but BCD tens have weight 16
    // and binary tens have weight 10.
    // Instead of calculating:
    // binTens = bcdTens / 16 * 10
    // calculate:
    // binTens = bcdTens / 2 + bcdTens / 8
    const uint8_t bcdTensDivBy2 = (bcd & 0xF0U) >> 1U;
    const uint8_t bcdTensDivBy8 = bcdTensDivBy2 >> 2U;
    const uint8_t ones = bcd & 0x0FU;

    return bcdTensDivBy2 + bcdTensDivBy8 + ones;
}

static inline bool IsInRange(
        const Bcd_t bcd,
        const Bcd_t min = 0x00U,
        const Bcd_t max = 0x99U)
{
    return ((bcd & 0xF0U) <= 0x90U) && ((bcd & 0x0FU) <= 0x09U) && (bcd >= min) && (bcd <= max);
}

static inline Bcd_t Correct(
        Bcd_t bcd)
{
    if ((bcd & 0x0FU) > 0x09U)
    {
        bcd += 0x06U; // Correct low nibble.
    }

    if ((bcd & 0xF0U) > 0x90U)
    {
        bcd += 0x60U; // Correct high nibble.
    }

    return bcd;
}

}
