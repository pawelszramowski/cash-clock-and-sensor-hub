#pragma once

#include "common.h"

#include "Bcd/Value.hpp"

namespace Bcd {

/// 21st century date represented in Binary Coded Decimals values.
///
/// @warning It must be a POD since it's exchanged with external devices
/// so it must have a trivial default constructor (not user-provided)!
struct Date final
{
    Bcd_t m_Year;
    Bcd_t m_Month;
    Bcd_t m_Day;
    Bcd_t m_Weekday;

    static constexpr Bcd_t YEAR_MIN = 0x00U;
    static constexpr Bcd_t YEAR_MAX = 0x99U;

    enum Month : Bcd_t
    {
        MONTH_JANUARY = 0x01U,
        MONTH_FEBRUARY = 0x02U,
        MONTH_MARCH = 0x03U,
        MONTH_APRIL = 0x04U,
        MONTH_MAY = 0x05U,
        MONTH_JUNE = 0x06U,
        MONTH_JULY = 0x07U,
        MONTH_AUGUST = 0x08U,
        MONTH_SEPTEMBER = 0x09U,
        MONTH_OCTOBER = 0x10U,
        MONTH_NOVEMBER = 0x11U,
        MONTH_DECEMBER = 0x12U
    };

    static constexpr Bcd_t DAY_FIRST = 0x01U;
    static constexpr Bcd_t DAY_LAST_LONG = 0x31U;
    static constexpr Bcd_t DAY_LAST_FEBRUARY = 0x28U;
    static constexpr Bcd_t DAY_LAST_FEBRUARY_LEAP = 0x29U;
    static constexpr Bcd_t DAY_LAST_SHORT = 0x30U;

    enum Weekday : Bcd_t
    {
        WEEKDAY_SUNDAY = 0x00U,
        WEEKDAY_MONDAY = 0x01U,
        WEEKDAY_TUESDAY = 0x02U,
        WEEKDAY_WEDNESDAY = 0x03U,
        WEEKDAY_THURSDAY = 0x04U,
        WEEKDAY_FRIDAY = 0x05U,
        WEEKDAY_SATURDAY = 0x06U,
        WEEKDAY_FIRST = WEEKDAY_SUNDAY,
        WEEKDAY_LAST = WEEKDAY_SATURDAY
    };

    bool operator !=(
            const Date& rRhs) const
    {
        return (m_Weekday != rRhs.m_Weekday) || (m_Day != rRhs.m_Day) || (m_Month != rRhs.m_Month)
                || (m_Year != rRhs.m_Year);
    }

    bool IsValid() const
    {
        return IsInRange(m_Year, YEAR_MIN, YEAR_MAX)
                && IsInRange(m_Month, MONTH_JANUARY, MONTH_DECEMBER)
                && IsInRange(m_Weekday, WEEKDAY_FIRST, WEEKDAY_LAST)
                && IsInRange(m_Day, DAY_FIRST, GetLastDayInMonth());
    }

    Bcd_t GetLastDayInMonth() const
    {
        switch (m_Month)
        {
        default:
        case MONTH_JANUARY:
        case MONTH_MARCH:
        case MONTH_MAY:
        case MONTH_JULY:
        case MONTH_AUGUST:
        case MONTH_OCTOBER:
        case MONTH_DECEMBER:
            return DAY_LAST_LONG;
        case MONTH_APRIL:
        case MONTH_JUNE:
        case MONTH_SEPTEMBER:
        case MONTH_NOVEMBER:
            return DAY_LAST_SHORT;
        case MONTH_FEBRUARY:
            return IsYearLeap() ? DAY_LAST_FEBRUARY_LEAP : DAY_LAST_FEBRUARY;
        }
    }

    bool IsYearLeap() const
    {
        // Assume 21st century, where all leap years are divisible by 4.
        switch (m_Year & 0x13U)
        {
        case 0x00U: // Tens are even and ones are 0, 4, or 8, e.g. 2000, 2024, 2048.
        case 0x12U: // Tens are odd and ones are 2 or 6, e.g. 2012, 2036.
            return true;
        default:
            return false;
        }
    }

    void SetToCenturyStart()
    {
        m_Year = YEAR_MIN;
        m_Month = MONTH_JANUARY;
        m_Day = DAY_FIRST;
        m_Weekday = WEEKDAY_SATURDAY;
    }

    void SetToCenturyEnd()
    {
        m_Year = YEAR_MAX;
        m_Month = MONTH_DECEMBER;
        m_Day = DAY_LAST_LONG;
        m_Weekday = WEEKDAY_THURSDAY;
    }
} ATTR_PACKED;

static_assert(sizeof(Date) == 4, "Unexpected Date size!");

}
