#include <type_traits>

#include "Bcd/Date.hpp"
#include "Bcd/Time.hpp"
#include "Usb/Hid/RtcUpdate/OutputReportData.hpp"
#include "Usb/Hid/Sensor/FeatureReportData.hpp"
#include "Usb/Hid/Sensor/InputReportData.hpp"

#define STATIC_ASSERT_IS_POD(type) static_assert(std::is_pod<type>::value == true, \
                                                 #type " must be Plain Old Data type!")

STATIC_ASSERT_IS_POD(Bcd::Date);
STATIC_ASSERT_IS_POD(Bcd::Time);
STATIC_ASSERT_IS_POD(Usb::Hid::RtcUpdate::OutputReportData);
STATIC_ASSERT_IS_POD(Usb::Hid::Sensor::FeatureReportData<uint16_t>);
STATIC_ASSERT_IS_POD(Usb::Hid::Sensor::InputReportData<uint16_t>);

int main()
{
    return 0;
}
