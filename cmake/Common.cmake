function(enable_ipo_if_supported TARGET)
    include(CheckIPOSupported)
    check_ipo_supported(RESULT IS_IPO_SUPPORTED)
    if(IS_IPO_SUPPORTED)
        set_target_properties(${TARGET}
            PROPERTIES
                INTERPROCEDURAL_OPTIMIZATION TRUE
        )
    endif()
endfunction()

function(add_doxygen_docs TARGET)
    find_package(Doxygen)
    if(DOXYGEN_FOUND)
        set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
        set(DOXYGEN_QT_AUTOBRIEF YES)
        set(DOXYGEN_MULTILINE_CPP_IS_BRIEF YES)
        set(DOXYGEN_EXTRACT_ALL YES)
        set(DOXYGEN_EXTRACT_PRIVATE YES)
        set(DOXYGEN_EXTRACT_PRIV_VIRTUAL YES)
        set(DOXYGEN_EXTRACT_PACKAGE YES)
        set(DOXYGEN_EXTRACT_STATIC YES)
        set(DOXYGEN_EXTRACT_ANON_NSPACES YES)
        set(DOXYGEN_SOURCE_BROWSER YES)
        set(DOXYGEN_INLINE_SOURCES YES)
        set(DOXYGEN_REFERENCED_BY_RELATION YES)
        set(DOXYGEN_REFERENCES_RELATION YES)
        set(DOXYGEN_UML_LOOK YES)
        set(DOXYGEN_TEMPLATE_RELATIONS YES)
        set(DOXYGEN_CALL_GRAPH NO)  # This option significantly increases the time of a run.
        set(DOXYGEN_CALLER_GRAPH NO)  # This option significantly increases the time of a run.
        set(DOXYGEN_DOT_MULTI_TARGETS YES)

        # Using "$<TARGET_PROPERTY:${TARGET},NAME>" directly
        # in the following function call does not work.
        get_target_property(DOXYGEN_INPUT_SOURCES ${TARGET} SOURCES)
        get_target_property(DOXYGEN_INPUT_INCLUDE_DIRECTORIES ${TARGET} INCLUDE_DIRECTORIES)

        doxygen_add_docs(doxygen_docs
            ${DOXYGEN_INPUT_SOURCES}
            ${DOXYGEN_INPUT_INCLUDE_DIRECTORIES}
        )
    endif()
endfunction()
